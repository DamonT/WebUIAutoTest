# Selenium WebUI自动化测试框架
使用Python+Selenium+第三方库实现WebUI自动化测试框架。

## 简介
### 项目目录
- `AutoIt`: 操作界面ui的第三方插件
- `Config`: 配置文件
- `Data`: 测试数据
- `Drivers`: 浏览器驱动
- `Elements`: 页面定位元素
- `Log`: 测试运行时生成日志存放的目录
- `Report`: 执行结束后生成报告存放的目录
- `src`: 项目源码
- `TestCases`: 存放测试用例集、测试用例生成的数据(用例中做数据比对时生成)
- `.flaskenv`: FlaskWeb环境变量配置
- `export_package_info.bat`: 导出当前环境下所有第三方库信息
- `package_info.txt`: 当前环境下所有第三方库信息
- `requirements.txt`: 依赖第三方库明细
- `automation.data`: 自动化测试运行数据
- `runtest.bat`: 启动并执行案例(执行`runtest.py`)
- `runtest.py`: 启动脚本
- `stoptest.bat`: 停止案例执行(执行`stoptest.py`)
- `stoptest.py`: 停止脚本
- `runserver.bat`: 网页启动脚本(执行`webserver.py`)
- `webserver.py`: 网页启动脚本

### 如何运行
1. 这里我们用`TestCases\test_案例_1.py`用例举例
   该用例实现的是访问百度(www.baidu.com)并搜索Python，具体可以查看该py文件内test_1_百度搜索测试()方法
2. 记得把案例都放到`TestCases`目录中，该目录存放所有自动化测试案例
3. 然后启动web服务, 在web页面中管理用例以及执行构建

## 运行效果

### 启动执行
- 1. 点击`runserver.bat`启动web服务
![web_console]
- 2. 用户登录后进入编辑页面<br>
     将页面左侧`案例目录`的用例拖拽到右侧`执行计划`中，在`执行计划`中管理用例的执行顺序，只有勾选才会执行，然后点击保存执行计划。(<small>*用例颗粒度已具体到方法层*</small>)
![web_edit]
- 3. 进入构建页面构建执行
![web_exec]

### 案例编辑
- 双击构建页面`案例目录`中的案例，自动跳转到编码页面，可以做轻量级案例编写工作
![coder]

### 测试报告
- 执行完毕报告展示
![report]

### 钉钉消息通知
- 通过钉钉机器人通知群里面成员
![dingdingmsg]

### 邮件通知
- 收到测试报告邮件
![email]

### 首页图表展示
- 展示用户最近构建次数
![index]
  > 后续增加更多图表展示

## 安装部署
- 推荐使用Pyhon3.5.4及以上版本
- 使用命令`pip install -r requirements.txt`安装第三方库(`requirements.txt`文件在项目目录下)
- 选择本地浏览器对应版本的驱动放到`Drivers`目录(谷歌:chromedriver.exe, 火狐:geckodriver.exe)
- `.flaskenv`文件，当部署生产环境时，需要将该文件中的配置`FLASK_ENV=production` `FLASK_DEBUG=0`，
测试时可以将其设置为`FLASK_ENV=development` `FLASK_DEBUG=1`

## 使用介绍
- 初始用户名和密码为`admin/admin`，可以直接登录使用。用户信息存在数据库`WebUIAutoTest\src\web\app\app.db`的`main.user`表中，
其中密码采用的是`from werkzeug.security import generate_password_hash`方法提供加密算法加密，密码设置为明文会无法校验通过。
- 如果使用注册功能注册新的账户，请先在`Config/config.ini`文件`EMAIL`选择项中配置系统管理员的SMTP邮箱服务，
之后在注册时会通过该邮箱发送注册信息。
相关配置为（`smtp_login_name`, `smtp_login_passwd`, `sender_address`, `smtp_server_address`, `smtp_port`, `smtp_ssl`, `smtp_tls`）
- Web服务相关日志在`WebUIAutoTest\src\web\app\logs\webserver.log`中，可以查看相关报错日志信息
自动化构建相关日志在`WebUIAutoTest\Log`目录中，可以查看自动化运行过程中日志信息（自动化构建日志也可以通过Web页面中日志菜单查看）

## 扩展功能

### 1. GUI操作
- 支持常用的鼠标操作(鼠标移动拖拽、左键右键中键点击、滚轮滚动...)
- 支持对windows窗口操作(获取窗口句柄、关闭窗口、检测窗口存在、检测是否被激活、激活指定窗口)
- 支持调用.au3脚本

    示例:
    ```py
    from src import Operate
    from src import run_autoit

    Operate.mouse_move(100, 100)  # 移动鼠标到指定位置
    run_autoit("UpLoadFile.au3")  # 执行"文件上传.au3“脚本
    ```

### 2. 数据比对
- 支持在执行完某一场景后对数据库表进行比对
- 支持MySql与Oracle

    示例：可查看`WebUIAutoTest\TestCases\test_dbcheck_demo.py`
    ```   
    每次执行完被装饰的场景后都会执行该条语句进行数据比对。
    *期望数据.xlsx与*执行结果.xlsx都在当前案例目录中，执行完比对后会生成一份比较结果文件。而且当比对失败后也会在报告中提示出来。   
    注意：当第一次执行时由于没有期望数据，不会成功比较，可以将第一次执生成的实际结果文件改成期望数据（修改文件名[执行结果]改为[期望数据]）。第二次执行时就可作为期望数据进行比对。
    ```
    ```py
    ...
    from src import test_dbcheck
    ...
    class DBCheckDemo(TestCaseMore):
    @classmethod
    def setUpClass(cls):
        pass
    @classmethod
    def tearDownClass(cls):
        pass

    @test_dbcheck(sql="select * from dbuser.tablename;")
    def test_1_dbcheck(self):
        pass
        # do something
    ...
    ```

### 3. 数据驱动
- 支持数据文件来驱动场景的执行

    示例：可查看`WebUIAutoTest\TestCases\test_datadriver_demo.py`
    ```py
    ...
    from src import DataDriver 
    ...
    class DataDriverDemo(TestCaseMore):
        def test_datadriver(self):
            # 调用被数据驱动的方法do_something(), 会根据文件每遍历一行数据调用一次do_something()，参数会根据列名将数据自动传入
            self.do_something()

        @DataDriver.run_date_from_csv(path=Config.projectDir + r"\Data\data.csv")
        def do_something(self, 工号, 姓名, 年龄):
            # 执行
            pass
    ...    
    ```
    数据文件(data.csv)
    ```
    工号,姓名,年龄
    1001,张三,21
    1002,李四,28
    1003,王五,24
    ```

### 4. 并发执行
- 支持同时并发执行多个用例
    
    并发执行案例的配置是通过在“测试案例场景.xlsx”中添加多个Sheet页案例来管理，每个sheet页会单独起一个进程来执行里面的案例。比如\Config\测试案例场景_2.xlsx中配置了3个Sheet页，那么在执行时会同时起3个进程分别执行每个Sheet页里面案例。
    > 注意：通过web页面进行构建时暂不支持并发执行

### 5. 插件拓展案例功能
- 支持用户添加插件拓展案例中可调用方法

    插件所在目录为`WebUIAutoTest\src\extension`
    
    目录下默认有一个`data_util.py`文件，里面定义了一个类`DataUtil`，方法`func1`定义在该类中。
    这样我们就可以在案例中使用 "self.func1(*args, **kwargs)"格式调用插件中的方法
    ```python
    class Test_Demo(TestCaseMore):  # 要继承TestCaseMore
    ...
    def test_method_demo(self):
        logger.info("正在执行 Test_Demo.test_method_demo")
        ...
        self.func1(*args, **kwargs)  # 调用插件中的方法
        ...
    ...
    ```
    需要注意的是，如果插件中定义的方法或属性与TestCaseMore类及其父类方法或属性重名，则会有警告日志提示该方法或属性重名。
    
    当然也可以在`WebUIAutoTest\src\extension`添加自己的py文件作为插件，`data_util.py`仅作为示例。

### 6. 支持定时任务配置
- 支持用户配置定时任务（与Jenkins中的定时任务配置类似）
   1. 配置定时时间，在`src/web/app/celery.py`中配置，如下。通过字典数据格式可以配置多个定时任务，其中`scheduel`项配置可参考文档[celery.schedules.crontab]
        ```python
        celery.conf.beat_schedule = {
            'run-once-every-day': {
                # 执行tasks下的run_test函数
                'task': 'src.web.app.tasks.run_test',
        
                # 每天指定时间执行一次
                'schedule': crontab(minute='30', hour='0'),
        
                # 传递参数
                # 'args': ('a',)
            },
        }
        ```
   2. 启动定时服务脚本，分别`celery_worker_server_start.bat`和`celery_beat_start.bat`
   3. 日志可以在`Log/celery_worker_server.log`和`Log/celery_beat.log`中查看
    
## 应用架构图
![architecture]

## 其他
> 作者联系方式
> - 邮箱: zhangzheng527@outlook.com
> - QQ交流群: 977134581
>
> 相关项目
> - [ApiAutomationTest](https://gitee.com/azhengzz/api-automation-test) (接口自动化平台) 

[console]: ./other/console.jpg
[web_console]: ./other/web_console.jpg
[web_edit]: ./other/web_edit.jpg
[web_exec]: ./other/web_exec.jpg
[report]: ./other/report.png
[email]: ./other/email.png
[dingdingmsg]: ./other/dingdingmsg.png
[index]: ./other/index.png
[coder]: ./other/coder.png
[architecture]: ./other/architecture.png
[celery.schedules.crontab]: http://docs.jinkan.org/docs/celery/reference/celery.schedules.html#celery.schedules.crontab