# coding=utf-8

from src.web.app import create_app
from src.web.app.init import web_init

web_init()

app = create_app()

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
    # app.run()
