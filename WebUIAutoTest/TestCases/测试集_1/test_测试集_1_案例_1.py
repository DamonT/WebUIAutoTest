# coding=utf-8

import time

from Elements.火车票 import 主页, 购票页
from src import Log                 # 日志记录功能
from src import TestCaseMore        # 继承一些公共方法
from src import Driver              # 重新封装的Dirver
from src import Keys

logger = Log()


class 购票(TestCaseMore):
    @classmethod
    def setUpClass(cls):
        cls.base_url = "http://www.12306.cn/"
        cls.driver = Driver(cls.base_url, "firefox")

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    def test_1_购票查询(self):
        logger.info("正在执行购票.test_1_购票查询")
        self.driver.get_element(*主页.购票).click()
        time.sleep(5)
        self.driver.switch_to_window(self.driver.window_handles[1])
        # 通过js实现元素操作
        # self.driver.execute_script("document.getElementById('fromStationText').value = '杭州';")
        # self.driver.execute_script("document.getElementById('toStationText').value = '郑州';")
        self.driver.get_element(*购票页.出发地).click()
        self.driver.get_element(*购票页.出发地).send_keys("杭州")
        self.driver.get_element(*购票页.出发地).keyboard(key=Keys.ENTER)
        self.driver.get_element(*购票页.目的地).click()
        self.driver.get_element(*购票页.目的地).send_keys("郑州")
        self.driver.get_element(*购票页.目的地).keyboard(key=Keys.ENTER)
        # 使用js方法输入
        self.driver.get_element(*购票页.出发日).js_send_keys("2018-06-20")
        self.driver.get_element(*购票页.返程日).js_send_keys("2018-06-21")
        self.driver.get_element(*购票页.学生).click()
        self.driver.get_element(*购票页.查询).click()
