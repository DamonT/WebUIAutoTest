# coding=utf-8

from selenium.common.exceptions import NoSuchElementException
import unittest
import time, os

from Elements.jira import JIRA
from src import Log
from src import TestCaseMore
from src import Driver
from src import Keys
from src import Config

logger = Log()


class 案例3_JIRA(TestCaseMore):
    @classmethod  # 执行整个TestCase中只调用一次setUp和tearDown
    def setUpClass(cls):
        cls.driver = Driver(
            "https://se.company.com/dm/secure/Dashboard.jspa"
        )  # 根据配置文件获取相应版本的浏览器Driver
        cls.base_url = "https://se.company.com/dm/secure/Dashboard.jspa"

    @classmethod  # 执行整个TestCase中只调用一次setUp和tearDown
    def tearDownClass(cls):
        # cls.driver.quit()
        pass

    def test_1_JIRA_登录测试(self):
        logger.info("正在执行 案例3_JIRA.test_1_JIRA_登录测试")
        self.driver.get(url=self.base_url)
        self.driver.get_element(*JIRA.登录用户名输入框).send_keys("azheng")
        self.driver.get_element(*JIRA.登录密码输入框).send_keys("********")
        self.driver.get_element(*JIRA.登录确定按钮).click()
        self.screen_shot()
        time.sleep(2)
        # 断言
        self.checkElement(*JIRA.a_azheng_loc)

    def test_2_JIRA_新增缺陷(self):
        logger.info("正在执行 案例3_JIRA.test_2_JIRA_新增缺陷")
        self.driver.get_element(*JIRA.新建按钮).click()
        self.driver.get_element(*JIRA.概要输入框).send_keys("输入概要")
        self.driver.get_element(*JIRA.优先级输入框).send_keys("致命")
        self.driver.get_element(*JIRA.缺陷来源下拉框).select("代码活动")
        self.driver.get_element(*JIRA.缺陷来源第二个下拉框).select("逻辑问题（函数引用、返回、顺序、易读性）")
        self.driver.get_element(*JIRA.模块多行输入框).send_keys("UFTDB")
        self.driver.get_element(*JIRA.单元测试发现).select("是")
        self.driver.get_element(*JIRA.经办人输入框).send_keys("张三")
        self.driver.get_element(*JIRA.经办人输入框).keyboard(Keys.ENTER)
        self.driver.get_element(*JIRA.环境多行输入框).send_keys("输入环境")
        self.driver.get_element(*JIRA.描述多行输入框).send_keys("输入描述")
        self.driver.get_element(*JIRA.上传文件).send_keys(
            os.path.join(Config.project_dir, "runtest.py")
        )
        self.driver.get_element(*JIRA.上传文件).send_keys(
            os.path.join(Config.project_dir, "demo.py")
        )
        self.driver.get_element(*JIRA.报告人输入框).send_keys("张三")
        self.driver.get_element(*JIRA.报告日期输入框).clear()
        self.driver.get_element(*JIRA.报告日期输入框).send_keys("1/四月/18")
        self.driver.get_element(*JIRA.修改单号输入框).send_keys("123123")
        self.driver.get_element(*JIRA.缺陷发现阶段选择框).select("编码（单元测试）")
        self.driver.get_element(*JIRA.类别选择框).select("A.功  能")
        self.driver.get_element(*JIRA.打回次数输入框).send_keys("1")
        self.driver.get_element(*JIRA.测试方法选择框).select("自动化测试")


# 案例也可单独调试
if __name__ == "__main__":
    unittest.main()
