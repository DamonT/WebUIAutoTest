# coding:utf-8

import unittest
import time

from Elements.百度 import 首页  # 元素库

from src import Log
from src import TestCaseMore
from src import Driver
from src import By
from src import DataDriver
from src import Config


logger = Log()  # 创建日志记录对象,记录日志信息通过logger.info logger.error选择不同级别来记录


# 继承TestCaseMore 必填
class 案例1_搜索测试(TestCaseMore):
    @classmethod  # 执行整个TestCase中只调用一次setUp
    def setUpClass(cls):  # 案例初始化时执行 必填
        cls.driver = Driver("https://www.baidu.com")  # 初始化驱动，指定浏览器类型并打开初始url

    @classmethod  # 执行整个TestCase中只调用一次tearDown
    def tearDownClass(cls):  # 案例结束时执行 必填
        cls.driver.quit()

    def test_1_百度搜索测试(self):  # 测试用例函数 类中至少要有一个
        logger.info("正在执行 案例1_搜索测试.test_1_百度搜索测试")  # 记录日志，级别为info
        self.driver.get_element(*首页.输入框).clear()  # 操作步骤
        self.driver.get_element(*首页.输入框).send_keys("Python")
        self.driver.get_element(*首页.确定按钮).click()
        time.sleep(2)
        logger.debug(self.driver.title)  # 记录日志，级别为debug
        self.driver.get_screen_shot()  # 截图，自动保存在log目录下
        print("测试报告打印")
        self.screen_shot()
        # 断言
        print(self.checkElement(*(By.CSS_SELECTOR, "#kw")))     # 检查指定元素是否存在
        self.assertEqual(self.driver.title, "Python_百度搜索")   # 断言标题是否为“Python_百度搜索”

    def test_2_百度搜索测试(self):
        # 数据驱动测试
        self.search()

    @DataDriver.run_data_from_excel(path=Config.project_dir + r"\Data\TestData.xlsx")
    def search(self, 搜索内容, 显示条数, 是否存在):
        print(搜索内容, 显示条数, 是否存在)
        # ...
        # 自动化场景
        # ...


# 案例也可单独调试
if __name__ == "__main__":
    unittest.main()
