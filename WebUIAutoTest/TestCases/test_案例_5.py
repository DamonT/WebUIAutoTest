# coding=utf-8

import unittest

from src import Log
from src import TestCaseMore
from src import Operate


logger = Log()


class Test_5(TestCaseMore):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def test_gui_operte(self):
        logger.info("正在执行 Test_5.test_gui_operte")
        Operate.mouse_click(x=500, y=300, button="right", speed=50)


# 案例也可单独调试
if __name__ == "__main__":
    unittest.main()
