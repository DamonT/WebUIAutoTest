# coding=utf-8

import unittest

from src import Log
from src import TestCaseMore


logger = Log()


def setUpModule():
    logger.info('setUpModule')


def tearDownModule():
    logger.info('tearDownModule')


class Test_4(TestCaseMore):
    @classmethod  # 执行整个TestCase中只调用一次setUp和tearDown
    def setUpClass(cls):
        logger.info('Test_4.setUpClass')
        pass

    @classmethod  # 执行整个TestCase中只调用一次setUp和tearDown
    def tearDownClass(cls):
        logger.info('Test_4.tearDownClass')
        pass

    def test_4_1(self):
        logger.info("正在执行 Test_4.test_4_1")
        print("test_4_1")
        self.assertFalse(True, '判断为False')

    # @unittest.skip('skip测试')
    def test_4_2(self):
        logger.info("正在执行 Test_4.test_4_2")
        # raise unittest.SkipTest('unittest.SkipTest测试')
        print("test_4_2")


class Test_5(unittest.TestCase):
    @classmethod  # 执行整个TestCase中只调用一次setUp和tearDown
    def setUpClass(cls):
        logger.info('Test_5.setUpClass')
        pass

    @classmethod  # 执行整个TestCase中只调用一次setUp和tearDown
    def tearDownClass(cls):
        logger.info('Test_5.tearDownClass')
        pass

    def test_5_1(self):
        logger.info("正在执行 Test_5.test_5_1")
        print("test_5_1")
        raise NameError('这是一个NameError')

    def test_5_2(self):
        logger.info("正在执行 Test_5.test_5_2")
        print("test_5_2")
        raise self.failureException('这是AssertionError')


class Test_6(unittest.TestCase):
    @classmethod  # 执行整个TestCase中只调用一次setUp和tearDown
    def setUpClass(cls):
        logger.info('Test_6.setUpClass')
        pass

    @classmethod  # 执行整个TestCase中只调用一次setUp和tearDown
    def tearDownClass(cls):
        logger.info('Test_6.tearDownClass')
        pass

    def test_6_1(self):
        logger.info("正在执行 Test_6.test_6_1")
        print("test_6_1")

    def test_6_2(self):
        logger.info("正在执行 Test_6.test_6_2")
        print("test_6_2")


# 案例也可单独调试
if __name__ == "__main__":
    unittest.main()
    # Test_4(methodName='test_4_1').run()
