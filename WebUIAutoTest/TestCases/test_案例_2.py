# coding=utf-8

import unittest
import time

from Elements.百度 import 首页
from src import TestCaseMore
from src import Config
from src import Driver
from src import Log

logger = Log()


class 案例2_搜索测试(TestCaseMore):
    @classmethod  # 执行整个TestCase中只调用一次setUp和tearDown
    def setUpClass(cls):
        cls.driver = Driver("https://www.baidu.com")  # 根据配置文件获取相应版本的浏览器Driver
        cls.base_url = "https://www.baidu.com"

    @classmethod  # 执行整个TestCase中只调用一次setUp和tearDown
    def tearDownClass(cls):
        cls.driver.quit()

    def test_2_百度搜索测试(self):
        logger.info("正在执行 Test_2.test_baidu_2_search")
        self.driver.get_element(*首页.输入框).clear()
        self.driver.get_element(*首页.输入框).send_keys("Selenium")
        self.driver.get_element(*首页.确定按钮).click()
        time.sleep(2)
        self.screen_shot()
        # 断言
        self.assertEqual(self.driver.title, "Selenium_百度搜索")

    def test_3_百度搜索测试(self):
        logger.info("正在执行 Test_2.test_baidu_3_search")
        excel_path = Config.project_dir + r"\Data\TestData.xlsx"
        self.driver.get(url=self.base_url)
        self.driver.get_element(*首页.输入框).clear()
        self.driver.get_element(*首页.输入框).send_keys(
            self.read_excel(path=excel_path, sheet=1, row=2, column="搜索内容")
        )
        self.driver.get_element(*首页.确定按钮).click()
        time.sleep(2)
        # 断言
        self.screen_shot()
        self.assertEqual(self.driver.title, "Selenium_百度搜索_断言")


# 案例也可单独调试
if __name__ == "__main__":
    unittest.main()
