# coding=utf-8

"""
Author: zhangzheng
Description: 用户项目内置sqlite数据库数据读取
Version: 0.0.1
LastUpdateDate: 
UpadteURL: 
LOG: 
"""
import os
import sqlite3

from src.config.config import PROJECT_DIR

DB_PATH = os.path.join(PROJECT_DIR, 'src', 'web', 'app', 'app.db')


class DB:
    def __init__(self):
        self.connection = sqlite3.connect(database=DB_PATH)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.connection.close()

    def get_test_plans(self):
        cursor = self.connection.cursor()
        cursor.execute('select version from exec_plan order by version desc limit 1;')
        result = cursor.fetchone()
        if result:
            max_version = result[0]
        cursor.execute('select data, responsible_person '
                       'from exec_plan '
                       'where version = ? and checked = 1 order by serial_no asc;',
                       (max_version,))
        result = cursor.fetchall()
        cursor.close()
        return result
