# coding=utf-8

"""
Author: zhangzheng
Description: 监控构建状态等信息
Version: 0.0.1
LastUpdateDate: 
UpadteURL: 
LOG: 
"""

import os
import psutil

from src.config.config import Config
from src.public.public import ParseXml


def running() -> bool:
    """
        获取自动化构建状态
    :return: True-正在运行, False-已停止
    """
    xml = ParseXml(filepath=os.path.join(Config.project_dir, 'automation.data'))
    pid = xml.get_value_from_xpath(xpath='./BuildData/RunningPID', attribute='pid')
    if psutil.pid_exists(int(pid)) and psutil.Process(int(pid)).name() == 'python.exe':
        return True
    else:
        return False


def progress() -> int:
    """获取当前构建的进度"""
    if running():
        xml = ParseXml(filepath=os.path.join(Config.project_dir, 'automation.data'))
        total = int(xml.get_value_from_xpath(xpath='./BuildData/ExecProgress', attribute='total'))
        executed = int(xml.get_value_from_xpath(xpath='./BuildData/ExecProgress', attribute='executed'))
        return 0 if total == 0 else round(executed / total * 100, 2)
    else:
        return 0


if __name__ == "__main__":
    print(running())
    print(progress())
