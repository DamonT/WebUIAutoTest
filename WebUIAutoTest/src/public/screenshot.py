# coding=utf-8

import win32gui
from PIL import ImageGrab
from typing import NoReturn


def screenshot(filepath: str) -> NoReturn:
    hwnd = win32gui.GetForegroundWindow()
    # Returns the rectangle for a window in screen coordinates
    # (left, top, right, bottom) = GetWindowRect(hwnd)
    bbox = win32gui.GetWindowRect(hwnd)
    img = ImageGrab.grab(bbox=bbox)
    img.save(filepath)


if __name__ == "__main__":
    pass
