# coding=utf-8

from datetime import datetime
import os

from src import Config


# 报告相关信息
class ReportInfo:
    time = datetime.now().strftime("%Y%m%d_%H%M%S")
    report_name = "ResultReport_%s" % time  # 报告名
    report_fullname = report_name + ".html"  # 报告文件名 格式为.html
    report_parent_dir = os.path.join(Config.project_dir, "Report", report_name)  # 报告文件所在的报告目录，目录名与报告名一致
    report_path = os.path.join(report_parent_dir, report_fullname)  # 同名目录下创建同名报告


# 重制后报告信息
class ReMakeReportInfo:
    start_time = " "
    stop_time = " "
    success_count = 0
    failure_count = 0
    skip_count = 0
    error_count = 0
    title = " "
    description = " "
    test_method_results = []  # 元素为每个测试方法的执行结果
    test_plan_results = []  # 元素为每个测试计划的执行结果
    responsible_person_of_failure_or_error_result = []  # 执行结束后所有失败或错误案例对应的责任人
