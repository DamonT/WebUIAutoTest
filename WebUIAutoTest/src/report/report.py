# coding=utf-8

from typing import List
import os

from src.config.config import Config


def get_all_reports() -> List:
    """
         获取当前Report目录下所有报告文件，按照文件名日期降序排序
    """
    report_dir = os.path.join(Config.project_dir, 'Report')

    report_files = []
    for report_file in os.listdir(report_dir):
        if report_file[:12] == 'ResultReport' and os.path.isdir(os.path.join(report_dir, report_file)):
            report_files.append(report_file)
    return sorted(report_files, reverse=True)
