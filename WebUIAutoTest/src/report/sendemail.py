# coding=utf-8

import smtplib
from email.mime.text import MIMEText  # 正文文本
from email.mime.multipart import MIMEMultipart
from email.header import Header
import traceback
import sys
import os
from typing import NoReturn, Optional

from src.log.log import Log
from src.config.config import Config
from src.report.reporthtml import ReportHtml
from src.public.public import ParseXml
from src.dataclass.report.report import ReMakeReportInfo

logger = Log()

USER = Config.smtp_login_name  # SMTP服务器登录名
PASSWORD = Config.smtp_login_passwd  # SMTP服务器登录密码
SENDER_ADDRESS = Config.sender_address  # 发件地址
RECEIVERS = Config.receiver_address  # 收件地址
SMTP_SERVER = Config.smtp_server_address  # SMTP服务器地址
SMTP_PORT = Config.smtp_port  # SMTP服务器地址端口
RESPONSIBLE_PERSON_EMAIL = Config.responsible_person_email  # 责任人姓名邮箱

HTML = ""
HTML_HEAD_TITLE = ""
HTML_BODY_TABLE = ""
HTML_BODY_TABLE_TBODY_TR = ""


def exit(errmsg: str) -> NoReturn:
    logger.error(errmsg)
    os.system("pause")
    sys.exit(0)


def get_html_text(attch_name: str, info: ReMakeReportInfo) -> str:
    """
    说明：将报告信息已html文本返回
    :param attch_name: 附件名
    :param info: 报告信息，用于邮件正文展示关键信息
    :return: html文本
    :rtype: str
    """
    global HTML, HTML_HEAD_TITLE, HTML_BODY_TABLE, HTML_BODY_TABLE_TBODY_TR
    xml = ParseXml(filepath=os.path.join(Config.project_dir, "automation.data"))
    build_number = xml.get_value_from_xpath(xpath="./BuildData/BuildCount", attribute="count")
    test_plan_results = info.test_plan_results
    total_success_count = 0
    total_failure_count = 0
    total_error_count = 0
    total_skip_count = 0
    total_count = 0
    for _results in test_plan_results:
        test_plan_name = _results[0].file_name
        success_count = 0
        failure_count = 0
        error_count = 0
        skip_count = 0
        count = 0
        for _res in _results:
            success_count += _res.success_count
            failure_count += _res.failure_count
            error_count += _res.error_count
            skip_count += _res.skip_count
        count = success_count + failure_count  + error_count + skip_count
        HTML_BODY_TABLE_TBODY_TR += ReportHtml.HTML_BODY_TABLE_TBODY_TR % {
            'TEST_PLAN_NAME': test_plan_name,
            'TEST_PLAN_CASE_COUNT': count,
            'TEST_PLAN_SUCCESS_COUNT': success_count,
            'TEST_PLAN_FAILURE_COUNT': failure_count,
            'TEST_PLAN_ERROR_COUNT': error_count,
            'TEST_PLAN_SKIP_COUNT': skip_count,
            'TEST_PLAN_SUCCESS_RATE': 0 if count == 0 else success_count / count,
            'TEST_PLAN_FAILURE_RATE': 0 if count == 0 else failure_count / count,
            'TEST_PLAN_ERROR_RATE': 0 if count == 0 else error_count / count,
            'TEST_PLAN_SKIP_RATE': 0 if count == 0 else skip_count / count,
        }
        total_success_count += success_count
        total_failure_count += failure_count
        total_error_count += error_count
        total_skip_count += skip_count
    total_count = total_success_count + total_failure_count + total_error_count + total_skip_count

    HTML_HEAD_TITLE = ReportHtml.HTML_HEAD_TITLE % {
        'BUILD_NUMBER': build_number,
    }

    HTML_BODY_TABLE = ReportHtml.HTML_BODY_TABLE % {
        'BUILD_STATUS': '构建成功',
        'PROJECT_NAME': 'Web自动化项目',
        'BUILD_NUMBER': build_number,
        'CAUSE': '手工触发',
        'HTML_BODY_TABLE_TBODY_TR': HTML_BODY_TABLE_TBODY_TR,
        'CASE_COUNT': total_count,
        'SUCCESS_COUNT': total_success_count,
        'FAILURE_COUNT': total_failure_count,
        'ERROR_COUNT': total_error_count,
        'SKIP_COUNT': total_skip_count,
        'SUCCESS_RATE': 0 if total_count == 0 else total_success_count / total_count,
        'FAILURE_RATE': 0 if total_count == 0 else total_failure_count / total_count,
        'ERROR_RATE': 0 if total_count == 0 else total_error_count / total_count,
        'SKIP_RATE': 0 if total_count == 0 else total_skip_count / total_count,
        'ATTACHED_FILE': attch_name,
    }

    HTML = ReportHtml.HTML % {
        'HTML_HEAD_TITLE': HTML_HEAD_TITLE,
        'HTML_BODY_TABLE': HTML_BODY_TABLE,
    }
    return HTML


def send_email(attch_path: str, info: ReMakeReportInfo, flag: Optional[str] = None) -> NoReturn:
    """
    说明：
        发送邮件
    :param attch_path: 附件文件路径
    :param info: 报告信息，用于邮件正文展示关键信息
    :param flag: 邮件类型
            RESPONSIBLE_PERSON_EMAIL - 发送给责任人
    """
    global HTML
    if attch_path:
        attch_name = os.path.basename(attch_path)
        attch_file = [
            {"path": attch_path, "name": attch_name}
        ]  # 添加多个附件
    else:
        attch_file = False
        attch_name = "无附件"

    message = MIMEMultipart()  # 创建一个带附件的实例
    # 下面三个为邮件发送时显示的发件人、收件人和邮件主题
    message["From"] = SENDER_ADDRESS  # 发件地址
    if flag == 'RESPONSIBLE_PERSON_EMAIL':
        for _key in list(RESPONSIBLE_PERSON_EMAIL.keys()):
            if _key not in info.responsible_person_of_failure_or_error_result:
                RESPONSIBLE_PERSON_EMAIL.pop(_key)
        receivers = list(filter(lambda x: x not in RECEIVERS, RESPONSIBLE_PERSON_EMAIL.values()))
        if not receivers:
            return
    else:
        receivers = RECEIVERS
    message["To"] = ",".join(receivers)  # 收件地址 字符串 多个地址用逗号隔开
    message["Subject"] = Header(Config.email_title, "utf-8")  # 邮件主题
    # 三个参数：第一个为文本内容，第二个 plain or html 设置文本格式，第三个 utf-8 设置编码
    if flag == 'RESPONSIBLE_PERSON_EMAIL':
        html_text = HTML
    else:
        html_text = get_html_text(attch_name, info)
    message.attach(MIMEText(html_text, 'html', 'utf-8'))  # 正文内容 Html格式
    # message.attach(MIMEText("test", 'plain', 'utf-8'))  # 正文内容 plain 文本格式

    # 构造附件
    if attch_file:
        for item in attch_file:
            att = MIMEText(open(item["path"], "rb").read(), "base64", "utf-8")  # 创建二进制流
            att["Content-Type"] = "application/octet-stream"
            # Content-Type，内容类型，一般是指网页中存在的Content-Type，
            # 用于定义网络文件的类型和网页的编码，决定浏览器将以什么形式、什么编码读取这个文件，
            # 这就是经常看到一些Asp网页点击的结果却是下载到的一个文件或一张图片的原因。
            # (引用:http://www.runoob.com/http/http-content-type.html)
            att.add_header(
                "Content-Disposition",
                "attachment",
                filename=("utf-8", "", item["name"]),
            )
            message.attach(att)  # 添加附件

    # 连接SMTP服务器
    smtp_obj = None
    try:
        logger.info("开始连接SMTP服务器...")
        # 这里填写发送邮件的SMTP服务器地址 返回<class 'smtplib.STMP'>
        if Config.smtp_ssl:  # SSL/TLS加密的SMTP的默认端口是465
            smtp_obj = smtplib.SMTP_SSL(SMTP_SERVER, SMTP_PORT)
        else:
            smtp_obj = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
        info_msg = smtp_obj.ehlo()  # 查看是否连接服务器成功 返回一个元祖，第一个元素为应答号 250表示链接SMTP服务器成功
        if info_msg[0] == 250:
            logger.info("连接SMTP服务器成功...")
        else:
            logger.error("连接SMTP服务器失败...\n" + "详细信息:\n" + str(info_msg))
    except Exception:
        logger.error("连接SMTP服务器失败...\n" + "详细信息:\n" + traceback.format_exc())

    # 开始TLS加密 (一般为587端口)
    if Config.smtp_tls:
        smtp_obj.starttls()  # 使用TLS加密传输必须使用此方法，否则无法登陆

    # 登录SMTP服务器
    try:
        logger.info(USER + "开始登录SMTP服务器...")
        # 注意： 一般情况下登录地址为邮箱，但不排除非邮箱地址登录的情况。
        login_msg = smtp_obj.login(user=USER, password=PASSWORD)  # 返回元祖 一个元素应答 235表示成功
        if login_msg[0] == 235:
            logger.info(USER + "登录SMTP服务器成功...")
        else:
            logger.error("登录SMTP服务器失败...\n" + "详细信息:\n" + login_msg)
    except Exception:
        logger.error("登录SMTP服务器失败...\n" + "详细信息:\n" + traceback.format_exc())

    # 发送邮件
    try:
        logger.info("开始发送邮件...")
        smtp_obj.sendmail(
            from_addr=SENDER_ADDRESS, to_addrs=receivers, msg=message.as_string()
        )
        smtp_obj.quit()
        logger.info("邮件发送成功...")
    except Exception:
        logger.error("邮件发送失败，详细信息:\n" + "详细信息:\n" + traceback.format_exc())
