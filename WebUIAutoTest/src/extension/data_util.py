# coding=utf-8

"""
在这里定义类
类中的方法将在启动案例时自动加载到TestCaseMore类方法与属性中。
这样我们就可以在案例中使用 "self.func1(*args, **kwargs)"格式调用插件中的方法
需要注意的是，如果插件中定义的方法或属性与TestCaseMore类及其父类方法或属性重名，则会有警告日志提示该方法或属性重名
"""


class DataUtil:

    def func1(self):
        pass
