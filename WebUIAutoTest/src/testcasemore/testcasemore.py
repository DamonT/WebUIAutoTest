# coding=utf-8

import traceback
import os
import random
from typing import Union, Iterable, NoReturn, Tuple
import pathlib
import importlib
import unittest
from selenium.common.exceptions import NoSuchElementException

from src.newselenium.driver import Driver
from src.newselenium.element import Element
from src.newselenium.by import By
from src.public.public import Public
from src.public import g
from src.exception.seleniumexecption import SeleniumTypeError
from src.config.config import Config
from src.database.db import DB
from src.database.compare import Compare
from src.log.log import Log


logger = Log()


class TestCaseMixIn(Public):
    """用于增强unittest.TestCase功能"""
    pass


class TestCaseMore(unittest.TestCase, TestCaseMixIn):
    """用于增强unittest.TestCase功能，且方便用例编写直接继承TestCaseMore"""

    IMPORT_FLAG = False

    def checkElement(self, by: str = By.ID, value: str = None) -> Union[Element, bool]:
        """
        说明：
            检查元素是否存在，如果存在则返回该元素对象，如果不存在则校验失败并在报告中体现
        :param by:
        :param value:
        :return: 存在则返回元素对象
        """
        if isinstance(self.driver, Driver):
            try:
                return self.driver.get_element(by=by, value=value)
                # self.driver.driver.find_element(by=by, value=value)
            except NoSuchElementException:
                # 2020-6-3 调整失败抛出方式
                # self.assertTrue(
                #     False, "未找到该元素，%s" % ("方式：" + str(by) + " 值：" + str(value))
                # )
                raise self.failureException("未找到该元素，%s" % ("方式：" + str(by) + " 值：" + str(value)))
                # self.verificationErrors.append("未找到该元素，%s" % ("方式：" + str(by) + " 值：" + str(value)))
        else:
            raise SeleniumTypeError("self.driver 对象类型不对，应为Driver类，请检查...")

    def screen_shot(self) -> NoReturn:
        """
        说明：
            截图并会在测试报告中体现
            单独调试TestCase时，可以在“\Report\tempimg\”目录下查看截图
        """
        s = traceback.extract_stack()
        project_dir = Config.project_dir
        random_int = random.randint(10000000, 99999999)  # 随机数
        # 图片名称 需要拼上测试案例相关信息
        screen_shot_name = (self.__module__
            + "."
            + self.__class__.__name__
            + "."
            + s[-2][2]
            + "_"
            + str(random_int)
            + ".png")
        path = os.path.join(
            project_dir,
            "report",
            "tempimg",
            screen_shot_name,
        )
        # 截图保存
        self.driver.get_screen_shot(path=path)
        g.SCREENSHOTS_NAME.append(screen_shot_name)

    def dbcheck(self, sql: Union[str, Iterable[str]]) -> bool:
        """
        说明：
            执行数据比对

        :param sql: 需要比对的sql语句

        :return: True or False <class 'bool'>
        """
        except_path, actual_path = self._get_compare_filename()
        except_exist = os.path.exists(except_path)
        if not except_exist:
            DB().expectation(sql=sql,
                             excel_path=actual_path,)
            return False
        else:  # 存在期望结果则开始比较
            DB().expectation(sql=sql,
                             excel_path=actual_path, )
            return Compare(expected=except_path, actual=actual_path).compare()

    def _get_compare_filename(self) -> Tuple[str, str]:
        _module = self.__module__
        _module = _module.replace(".", os.sep)
        case_filename = os.path.realpath(_module + ".py")
        _dir = os.path.dirname(case_filename)  # case案例所在目录
        _name = ( "["
                + self.__module__
                + "."
                + self.__class__.__name__
                + "."
                + self._testMethodName
                + "]")
        except_path = os.path.join(_dir, _name) + "_[期望结果].xlsx"
        actual_path = os.path.join(_dir, _name) + "_[执行结果].xlsx"
        return except_path, actual_path


def load_extension(path):
    """加载插件到TestCaseMore中"""
    ignore_method_name = ['__module__', '__dict__', '__weakref__', '__doc__']
    p = pathlib.Path(path)
    if p.is_dir():
        for pyfile in list(p.rglob('*.py')):
            if pyfile.name == '__init__.py':
                continue
            else:
                relative_path = pyfile.relative_to(Config.project_dir)
                relative_import = str(relative_path).replace(os.sep, '.').split('.')[:-1]
                relative_import = '.'.join(relative_import)
                module = importlib.import_module(relative_import)
                for module_attr_name in dir(module):
                    module_attr = getattr(module, module_attr_name)
                    if isinstance(module_attr, type):
                        _class = module_attr
                        # for class_attr_name in dir(_class):
                        for class_attr_name in _class.__dict__:  # 只从当前类的成员获取，而dir会把父类所有属性获取。因此插件中的类最好不要继承父类
                            if class_attr_name in ignore_method_name:
                                continue
                            class_attr = getattr(_class, class_attr_name)
                            if callable(class_attr) and class_attr_name not in dir(TestCaseMore):
                                exec('TestCaseMore.%s = class_attr' % class_attr_name)
                            else:
                                logger.warning('插件 %s 中的方法 %s 与 %s 中已存在的属性名冲突，会导致插件中该方法无效，'
                                               '请修改属性名' % (_class, class_attr_name, 'TestCaseMore'))


# 解析extension包加载插件中的方法
if not TestCaseMore.IMPORT_FLAG:
    import src.extension as modul_extension
    try:
        for path in modul_extension.__path__:
            load_extension(path)
    except Exception as e:
        logger.error('测试用例构建失败。报错信息:\n' + traceback.format_exc())
        raise RuntimeError('插件加载失败') from e
    else:
        TestCaseMore.IMPORT_FLAG = True
