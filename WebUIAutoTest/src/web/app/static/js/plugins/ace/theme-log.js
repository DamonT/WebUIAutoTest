/*
* 自定义日志文本高亮规则
*
* copy from theme-github.js
* */

define("ace/theme/log", function (require, exports, modules) {
    exports.isDark = !1;
    exports.cssClass = "ace-log";
    exports.cssText =
        '.ace-log .ace_gutter {background: #e8e8e8;color: #AAA;}' +
        '.ace-log  {background: #fff;color: #000;}' +
        '.ace-log .ace_keyword {font-weight: bold;}' +
        '.ace-log .ace_string {color: #D14;}' +
        '.ace-log .ace_variable.ace_class {color: teal;}' +
        '.ace-log .ace_constant.ace_numeric {color: #099;}' +
        '.ace-log .ace_constant.ace_buildin {color: #0086B3;}' +
        '.ace-log .ace_support.ace_function {color: #0086B3;}' +
        '.ace-log .ace_comment {color: #998;font-style: italic;}' +
        '.ace-log .ace_variable.ace_language  {color: #0086B3;}' +
        '.ace-log .ace_paren {font-weight: bold;}' +
        '.ace-log .ace_boolean {font-weight: bold;}' +
        '.ace-log .ace_string.ace_regexp {color: #009926;font-weight: normal;}' +
        '.ace-log .ace_variable.ace_instance {color: teal;}' +
        '.ace-log .ace_constant.ace_language {font-weight: bold;}' +
        '.ace-log .ace_cursor {color: black;}' +
        '.ace-log .ace_focus .ace_marker-layer .ace_active-line {background: rgb(255, 255, 204);}' +
        '.ace-log .ace_marker-layer .ace_active-line {background: rgb(245, 245, 245);}' +
        '.ace-log .ace_marker-layer .ace_selection {background: rgb(181, 213, 255);}' +
        '.ace-log .ace_multiselect .ace_selection.ace_start {box-shadow: 0 0 3px 0px white;}' +
        '.ace-log .ace_nobold .ace_line > span {font-weight: normal !important;}' +
        '.ace-log .ace_marker-layer .ace_step {background: rgb(252, 255, 0);}' +
        '.ace-log .ace_marker-layer .ace_stack {background: rgb(164, 229, 101);}' +
        '.ace-log .ace_marker-layer .ace_bracket {margin: -1px 0 0 -1px;border: 1px solid rgb(192, 192, 192);}' +
        '.ace-log .ace_gutter-active-line {background-color : rgba(0, 0, 0, 0.07);}' +
        '.ace-log .ace_marker-layer .ace_selected-word {background: rgb(250, 250, 255);border: 1px solid rgb(200, 200, 250);}' +
        '.ace-log .ace_invisible {color: #BFBFBF}' +
        '.ace-log .ace_print-margin {width: 1px;background: #e8e8e8;}' +
        '.ace-log .ace_indent-guide {background: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAACCAYAAACZgbYnAAAAE0lEQVQImWP4////f4bLly//BwAmVgd1/w11/gAAAABJRU5ErkJggg==") right repeat-y;}' +
        // theme-log 添加
        '.ace-log .ace_error {color: #D14;}' +
        '.ace-log .ace_warn {color: #ffa500;}' +
        '';
    var dom = require("../lib/dom");
    dom.importCssString(exports.cssText, exports.cssClass)
});
