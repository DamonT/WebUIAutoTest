/*
* 自定义日志文本高亮规则
* copy from mode-abc.js
*
* 可参考: https://ace.c9.io/tool/mode_creator.html
* */

define("ace/mode/log_highlight_rules", function (require, exports, modules) {
    "use strict";

    var oop = require("../lib/oop");
    var TextHighlightRules = require("./text_highlight_rules").TextHighlightRules;
    var LogHighlightRules = function () {
        // [2021-04-29 17:43:42.930295] [INFO]
        this.$rules = {
            start: [{
                token: 'error',
                regex: /\[\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}.\d{6}\] \[(ERROR|CRITICAL)\]/,
                comment: 'Instruction Comment',
                next: 'error_log'
            }, {
                token: 'warn',
                regex: /\[\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}.\d{6}\] \[(WARNING|WARN)\]/,
                comment: 'Instruction Comment',
                next: 'warn_log'
            },],
            error_log: [{
                token: "",
                regex: /(?=\[\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}.\d{6}\] \[(DEBUG|INFO|WARN|WARNING|ERROR|CRITICAL)\])/,  // ?= 匹配之前的内容
                next: "start"
            }, {
                defaultToken: "error"
            }],
            warn_log: [{
                token: "",
                regex: /(?=\[\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}.\d{6}\] \[(DEBUG|INFO|WARN|WARNING|ERROR|CRITICAL)\])/,  // ?= 匹配之前的内容
                next: "start"
            }, {
                defaultToken: "warn"
            }],
        };
        this.normalizeRules();
    };
    oop.inherits(LogHighlightRules, TextHighlightRules);
    exports.LogHighlightRules = LogHighlightRules;
});

define("ace/mode/log", function (require, exports, modules) {
    "use strict";
    var oop = require("../lib/oop");
    var TextMode  = require("./text").Mode;
    var s = require("./log_highlight_rules").LogHighlightRules;
    var Mode = function () {
            this.HighlightRules = s;
        };
    oop.inherits(Mode, TextMode);

    (function () {
        this.$id = "ace/mode/log";
    }).call(Mode.prototype);

    exports.Mode = Mode;
});
