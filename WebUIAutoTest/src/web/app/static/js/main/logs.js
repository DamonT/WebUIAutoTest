$(document).ready(function () {

    // 渲染Editor
    ace_execution_log_editor = ace.edit('div-ace-execution-log');
    ace_execution_log_editor.setReadOnly(true);
    ace_execution_log_editor.setTheme("ace/theme/log");
    ace_execution_log_editor.session.setMode("ace/mode/log");

});

let ace_execution_log_editor = null;

function clearAndShowLog(which) {
    ace_execution_log_editor.session.setValue('');
    showLog(which, 1);
}

function showLog(which, rowNum) {
    let log = $(which).attr('id');
    $.ajax({
        type: 'POST',
        url: '/logs',
        data: {
            log: log,
            row_num: rowNum - 1,
        },
        success: function (data, textStatus, jqXHR) {
            if (data.error_no === 0){
                ace_execution_log_editor.session.insert({row:rowNum, column:0}, data.content);
                rowNum = ace_execution_log_editor.session.getLength();  // 下次日志更新插入位置行数
                if (! data.done){
                    // 递归继续读取日志
                    showLog(which, rowNum);
                }
            }else{
                message('日志文件读取失败: ' + data.error_msg, 'error');
            }
        },
    });
}
