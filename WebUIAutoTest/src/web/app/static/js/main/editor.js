var copyMoveCount = 0;  // 节点copy or move 次数
var DEFAULT_ZTREEPLAN = "<ul id=\"treePlan\" class=\"ztree\" build-hash=\"\" style=\"-moz-user-select: none; display: none;\"></ul>";

var setting_case = {
    edit: {
        enable: true,
        showRemoveBtn: false,
        showRenameBtn: false,
        drag: {
            // isCopy: true isMove: false 每次从案例目录推拽到执行计划都是复制
            isCopy: true,
            isMove: false,
            // 禁止从执行计划拖拽到案例目录中
            prev: false,
            next: false,
            inner: false,
        }
    },
    data: {
        simpleData: {
            enable: true
        }
    },
    callback: {
        beforeDrag: true,
        beforeDrop: beforeDrop,
        onDrop: onDrop,
        onDblClick: onDblClickForCase,
    }
};

var setting_plan = {
    view: {
        addHoverDom: addHoverDom,
        removeHoverDom: removeHoverDom,
        selectedMulti: false,
        fontCss: setFontCss,
    },
    edit: {
        enable: true,
        showRemoveBtn: true,
        showRenameBtn: true,
        drag: {
            isCopy: true,
            isMove: true,
            inner: dropInner,
            prev: dropPrev,
            next: dropNext,
        },
        removeTitle: '删除',
        renameTitle: '重命名',
    },
    data: {
        simpleData: {
            enable: true
        },
        keep: {
			parent: true
		},
    },
    check: {
        enable: true,
    },
    callback: {
        beforeDrag: beforeDrag,
        beforeDrop: beforeDrop,
        onDrag: onDrag,
        onDrop: onDrop,
        onRemove: zTreeOnRemove,
        beforeRemove: zTreeBeforeRemove,
        onRename: zTreeOnRename,
    }
};

var setting_build = {
    view: {
        addHoverDom: addHoverDomForBuild,
        removeHoverDom: removeHoverDomForBuild,
    },
    edit: {
        enable: true,
        showRemoveBtn: true,
        showRenameBtn: true,
        removeTitle: '删除',
        renameTitle: '重命名',
        drag: {
            isCopy: false,
            isMove: true,
            prev: dropPrevForBuild,
            next: dropNextForBuild,
            inner: false,
        }
    },
    data: {
        simpleData: {
            enable: true
        }
    },
    check: {
        enable: true,
    },
    callback: {
        beforeClick: beforeClickForBuild,
        onClick: onClickForBuild,
        beforeRemove: beforeRemoveForBuild,
        onRemove: onRemoveForBuild,
        onRename: onRenameForBuild,
    }
};

function getZTreePlanObjByBuildHash(buildHash) {
    var treePlanId = $('#treePlan-div .ztree[build-hash=\'%buildHash%\']'.replace('%buildHash%', buildHash)).attr('id');
    var zTreePlan = $.fn.zTree.getZTreeObj(treePlanId);
    return zTreePlan;
}

function getZTreeBuildObj() {
    var zTreeBuildObj = $.fn.zTree.getZTreeObj('treeBuild');
    return zTreeBuildObj;
}

function zTreeOnRename(event, treeId, treeNode, isCancel) {
    if (treeNode.isRoot){
        var buildName = treeNode.name;
        var buildHash = treeNode.buildHash;
        var zTreeBuildObj = getZTreeBuildObj();
        var buildNode = zTreeBuildObj.getNodeByParam('buildHash', buildHash);
        buildNode.name = buildName;
        zTreeBuildObj.updateNode(buildNode);
    }
}

function onRenameForBuild(event, treeId, treeNode, isCancel) {
    if (treeNode.node_type === 'build') {
        var buildName = treeNode.name;
        var buildHash = treeNode.buildHash;
        var zTreePlanObj = getZTreePlanObjByBuildHash(buildHash);
        var planRoot = zTreePlanObj.getNodeByParam('isRoot', true);
        planRoot.name = buildName;
        zTreePlanObj.updateNode(planRoot);
    }
}

function zTreeBeforeRemove(treeId, treeNode) {
    if (treeNode.isRoot){
        message('根节点不能删除', 'warning');
        return false;
    }else{
        return true;
    }
}

function removeTreeBuildNode(treeNode) {
    // 1.首先要删除treeBuild中指定删除的执行计划节点
    var zTree = $.fn.zTree.getZTreeObj("treeBuild");
    zTree.removeNode(treeNode, false);
    // 2.要删除对应treePlan执行计划树
    $('.ztree[build-hash=\'%build-hash%\']'.replace('%build-hash%', treeNode.buildHash)).remove();
    // 3.发请求删除数据库中的记录
}

function beforeRemoveForBuild(treeId, treeNode) {
    if (treeNode.isRoot){
        message('根节点不能删除', 'warning');
        return false;
    }else if(treeNode.node_type === 'build') {
        // 由于bootbox提供的组件都是非阻塞的方法（原生js.confirm是阻塞的）
        // 因此在else if 直接返回false（表示默认不删除节点）
        // 但在bootbox的回调函数里面根据选择结果"否"或"是"来调用zTree.removeNode方法来删除节点
        bootbox.confirm({ // 非阻塞
            message: "删除后不可恢复，确认删除当前执行计划？",
            buttons: {
                confirm: {
                    label: '是',
                    className: 'btn-success'
                },
                cancel: {
                    label: '否',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result){
                    removeTreeBuildNode(treeNode);
                }
            }
        });
        return false;
    }
}

function zTreeOnRemove(event, treeId, treeNode) {
    var parentNode = treeNode.getParentNode();
    if (parentNode !== null){
        if (parentNode.node_type === 'dir' || parentNode.isRoot){
            $('#' + parentNode.tId + '_ico').removeClass('ico_docu').addClass('ico_open');
        }
    }
}

function onRemoveForBuild(event, treeId, treeNode) {

}

function beforeDrag(treeId, treeNodes) {
    for (var i=0,l=treeNodes.length; i<l; i++) {
        if (treeNodes[i].drag === false) {
            return false;
        }
    }
    return true;
}

function beforeDrop(treeId, treeNodes, targetNode, moveType) {
    if (targetNode){
        if (targetNode.drop === false){
            return false
        }
        if (treeNodes[0].node_type === 'func' || treeNodes[0].node_type === 'class'){
            // 当拖拽的节点是函数和类时，只能在当前层级上下移动
            if (treeNodes[0].pId !== targetNode.pId){
                return false;
            }
        }
        // 当拖拽节点是文件和目录时，目标节点不能是类和函数（会禁止放到目标节点的同层前面或后面，或者目标节点里面）
        if (treeNodes[0].node_type === 'file' && 'class,func'.indexOf(targetNode.node_type) > -1){
            return false;
        }
        if (treeNodes[0].node_type === 'dir' && 'class,func'.indexOf(targetNode.node_type) > -1){
            return false;
        }
    }
    return true;
}

function dropPrev(treeId, treeNodes, targetNode) {
    return !targetNode.isRoot;
}

function dropPrevForBuild(treeId, treeNodes, targetNode) {
    if (targetNode.isRoot){
        return false;
    }
    for (var i=0,l=treeNodes.length; i<l; i++) {
        if (treeNodes[i].node_type !== 'build') {
            return false;
        }
    }
    return true;
}

function dropInner(treeId, treeNodes, targetNode) {
    // return targetNode? targetNode.isParent : false;
    if (targetNode){
        if (targetNode.isParent){
            // 被拖拽节点为类时，目标节点是类、函数、目录，则不能将拖拽节点放入目标节点里面
            if (treeNodes[0].node_type === 'class' && 'class,func,dir'.indexOf(targetNode.node_type) > -1) {
                return false;
            }
            // 同上
            if (treeNodes[0].node_type === 'func' && 'file,func,dir'.indexOf(targetNode.node_type) > -1) {
                return false;
            }
            // 同上
            if (treeNodes[0].node_type === 'file' && 'class,func,file'.indexOf(targetNode.node_type) > -1) {
                return false;
            }
            // 同上
            if (treeNodes[0].node_type === 'dir' && 'class,func,file'.indexOf(targetNode.node_type) > -1) {
                return false;
            }
            return true;
        }else{
            return false;
        }
    }else {
        return false;
    }
}

function dropNext(treeId, treeNodes, targetNode) {
    return !targetNode.isRoot;
}

function dropNextForBuild(treeId, treeNodes, targetNode) {
    if (targetNode.isRoot){
        return false;
    }
    for (var i=0,l=treeNodes.length; i<l; i++) {
        if (treeNodes[i].node_type !== 'build') {
            return false;
        }
    }
    return true;
}

var dragNodeParent = null;
function onDrag(event, treeId, treeNodes) {
    if (treeNodes.length > 0){
        dragNodeParent = treeNodes[0].getParentNode();
    }
}


function onDrop(event, treeId, treeNodes, targetNode, moveType, isCopy) {
    if (treeNodes.length > 0){
        if (dragNodeParent !== null) {
            dragNodeParent.isParent = true;
            $('#' + dragNodeParent.tId + '_ico').removeClass('ico_docu').addClass('ico_open');
        }
    }
    if (targetNode && moveType !== null){
        var pId = null;
        if (moveType === 'inner'){
            pId = targetNode.id;
        }else if (moveType === 'prev' || moveType === 'next'){
            pId = targetNode.pId;
        }
        recursiveHandleTidPid(treeNodes, pId); // 解决复制节点时tid与pid冲突导致保存和显示错位的问题
    }
}

function recursiveHandleTidPid(treeNodes, pId) {
    $.each(treeNodes, function (_key) {
        var node = treeNodes[_key];
        node.pId = pId;
        var hash = node.hash;
        copyMoveCount += 1;
        var id = hash + "_" + copyMoveCount;
        node.id = id;
        if (node.isParent){
            recursiveHandleTidPid(node.children, node.id);
        }
    });
}

// 根据treeNode找到根节点
function getRootNode(treeNode) {
    while (true) {
        if (treeNode.isRoot){
            return treeNode;
        }else {
            treeNode = treeNode.getParentNode();
        }
    }
}

var newCount = 1;
function addHoverDom(treeId, treeNode) {
    var sObj = $("#" + treeNode.tId + "_span");
    var addBtn = $("#addBtn_"+treeNode.tId);
    if (treeNode.editNameFlag || addBtn.length>0 || 'func,class,file'.indexOf(treeNode.node_type) > -1) {
        // 如果节点是py文件、类、方法则不显示添加按钮
        return;
    }
    var addStr = "<span class='button add' id='addBtn_" + treeNode.tId + "' title='新增节点' onfocus='this.blur();'></span>";
    sObj.after(addStr);
    var addBtn = $("#addBtn_"+treeNode.tId); // 再查一次
    if (addBtn) addBtn.bind("click", function(){
        var rootNode = getRootNode(treeNode);
        var zTreePlanObj = getZTreePlanObjByBuildHash(rootNode.buildHash);
        var addNode = {
            id:(100 + newCount),
            pId:treeNode.id,
            name:"新建文件夹" + (newCount++),
            isParent: true,
            node_type: 'dir',
        };
        zTreePlanObj.addNodes(treeNode, addNode);
        return false;
    });
}

var buildNewCount = 0;  // 构建计划中默认命名后缀次数(新建执行计划2/3/4)
function addHoverDomForBuild(treeId, treeNode) {
    var responsiblePerson = treeNode.responsiblePerson;
    var sObj = $("#" + treeNode.tId + "_span");
    var addBtn = $("#addBtn_"+treeNode.tId);
    var editResponsiblePersonBtn = $("#responsiblePersonBtn_"+treeNode.tId);
    // 设置责任人按钮
    if (treeNode.node_type === 'build' && editResponsiblePersonBtn.length === 0) {
        var editResponsiblePersonStr = "<span class='button responsible-person' id='responsiblePersonBtn_" + treeNode.tId + "' title='设置责任人'></span>";
        sObj.after(editResponsiblePersonStr);
        var editResponsiblePersonBtn = $("#responsiblePersonBtn_"+treeNode.tId);
        if (editResponsiblePersonBtn.length > 0) {
            editResponsiblePersonBtn.bind('click', function () {
                // 加载多选框
                var responsibleEditModal = $('#exec-plan-responsible-edit-modal');
                var responsiblePersonSelect = $('#exec-plan-responsible-edit-modal-select');
                var responsibleSelectLoading = $('#responsible-select-loading');
                responsibleSelectLoading.css('display', '');
                responsiblePersonSelect.prop('disabled', true);
                responsiblePersonSelect.children().remove();
                $.ajax({
                    type: 'POST',
                    url: '/editor/get_all_responsible_person',
                    data: {},
                    success: function (data, textStatus, jqXHR) {
                        if (data.error_no === 0){
                            var arr_responsible_person = data.responsible_person.split(',');
                            $.each(arr_responsible_person, function (index) {
                                var option = '<option>%value%</option>'.replace('%value%', arr_responsible_person[index]);
                                responsiblePersonSelect.append(option);
                                responsiblePersonSelect.removeAttr('disabled');
                                responsibleSelectLoading.css('display', 'none');
                            });
                            responsiblePersonSelect.selectpicker('refresh');  // 重新加载option
                            responsiblePersonSelect.selectpicker("val", responsiblePerson.split(','));
                        }else{
                            message('获取责任人信息配置失败: ' + data.error_msg, 'error');
                        }
                    },
                });
                responsibleEditModal.attr('build-hash', treeNode.buildHash);
                responsibleEditModal.modal();
            })
        }
    }
    // 添加文件夹按钮
    if (addBtn.length === 0 && treeNode.isRoot) {
        var addStr = "<span class='button add' id='addBtn_" + treeNode.tId + "' title='新增执行计划'></span>";
        sObj.after(addStr);
        var addBtn = $("#addBtn_" + treeNode.tId); // 再查一次
        if (addBtn.length > 0) {
            addBtn.bind("click", function () {
                var zTree = $.fn.zTree.getZTreeObj("treeBuild");
                buildNewCount++;
                var addNode = {
                    id: "新建执行计划" + buildNewCount,
                    pId: treeNode.id,
                    name: "新建执行计划" + buildNewCount,
                    isParent: false,
                    node_type: 'build',
                    buildHash: createHash(24),
                    responsiblePerson: ' ',
                    icon: '/static/icon/plan.png',
                };
                zTree.addNodes(treeNode, addNode);
            });
        }
    }
}


function removeHoverDom(treeId, treeNode) {
    $("#addBtn_"+treeNode.tId).unbind().remove();
}

function removeHoverDomForBuild(treeId, treeNode) {
    $("#addBtn_"+treeNode.tId).unbind().remove();
    $("#responsiblePersonBtn_"+treeNode.tId).unbind().remove();
}

function setFontCss(treeId, treeNode){
    if (treeNode.isRoot){
        return {}
    }else{
        if ('file,class,func'.indexOf(treeNode.node_type) > -1){
            return treeNode.exist_flag ? {} : {'font-style':'italic', 'text-decoration':'line-through'};
        }else{
            return {}
        }
    }
}

function onDblClickForCase(event, treeId, treeNode) {
    if (treeNode.node_type === null) return;
    var path = '';
    while (true){
        if (treeNode.node_type === 'file'){
            path = treeNode.path;
            break
        }else{
            treeNode = treeNode.getParentNode();
        }
    }
    var url = "/coder?path=%path%".replace('%path%', path);
    window.open(encodeURI(url));  // encodeURI(url):修复由于url路径或查询字符串中有中文导致后台解析出现乱码的问题(IE浏览器)
}

function beforeClickForBuild(treeId, treeNode, clickFlag){
    return treeNode.node_type === 'build';
}

function onClickForBuild(event, treeId, treeNode){
    // 在构建计划注脚中展示被点击计划的其他信息
    buildTreeFooterShow(treeNode);
    var buildName = treeNode.name;
    var buildHash = treeNode.buildHash;
    var treePlanId = getDisplayTreePlan();
    var treePlan = $('#' + treePlanId);
    if (treePlan.attr('build-hash') === buildHash ){
        return;
    }else{
        // 先将当前treePlan隐藏
        treePlan.css('display', 'none');
        // 如果在treePlan-div中找到当前build-hash匹配的treePlan则直接将其显示出来
        var exist = false;
        $.each($('#treePlan-div').children(), function (index) {
            var ul = $('#treePlan-div').children()[index];
            if ($(ul).attr('build-hash') === buildHash){
                $(ul).css('display', '');
                exist = true;
                return false;  // 跳出each循环
            }
        });
        // 当不存在时则根据build-name去后台查询该treePlan数据，并展示在页面树中
        if (!exist){
            $('#treePlan-div').prepend(DEFAULT_ZTREEPLAN);
            $.ajax({
                type: 'POST',
                url: '/editor',
                data: {
                    kind: 'select',
                    buildName: buildName,
                    buildHash: buildHash,
                },
                success: function (data, textStatus, jqXHR) {
                    if (data.error_no === 0){
                        var zNodesCase = data.jsonCase;
                        var zNodesPlan = data.jsonPlan;
                        var zNodesBuild = data.jsonBuild;
                        initzTreePlan(zNodesPlan)
                    }else{
                        message('测试计划数据读取失败: ' + data.error_msg, 'error');
                    }
                },
            });
        }
    }

}

// 在面板的注脚中展示执行计划对应责任人信息
function buildTreeFooterShow(treeNode) {
    $('.panel-footer #exec-plan-name').text('[' + treeNode.name + ']');
    $('.panel-footer #responsible-info').text(treeNode.responsiblePerson);
}

// 获取当前显示的zTreePlan的id
function getDisplayTreePlan() {
    var treeId = null;
    $.each($('#treePlan-div').children(), function (index) {
        var ul = $('#treePlan-div').children()[index];
        if ($(ul).css('display') === 'block'){
            treeId = $(ul).attr('id');
            return false;  // 跳出each循环
        }
    });
    return treeId;
}

// 生成随机y-x之间的数字
function getRandom() {
    var x = 999999999;
    var y = 100000000;
    var rand = parseInt(Math.random()*(x-y+1)+y);
    return rand;
}

// 获取到ztreePlan树中最大的copyMoverCount值
function getMaxCopyMoveCount(num) {
    if (num > copyMoveCount){
        return num;
    }else{
        return copyMoveCount;
    }
}

// 初始化执行计划树
function initzTreePlan(zNodesPlan) {
    var spliceTreeId = getRandom();
    var treeId = 'treePlan_' + spliceTreeId;
    // #treePlan 是页面默认的id
    $("#treePlan").attr('id', treeId);
    $.fn.zTree.init($("#" + treeId), setting_plan, zNodesPlan);
    var zTreePlan = $.fn.zTree.getZTreeObj(treeId);
    var planRoot = zTreePlan.getNodeByParam('isRoot', true);
    copyMoveCount = getMaxCopyMoveCount(planRoot.copyMoveCount);
    $("#" + treeId).attr('build-hash', planRoot.buildHash).css('display', '');
}

// 初始化“案例目录”、“执行计划”、“构建计划”树
function initAllzTree(zNodesCase, zAllNodesPlan, zNodesBuild){
    $.fn.zTree.init($("#treeCase"), setting_case, zNodesCase);
    $.fn.zTree.init($("#treeBuild"), setting_build, zNodesBuild);
    $.each(zAllNodesPlan, function (index) {
        var zNodesPlan = zAllNodesPlan[index];
        var spliceTreeId = getRandom();
        var treeId = 'treePlan_' + spliceTreeId;
        // #treePlan 是页面默认的id
        // 如果存在默认的treePlan则先替换掉
        if ($("#treePlan").length > 0){
            $("#treePlan").attr('id', treeId);
        }else{
            // 新增一个ul, 并默认隐藏
            $('#treePlan-div').prepend(DEFAULT_ZTREEPLAN);
            $("#treePlan").attr('id', treeId);
        }
        $.fn.zTree.init($("#" + treeId), setting_plan, zNodesPlan);
        var zTreePlan = $.fn.zTree.getZTreeObj(treeId);
        var planRoot = zTreePlan.getNodeByParam('isRoot', true);
        copyMoveCount = getMaxCopyMoveCount(planRoot.copyMoveCount);
        $("#" + treeId).attr('build-hash', planRoot.buildHash);
    })
}

// 生成指定长度hash值
function createHash (hashLength) {
    if (!hashLength || typeof(Number(hashLength)) != 'number') {return;}
    var ar = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
    var hs = [];
    var hl = Number(hashLength);
    var al = ar.length;
    for (var i = 0; i < hl; i ++) {
        hs.push(ar[Math.floor(Math.random() * al)]);
    }
    return hs.join('');
}

// 保存执行以及构建计划数据到数据库中
function saveTreePlanToDB(zTreePlanObj, serialNo, version, checked, responsiblePerson) {
    var planRoot = zTreePlanObj.getNodeByParam('isRoot', true);
    var buildName = planRoot.name;
    var buildHash = planRoot.buildHash;
    if (planRoot.children && planRoot.children.length === 0){  // 如果清空了则重置copyMoveCount为0
        planRoot.copyMoveCount = 0;
    }else {
        planRoot.copyMoveCount = copyMoveCount;
    }
    var zTreeJson = zTreePlanObj.transformToArray(zTreePlanObj.getNodes());
    var copyzTreeJson = [];
    $.extend(true, copyzTreeJson, zTreeJson);  // deepcopy
    $.each(copyzTreeJson, function (_nodeJsonId) {
       $.each(copyzTreeJson[_nodeJsonId], function (_key) {
            if (_key === 'children'){
                delete copyzTreeJson[_nodeJsonId][_key]
            }
        });
    });
    $.ajax({
        type: 'POST',
        url: '/editor',
        data: {
            kind: 'save',
            buildName: buildName,
            buildHash: buildHash,
            serialNo: serialNo,
            version: version,
            checked: checked,
            responsiblePerson: responsiblePerson,
            json: JSON.stringify(copyzTreeJson),
        },
        success: function (data, textStatus, jqXHR) {
            if (data.error_no === 0){
                message('[%buildName%]保存成功'.replace('%buildName%', buildName), 'success');
            }else{
                message('[%buildName%]测试计划数据保存失败: '.replace('%buildName%', buildName) + data.error_msg, 'error');
            }
        },
    })
}

// 保存执行计划数据
function savePlan() {
    var version = (new Date()).valueOf();  // 将时间戳作为版本存入数据库
    // 获取treeBuild构建计划中的构建顺序和分别将对应的treePlan执行计划保存到数据库中
    var zTreeBuild = $.fn.zTree.getZTreeObj('treeBuild');
    var zTreeBuildNodes = zTreeBuild.getNodes()[0];
    if (zTreeBuildNodes.children && zTreeBuildNodes.children.length > 0){
        var count = 1;
        $.each(zTreeBuildNodes.children, function (index) {
            var buildHash = zTreeBuildNodes.children[index].buildHash;
            var checked = zTreeBuildNodes.children[index].checked; // zTreeBuild节点的勾选状态
            var responsiblePerson = zTreeBuildNodes.children[index].responsiblePerson;
            // 找到对应的treePlan
            var zTreePlanObj = getZTreePlanObjByBuildHash(buildHash);
            if (zTreePlanObj){
                // 如果找不到zTreeBuild节点对应的zTreePlan，则不保存
                saveTreePlanToDB(zTreePlanObj, count, version, checked, responsiblePerson);
                count++;
            }
        })
    }else{
        message('构建计划内容为空，请添加后再保存', 'warning');
    }
}

$('#exec-plan-responsible-edit-modal-edit-button').click(function () {
    var responsibleEditModal = $('#exec-plan-responsible-edit-modal');
    var responsiblePersonSelect = $('#exec-plan-responsible-edit-modal-select');
    var bulidHash = responsibleEditModal.attr('build-hash');
    var zTreeBuildObj = $.fn.zTree.getZTreeObj('treeBuild');
    var node = zTreeBuildObj.getNodeByParam('buildHash', bulidHash, null);
    var arrResponsoblePerson = responsiblePersonSelect.val();
    if (arrResponsoblePerson && arrResponsoblePerson.length > 0){
        node.responsiblePerson = arrResponsoblePerson.join(',');
    }else{
        node.responsiblePerson = ' ';
    }
    responsibleEditModal.modal('hide');
});


function upload() {
    var uploadModal = $('#upload-modal');
    uploadModal.modal();
    Dropzone.forElement("#myDropzone").removeAllFiles();
}


$(document).ready(function(){
    $.ajax({
        type: 'POST',
        url: '/editor',
        data: {
            kind: 'select',
            buildName: '',
            buildHash: '',
        },
        success: function (data, textStatus, jqXHR) {
            if (data.error_no === 0){
                var zNodesCase = data.jsonCase;
                var zAllNodesPlan = data.jsonPlan;  // 返回所有执行计划数据
                var zNodesBuild = data.jsonBuild;
                initAllzTree(zNodesCase, zAllNodesPlan, zNodesBuild)
            }else{
                message('测试案例目录读取失败: ' + data.error_msg, 'error');
            }
        },
    });

    // Dropzone插件初始化
    Dropzone.options.myDropzone = {
        init: function() {
            this.on("success",  function (file, response) {
                if (response.error_no === 0){
                    message('文件上传成功', 'success');
                }else{
                    message('文件上传失败: ' + response.error_msg, 'error');
                }
            });
            
            this.on("error", function (file, errorMessage, xhr) {
                message('文件上传失败:' + errorMessage, 'error');
            });
            
            this.on("sending", function (file, xhr, formData) {
                formData.append("fileSize", file.size);
                formData.append("type", "case");  // 用例
            });
        },
        accept: function (file, done) {
            if (file.name !== 'TestCases.zip'){
                done("文件名必须为TestCases.zip");
            }else{
                done();  // 会发起post请求上传
            }
        },
        // acceptedFiles: "image/*",
        addRemoveLinks: true,
        acceptedFiles: "application/zip,.zip",  // 上传文件类型（mime types or file extensions）
        clickable: true,  // 上传区域是否可以通过点击上传，默认true。如果false则表示只能拖拽上传。,
        dictDefaultMessage: `拖拽到这里或点击上传.`, // message display on drop area
        dictFallbackMessage: "Your browser does not support drag'n'drop file uploads.",
        dictInvalidFileType: "不能上传该类型文件, 只支持.zip后缀",
        dictFileTooBig: "文件大小 {{filesize}}Mib 超过最大限制 {{maxFilesize}}MiB.",
        dictResponseError: "Server error: {{statusCode}}",
        dictMaxFilesExceeded: "已超过最大上传数量限制 {{maxFiles}} 个.",
        dictCancelUpload: "取消上传",
        dictRemoveFile: "移除文件",
        dictCancelUploadConfirmation: "确定取消该文件上传？",
        dictUploadCanceled: "已取消上传",
        headers: {"X-CSRF-Token": csrf_token},  // custom options code
        maxFiles: 1,  // 允许上传文件数量
        maxFilesize: 128, // MB 单个文件大小限制
        parallelUploads: 1,  // How many file uploads to process in parallel
        paramName: "file", // The name that will be used to transfer the file
        uploadMultiple: false,
    };

});