$(document).ready(function () {
    var editor = null;

    require.config({ paths: { 'vs': '/static/js/plugins/monaco-editor/min/vs' } });
    require(['vs/editor/editor.main'], function () {
        editor = monaco.editor.create(document.getElementById('container'), {
            value: codeContent,
            language: 'python'
        });

    });

    function saveCode() {
        var modifiedCodeContent = editor.getValue();
        $.ajax({
            type: 'POST',
            url: '/coder/save_to_file',
            data: {
                path: filePath,
                content: modifiedCodeContent,
            },
            success: function (data, textStatus, jqXHR) {
                if (data.error_no === 0){
                    message('保存成功', 'success');
                }else{
                    message('保存py文件失败: ' + data.error_msg, 'error');
                }
            },
        });
    }

    function downloadCode() {
        download(editor.getValue(), fileName, "text/plain");
    }

    $('#save-code').bind('click', saveCode);
    $('#download-code').bind('click', downloadCode);


});
