// 指定图表的配置项和数据
var userBuildCountOption = {
    title: {
        text: '构建次数统计'
    },
    grid:{
        x:35,
        y:50,
        x2:25,
        y2:35,
    },
    tooltip: {},
    legend: {
        data:['构建次数']
    },
    dataset: {
        dimensions: ['date', 'count'],
        source: [
            {},
        ]
    },
    xAxis: {type: 'category'},
    yAxis: {},
    series: [{
        name: '构建次数',
        type: 'line',
        smooth: true,
    }]
};

var currentUserBuildCountChart = echarts.init(document.getElementById('echarts-build-count-user'));
var allUserBuildCountChart = echarts.init(document.getElementById('echarts-build-count-all'));

$(document).ready(function () {
    // 构建次数折线图
    currentUserBuildCountChart.setOption(userBuildCountOption);
    allUserBuildCountChart.setOption(userBuildCountOption);

    updateChartOptionDataset();
    
});

// 更新页面中所有图表数据
function updateChartOptionDataset() {
    var display = $('#echarts-build-count-user').parents('div.col-lg-4').css('display');
    if (display === 'block'){
        updateCurrentUserBuildCountOption();
    }
    updateAllUserBuildCountOption();
}

// 更新当前用户构建次数折线图数据
function updateCurrentUserBuildCountOption() {
    $.ajax({
        type: 'POST',
        url: '/index/get_build_count',
        data: {
            flag: 'current'
        },
        success: function (data, status, xhr) {
            if (data.error_no === 0){
                userBuildCountOption.dataset.source = data.data;
                currentUserBuildCountChart.setOption(userBuildCountOption);
                message('图表更新成功', 'success');
            }else {
                message('当前用户构建次数图表更新失败: ' + data.error_msg, 'error');
            }
        },
    });
}

// 更新所有用户构建次数折线图数据
function updateAllUserBuildCountOption() {
    $.ajax({
        type: 'POST',
        url: '/index/get_build_count',
        data: {
            flag: 'all'
        },
        success: function (data, status, xhr) {
            if (data.error_no === 0){
                userBuildCountOption.dataset.source = data.data;
                allUserBuildCountChart.setOption(userBuildCountOption);
                message('图表更新成功', 'success');
            }else {
                message('所有用户构建次数图表更新失败: ' + data.error_msg, 'error');
            }
        },
    });
}
