$(document).ready(function () {

    $('[data-toggle="popover"]').popover();

    $.ajax({
          type: 'POST',
          url: '/execution/exec_status',
          data: {},
          success: function (data, status, xhr) {
              if (data.error_no === 0){
                  if (data.status){
                      runningButtonStatus();
                  }else{
                      stoppedButtonStatus();
                  }
              }else {
                  message('状态读取失败: ' + data.error_msg, 'error');
              }
          },
    });

    // ------------------------------------------- 构建启停与状态查询 开始------------------------------------------------
    function runningButtonStatus() {
        $('button#start').attr('disabled', 'true');
        $('button#stop').removeAttr('disabled');
    }

    function stoppedButtonStatus() {
        $('button#start').removeAttr('disabled');
        $('button#stop').attr('disabled', 'true');
    }

    function startingProgressBarStatus() {
        $('#progress-bar-executing').css('width', '0%')
                                    .css('min-width', '0em')
                                    .removeClass('active')
                                    .data('progress', '0%')
                                    .attr('data-progress', '0%');
        $('#progress-bar-executing small').text('');

        $('#progress-bar-stopping').css('width', '0%')
                                   .removeClass('active');
        $('#progress-bar-stopping small').text('');

        $('#progress-bar-starting').css('width', '15%')
                                   .addClass('active');
        $('#progress-bar-starting small').text('启动中');
    }

    function startedProgressBarStatus() {
        $('#progress-bar-starting').css('width', '15%')
                                   .removeClass('active');
        $('#progress-bar-starting small').text('已启动');
    }

    function startFailedProgressBarStatus() {
        $('#progress-bar-starting').css('width', '0%')
                                   .removeClass('active');
        $('#progress-bar-starting small').text('');
    }

    // function executingProgressBarStatus(progress=0) {  // 老版本谷歌不支持该方式设置默认参数
    function executingProgressBarStatus(progress) {
        if (progress === undefined) progress = 0;
        startedProgressBarStatus();
        $('#progress-bar-starting small').text('');

        var width = String(Number(progress) * 0.7) + '%';
        $('#progress-bar-executing').css('min-width', '6em').css('width', width)
                                    .addClass('active')
                                    .data('progress', progress + '%')
                                    .attr('data-progress', progress + '%');
        $('#progress-bar-executing small').text('构建中 ' + progress + '%');
    }

    function stoppingProgressBarStatus() {
        $('#progress-bar-stopping').css('width', '15%')
                                   .addClass('active');
        $('#progress-bar-stopping small').text('停止中');
    }

    function stoppedProgressBarStatus() {
        $('#progress-bar-executing').removeClass('active');
        $('#progress-bar-executing small').text('构建停止 ' + $('#progress-bar-executing').data('progress'));

        $('#progress-bar-stopping').css('width', '15%')
                                   .addClass('')
                                   .removeClass('active');
        $('#progress-bar-stopping small').text('已停止');
    }

    function stopFailedProgressBarStatus() {
        $('#progress-bar-stopping').css('width', '15%')
                                   .addClass('');
        $('#progress-bar-stopping small').text('停止失败');
    }

    var startingFlag = false;  // 正在启动的状态，启动成功或失败后会更新为false

    function getExecStatus() {
        $.ajax({
            type: 'POST',
            url: '/execution/exec_status',
            data: {},
            success: function (data, status, xhr) {
                if (data.error_no === 0){
                    if (!startingFlag){  // 如果不是正在启动中才去更新
                        if (data.status){
                            runningButtonStatus();
                            executingProgressBarStatus(data.progress)
                        }else{
                            stoppedButtonStatus();
                            stoppedProgressBarStatus();
                        }
                    }
                }else {
                    message('状态读取失败: ' + data.error_msg, 'error');
                }
            },
        });
    }

    var getExecStatusTimer = setInterval(getExecStatus,1000);
    var getExecStatusTimerFlag = true;

    $('button#start').click(function () {
        startingFlag = true;
        startingProgressBarStatus();
        if (getExecStatusTimerFlag){
            clearInterval(getExecStatusTimer);
            getExecStatusTimerFlag = false;
        }
        $('button#start').attr('disabled', 'true');
        $.ajax({
            type: 'POST',
            url: "/execution",
            data: {
                flag: "start"
            },
            success: function (data, status, xhr) {
                if (data.error_no === 0){
                    runningButtonStatus();
                    startedProgressBarStatus();
                    message('启动成功', 'success');
                }else {
                    stoppedButtonStatus();
                    startFailedProgressBarStatus();
                    message('启动失败: ' + data.error_msg, 'error');
                }
            },
            complete: function(xhr, status){
                startingFlag = false;
                if (getExecStatusTimerFlag === false){
                    getExecStatusTimer = setInterval(getExecStatus,1000);
                    getExecStatusTimerFlag = true;
                }
            }
        });
    });

    $('button#stop').click(function () {
        stoppingProgressBarStatus();
        $.ajax({
            type: 'POST',
            url: '/execution',
            data: {
                flag: 'stop'
            },
            success: function (data, status, xhr) {
                if (data.error_no === 0){
                    stoppedButtonStatus();
                    stoppedProgressBarStatus();
                    message('停止成功', 'success');
                }else {
                    runningButtonStatus();
                    stopFailedProgressBarStatus();
                    message('停止失败: ' + data.error_msg, 'error');
                }
            },
        });
    });

    // ------------------------------------------- 构建启停与状态查询 结束------------------------------------------------

    // ------------------------------------------- 日志内容查询 开始-----------------------------------------------------

    // 渲染Editor
    let ace_execution_log_editor = ace.edit('div-ace-execution-log');
    ace_execution_log_editor.setReadOnly(true);
    ace_execution_log_editor.setTheme("ace/theme/log");
    ace_execution_log_editor.session.setMode("ace/mode/log");

    // 定时查询直到调度执行结束
    let rowNum = 0;
    let log = '';
    let _iIntervalID = setInterval(function() {
        rowNum = ace_execution_log_editor.session.getLength();  // 下次日志更新插入位置行数
        log = $('#div-ace-execution-log').data('log');
        $.ajax({
            type: 'POST',
            url: '/execution/get_log',
            data: {
                row_num: rowNum - 1,
                log: log,
            },
            success: function (data, status, xhr) {
                if (data.error_no === 0){
                    $('#div-ace-execution-log').data('log', data.log);
                    ace_execution_log_editor.session.insert({row:rowNum, column:0}, data.content);
                    // 当滚动条滚动到最底部时，插入日志都会进行自动滚动
                    if (ace_execution_log_editor.renderer.getScrollBottomRow() >= rowNum - 1){
                        ace_execution_log_editor.renderer.scrollToLine(999999);
                    }
                    // 后台日志文件发生变更
                    if (log !== '' && data.log !== log){
                        console.log('后台日志文件发生变更');
                        ace_execution_log_editor.session.setValue('');
                    }
                }else{
                    message('日志读取失败: ' + data.error_msg, 'error');
                }
            },
        });
    }, 3000);

    // ------------------------------------------- 日志内容查询 结束-----------------------------------------------------

});
