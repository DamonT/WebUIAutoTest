$(document).ready(function () {
    $('#save-frame').popover({
        html: true,
        placement: 'right',
        title: '请确认?',
        trigger: 'focus',
        content: function () {
            return "<p>点击确认后会立刻重载页面生效。</p>" +
                "<input id='submit' class='btn btn-primary' type='submit' value='确认' name='submit'>" +
                "<button type='button' class='btn btn-default' style='float:right' onclick='hidePopover(\"#save-frame\")'>取消</button>"
        }
    });
});


// 根据id隐藏指定popover
function hidePopover(id) {
    $(id).popover('hide');
}

