var $responsibleInfoTable = $('#table-responsible-info');
var $responsibleInfoTableDelete = $('#table-responsible-info-btn-delete');
var $responsibleInfoTableEdit = $('#table-responsible-info-btn-edit');


function ResponsibleInfoTable(){
    var table = new Object;

    table.init = function(){
        // 初始化参数option
        $responsibleInfoTable.bootstrapTable({
            url: '/config/get_responsible_info',       //请求后台的URL（*）
            method: 'post',                     //请求方式（*）
            toolbar: '#toolbar',                //工具按钮用哪个容器
            striped: true,                      //是否显示行间隔色
            cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
            pagination: true,                   //是否显示分页（*）
            sortable: false,                     //是否启用排序
            sortOrder: "asc",                   //排序方式
            // queryParams: table.queryParams,     //传递参数（*）
            sidePagination: "client",           //分页方式：client客户端分页，server服务端分页（*）
            pageNumber:1,                       //初始化加载第一页，默认第一页
            pageSize: 10,                       //每页的记录行数（*）
            pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
            search: true,                       //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
            strictSearch: true,                 // 严格搜索，非模糊搜素
            showColumns: true,                  //是否显示所有的列
            showRefresh: false,                 //是否显示刷新按钮
            minimumCountColumns: 2,             //最少允许的列数
            clickToSelect: true,                //是否启用点击选中行
            height: 500,                        //行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
            uniqueId: "ID",                     //每一行的唯一标识，一般为主键列
            showToggle:true,                    //是否显示详细视图和列表视图的切换按钮
            cardView: false,                    //是否显示详细视图
            detailView: false,                  //是否显示父子表
            locale: 'zh-CN',
            columns: [{
                checkbox: true
            }, {
                field: 'ID',
                title: '编号'
            }, {
                field: 'Name',
                title: '姓名'
            }, {
                field: 'EMailAddress',
                title: '邮箱地址'
            }, ]
        });

        // table 事件处理
        $responsibleInfoTable.on('check.bs.table uncheck.bs.table ' +
                                       'check-all.bs.table uncheck-all.bs.table',
        function () {
            $responsibleInfoTableDelete.prop('disabled', !$responsibleInfoTable.bootstrapTable('getSelections').length);
            $responsibleInfoTableEdit.prop('disabled', !$responsibleInfoTable.bootstrapTable('getSelections').length);
        });

        $responsibleInfoTable.on('all.bs.table', function (data, name) {
            // console.log(name);
        });
        
        $responsibleInfoTable.on('load-success.bs.table', function (data, status, jqXHR) {
            $responsibleInfoTableDelete.prop('disabled', !$responsibleInfoTable.bootstrapTable('getSelections').length);
            $responsibleInfoTableEdit.prop('disabled', !$responsibleInfoTable.bootstrapTable('getSelections').length);
        });

    };

    return table;
}

// 获取ResponsibleInfoTable选择的ID
function getResponsibleInfoTableIdSelections() {
    return $.map($responsibleInfoTable.bootstrapTable('getSelections'), function (row) {
        return row.ID
    })
}


// 获取ResponsibleInfoTable的最大主键ID的编号
function getResponsibleInfoTableMaxID(){
    var rows = $responsibleInfoTable.bootstrapTable('getData');
    var maxID = 0;
    $.each(rows, function (index) {
        if (rows[index].ID > maxID){
            maxID = rows[index].ID;
        }
    });
    maxID = maxID + 1;
    return maxID;
}

function ResponsibleInfoButton() {
    var button = new Object;

    button.init = function () {
        $('#table-responsible-info-btn-delete').click(function () {
            var selectedId = getResponsibleInfoTableIdSelections();
            $responsibleInfoTable.bootstrapTable('remove', {
                field: 'ID',
                values: selectedId,
            })
        });
        
        $('#table-responsible-info-btn-add').click(function () {
            $('#table-responsible-add-modal').modal();
        });

        $('#table-responsible-info-btn-edit').click(function () {
            var selectedId = getResponsibleInfoTableIdSelections();
            var row = $responsibleInfoTable.bootstrapTable('getRowByUniqueId', selectedId[0]);
            var name = row.Name;
            var emailAddree = row.EMailAddress;
            $('#edit-responsible-name').val(name);
            $('#edit-responsible-email-address').val(emailAddree);
            $('#table-responsible-edit-modal').modal();
        });

        $('#table-responsible-info-btn-save').click(function () {
           var rows = $responsibleInfoTable.bootstrapTable('getData');
           $.ajax({
                type: 'POST',
                url: '/config/save_responsible_info',
                data: {
                    rows: JSON.stringify(rows),
                },
                success: function (data, textStatus, jqXHR) {
                    if (data.error_no === 0){
                        message('保存成功', 'success');
                    }else{
                        message('保存失败: ' + data.error_msg, 'error');
                    }
                },
           })
        });
    };

    return button;
}

$('#table-responsible-add-modal-add-button').click(function () {
    var name = $('input#add-responsible-name').val();
    var address = $('input#add-responsible-email-address').val();
    $('#table-responsible-add-modal').modal('hide');
    $responsibleInfoTable.bootstrapTable('insertRow', {
        index: 99999999,
        row: {
            ID: getResponsibleInfoTableMaxID(),
            Name: name,
            EMailAddress: address,
        }
    });
});

$('#table-responsible-edit-modal-edit-button').click(function () {
    var selectedId = getResponsibleInfoTableIdSelections();
    var name = $('input#edit-responsible-name').val();
    var address = $('input#edit-responsible-email-address').val();
    $('#table-responsible-edit-modal').modal('hide');
    $responsibleInfoTable.bootstrapTable('updateByUniqueId', {
        id: selectedId[0],
        row: {
            Name: name,
            EMailAddress: address,
        }
    })
});

$(document).ready(function () {
    // 初始化Table
    var responsibleInfoTable = new ResponsibleInfoTable();
    responsibleInfoTable.init();

    // 初始化table上的按钮
    var responsibleInfoButton = new ResponsibleInfoButton();
    responsibleInfoButton.init();

});
