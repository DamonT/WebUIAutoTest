$(document).ready(function () {

    // 在ajax请求头中添加csrf_token保护
    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            // 没有跨域且类型不属于以下四种类型才添加csrf_token
            if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader('X-CSRFToken', csrf_token);
            }
        }
    });

    $(document).ajaxError(function (event, request, settings) {
        var default_error_message = '服务端出错，请稍后重试。';
        var _message = null;
        if (request.status === 0){
            message('后台服务拒绝，请稍后重试。', 'error');
            return
        }
        if (request.responseJSON && request.responseJSON.hasOwnProperty('ertooltipror_msg')) {
            _message = request.responseJSON.error_msg;
        } else if (request.responseText) {
            var IS_JSON = true;
            try {
                var data = JSON.parse(request.responseText);
            }
            catch (err) {
                IS_JSON = false;
            }
            if (IS_JSON && data !== undefined && data.hasOwnProperty('error_msg')) {
                _message = JSON.parse(request.responseText).error_msg;
            } else {
                _message = default_error_message;
            }
        } else {
            _message = default_error_message;
        }
        message(_message, 'error');
    });

    $('li').hover(
        function () {
            $(this).addClass('active');
        },
        function () {
            $(this).removeClass('active');
        }
    );

    // 自动滚动到错误控件位置
    var helpBlocks = $('.help-block');
    if (helpBlocks.length > 0){
        var firstHelpBlock  = helpBlocks.first();
        $('html,body').animate({scrollTop: firstHelpBlock.offset().top - 200 + "px"});
    }

    function render_time(){
        return moment($(this).data('timestamp')).format('LLL');
    }

    $("[data-toggle='tooltip']").tooltip(
        {title: render_time}
    );

    // 处理下拉选择框的默认值
    $('select').each(function () {
        var defaultValue = $(this).attr('default_value');
        $(this).children().each(function () {
            var value =$(this).attr('value');
            if ($.trim(value) === defaultValue){
                $(this).attr('selected', 'selected');
            }
        });
    });
    // 处理密码输入框的默认值
    $('input[type=password]').each(function () {
        var defaultValue = $(this).attr('default_value');
        $(this).attr('value', defaultValue);
    });

});

function message(msg, type) {
    type = type || 'info';
    toastr.options.closeButton = true;
    if (type === 'info'){
        toastr.info(msg);
    }
    else if (type === 'warning') {
        toastr.warning(msg);
    }
    else if (type === 'error') {
        toastr.error(msg);
    }
    else if (type === 'success') {
        toastr.success(msg);
    }

}

function displayPage(page, total_pages) {
    page = parseInt(page);
    total_pages = parseInt(total_pages);
    // 展示日志
    $('.list-group-item').css('display', 'none');
    $('.list-group-item').next('#open-report').css('display', 'none');
    $('.list-group-item[page='+page+']').css('display', '');
    $('.list-group-item[page='+page+']').next('#open-report').css('display', '');
    // 翻页按钮
    $('a[aria-label=Previous]').attr('prev_page', Number(page) - 1);
    $('a[aria-label=Next]').attr('next_page', Number(page) + 1);
    if (page > 1){
        $('#prev_li').removeClass('disabled');
        $('a[aria-label=Previous]').attr('onclick', 'turnPages(this)');
    }else{
        $('#prev_li').addClass('disabled');
        $('a[aria-label=Previous]').attr('onclick', '');
    }
    if (page < total_pages){
        $('#next_li').removeClass('disabled');
        $('a[aria-label=Next]').attr('onclick', 'turnPages(this)');
    }else{
        $('#next_li').addClass('disabled');
        $('a[aria-label=Next]').attr('onclick', '');
    }
    // 页按钮
    $.each($('.pagination').children('li'), function (index) {
        $('.pagination').children('li').removeClass('current');
    });
    $('a[page=%page%]'.replace('%page%', page)).parent().addClass('current');
    // 省略号按钮
    var display_flag = false;  // 当前页号对应的按钮是否在页面上显示
    $.each($('.pagination li a'), function (index) {
        var a = $('.pagination li a')[index];
        if ($(a).css('display') === 'block' && $(a).attr('page') === page.toString()) {
            display_flag = true;
        }
    });
    if (!display_flag){
        $('.pagination li#ellipsis a').text( '.' + page + '.');
    }else{
        $('.pagination li#ellipsis a').text( '...');
    }
}

function showPage(which) {
    var page = $(which).attr('page');
    var total_pages = $(which).attr('total_pages');
    displayPage(page, total_pages);
}

function turnPages(which) {
    var next_page = $(which).attr('next_page');
    var prev_page = $(which).attr('prev_page');
    var total_pages = $(which).attr('total_pages');
    if (next_page){
        displayPage(next_page, total_pages);
    }
    if (prev_page){
        displayPage(prev_page, total_pages);
    }
}