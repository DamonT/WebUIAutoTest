# coding=utf-8

from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_bootstrap import Bootstrap
from flask_login import LoginManager
from flask_mail import Mail
from flask_moment import Moment
from flask_wtf import CSRFProtect
from flask_caching import Cache


db = SQLAlchemy()
bootstrap = Bootstrap()
migrate = Migrate()
login_manager = LoginManager()
login_manager.login_view = "auth.login"  # 注册登录视图
login_manager.login_message_category = 'warning'
login_manager.login_message = "请先登录"
mail = Mail()
moment = Moment()
csrf = CSRFProtect()
cache = Cache()


@login_manager.user_loader
def load_user(id):
    from src.web.app.models import User
    return User.query.filter_by(id=int(id)).first()
