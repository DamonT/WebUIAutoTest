# coding=utf-8

from flask import render_template, current_app, session, request
from flask_wtf.csrf import CSRFError

from src.web.app.extensions import db
from src.web.app.errors import bp
from src.web.app.util.util import save_request_session_info


@bp.app_errorhandler(404)
def not_found_error(error):
    return render_template('/errors/404.html'), 404


@bp.app_errorhandler(500)
def internal_error(error):
    db.session.rollback()
    save_request_session_info()
    return render_template('/errors/500.html'), 500


@bp.app_errorhandler(CSRFError)
def csrf_error(error):
    save_request_session_info()
    return render_template('/errors/error.html', msg='csrf_token异常: %s' % error.description), 400
