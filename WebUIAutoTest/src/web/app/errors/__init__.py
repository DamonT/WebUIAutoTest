# coding=utf-8

from flask import Blueprint

bp = Blueprint('errors', __name__)

from src.web.app.errors import handlers
