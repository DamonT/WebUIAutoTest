# coding=utf-8

"""
Author: zhangzheng
Description: 用于处理TestCase目录下面的案例，将文件信息转化为字典存储
"""

import os
from typing import List, Dict
from pathlib import Path
import hashlib
import inspect
import unittest

from src.config.config import Config
from src.web.app.util.node import node_for_test_case


class HandleCase:

    TestCaseDir = os.path.join(Config.project_dir, 'TestCases')

    def __init__(self):
        pass

    def parse_dir(self) -> List[Dict]:
        """
        解析TestCases目录，将目录中的文件夹、test_*.py文件、py文件中的test_*方法使用 List[Dict] 存储
        :return:
        """
        # result = [{
        #     'id': self._get_hash(self.TestCaseDir),
        #     'pId': 0,
        #     'name': 'TestCases',
        #     'open': True,
        #     'path': self.TestCaseDir,  # 自定义
        #     'hash': self._get_hash(self.TestCaseDir), # 自定义
        #     'exist_flag': True,  # 自定义
        # }, ]
        result = [
            node_for_test_case(
                id=self._get_hash(self.TestCaseDir),
                pId=0,
                name='TestCases',
                open=True,
                path=self.TestCaseDir,
                hash=self._get_hash(self.TestCaseDir),
                exist_flag=True,
            ),
        ]

        def _recursion(p, parent_id):
            for p in p.iterdir():
                if p.is_file():
                    if p.suffix == '.py':
                        # 解析案例文件
                        if p.name == '__init__.py':  # 忽略非案例相关文件
                            continue
                        _path_f = p.__str__()
                        _relative_path_f = self._get_relative_path(_path_f)
                        _hash_f = self._get_hash(_path_f)
                        _id_f = _hash_f
                        _pId_f = parent_id
                        _name_f = p.name
                        _open_f = True
                        # result.append(dict(
                        #     id=_id_f,
                        #     pId=_pId_f,
                        #     name=_name_f,
                        #     open=_open_f,
                        #     path=_path_f,  # 自定义
                        #     relative_path=_relative_path_f,  # 自定义
                        #     hash=_hash_f,  # 自定义
                        #     icon='/static/icon/py.png',
                        #     node_type='file',  # 自定义
                        #     exist_flag=True,  # 自定义
                        # ))
                        result.append(node_for_test_case(
                            id=_id_f,
                            pId=_pId_f,
                            name=_name_f,
                            open=_open_f,
                            path=_path_f,
                            relative_path=_relative_path_f,
                            hash=_hash_f,
                            icon='/static/icon/py.png',
                            node_type='file',
                            exist_flag=True,
                        ))

                        # 解析文件中的类与方法
                        _ret = self._get_methods_from_case(_relative_path_f)
                        for classes_methods_dict in _ret:
                            for class_name in classes_methods_dict:
                                _path_c = p.__str__() + '.' + class_name
                                _relative_path_c = self._get_relative_path(_path_c)
                                _hash_c = self._get_hash(_path_c)
                                _id_c = _hash_c
                                _pId_c = _id_f
                                _name_c = class_name
                                _open_c = False
                                # result.append(dict(
                                #     id=_id_c,
                                #     pId=_pId_c,
                                #     name=_name_c,
                                #     open=_open_c,
                                #     path=_path_c,  # 自定义
                                #     relative_path=_relative_path_c,  # 自定义
                                #     hash=_hash_c,  # 自定义
                                #     icon='/static/icon/class.png',
                                #     node_type='class',  # 自定义
                                #     exist_flag=True,  # 自定义
                                # ))
                                result.append(node_for_test_case(
                                    id=_id_c,
                                    pId=_pId_c,
                                    name=_name_c,
                                    open=_open_c,
                                    path=_path_c,
                                    relative_path=_relative_path_c,
                                    hash=_hash_c,
                                    icon='/static/icon/class.png',
                                    node_type='class',
                                    exist_flag=True,
                                ))
                                # 解析类中的方法
                                for method_name in classes_methods_dict[class_name]:
                                    _path_m = p.__str__() + '.' + class_name + '.' + method_name
                                    _relative_path_m = self._get_relative_path(_path_m)
                                    _hash_m = self._get_hash(_path_m)
                                    _id_m = _hash_m
                                    _pId_m = _id_c
                                    _name_m = method_name
                                    _open_m = False
                                    # result.append(dict(
                                    #     id=_id_m,
                                    #     pId=_pId_m,
                                    #     name=_name_m,
                                    #     open=_open_m,
                                    #     path=_path_m,  # 自定义
                                    #     relative_path=_relative_path_m,  # 自定义
                                    #     hash=_hash_m,  # 自定义
                                    #     icon='/static/icon/function.png',
                                    #     node_type='func',  # 自定义
                                    #     exist_flag=True,  # 自定义
                                    # ))
                                    result.append(node_for_test_case(
                                        id=_id_m,
                                        pId=_pId_m,
                                        name=_name_m,
                                        open=_open_m,
                                        path=_path_m,
                                        relative_path=_relative_path_m,
                                        hash=_hash_m,
                                        icon='/static/icon/function.png',
                                        node_type='func',
                                        exist_flag=True,
                                    ))

                else:
                    # 解析目录
                    if p.name == '__pycache__':  # 忽略非案例相关目录
                        continue
                    _path_d = p.__str__()
                    _relative_path_d = self._get_relative_path(_path_d)
                    _hash_d = self._get_hash(_path_d)
                    _id_d = _hash_d
                    _pId_d = parent_id
                    _name_d = p.name
                    _open_d = True
                    _recursion(p=p, parent_id=_id_d)
                    # result.append(dict(
                    #     id=_id_d,
                    #     pId=_pId_d,
                    #     name=_name_d,
                    #     open=_open_d,
                    #     path=_path_d,  # 自定义
                    #     relative_path=_relative_path_d,  # 自定义
                    #     hash=_hash_d,  # 自定义
                    #     node_type='dir',  # 自定义
                    #     exist_flag=True,  # 自定义
                    # ))
                    result.append(node_for_test_case(
                        id=_id_d,
                        pId=_pId_d,
                        name=_name_d,
                        open=_open_d,
                        path=_path_d,
                        relative_path=_relative_path_d,
                        hash=_hash_d,
                        node_type='dir',
                        exist_flag=True,
                    ))

        testcase_path = Path(self.TestCaseDir)
        _recursion(p=testcase_path, parent_id=self._get_hash(self.TestCaseDir))

        return result

    def _get_hash(self, value: str) -> str:
        """相同字符串即使在不同进程中，也返回相同的hash值"""
        md5 = hashlib.md5()
        md5.update(value.encode(encoding='utf-8'))
        return md5.hexdigest()

    def _get_relative_path(self, path: str) -> str:
        return path.replace(Config.project_dir, '')

    def _get_methods_from_case(self, path) -> List[Dict]:
        """
        :param path: py文件相对路径，解析该py文件测试类的方法
        :return:
        """
        path = path.replace(os.sep, '.').replace('.TestCase', 'TestCase').replace('.py', '')
        import_str = 'import ' + path + ' as case_module'
        glb = {}
        loc = {}
        exec(import_str, glb, loc)
        case_module = loc.get('case_module', None)
        result = []
        if case_module is not None:
            for case_class_name, case_class in inspect.getmembers(case_module, inspect.isclass):
                # 获取到此py文件中定义的类
                if case_class.__module__ == path:
                    # 排查非TestCase子类的其他类
                    if not issubclass(case_class, unittest.TestCase): continue
                    # 获取到此类中定义的方法
                    case_method_list = []
                    for _k, _v in inspect.getmembers(case_class):
                        if (inspect.ismethod(_v) or inspect.isfunction(_v)) and (_v.__module__ == path):
                            if _k[:4].lower() == 'test':  # 找到test开头的方法
                                case_module_name = _k
                                case_method_list.append(case_module_name)
                    result.append({case_class_name: tuple(case_method_list)})
        return result
