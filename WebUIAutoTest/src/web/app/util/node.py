# coding=utf-8

"""
Author: zhangzheng
Description: 案例目录、执行计划、测试计划的json节点数据
Version: 0.0.1
LastUpdateDate: 
UpadteURL: 
LOG: 
"""
from typing import Dict


def node_for_test_case(*args, **kwargs) -> Dict:
    """
        生成案例目录节点对应jsonpy数据
        :param id: 节点数据中保存唯一标识的属性名称。[setting.data.simpleData.enable = true 时生效] 默认值："id"
        :param pId: 节点数据中保存其父节点唯一标识的属性名称。[setting.data.simpleData.enable = true 时生效] 默认值："pId"
        :param name: zTree 节点数据保存节点名称的属性名称。默认值："name"
        :param open: 记录 treeNode 节点的 展开 / 折叠 状态。
        :param path: [自定义属性] 表示该节点(可能是目录、文件、类、方法)的绝对路径信息
        :param relative_path: [自定义属性] 表示该节点(可能是目录、文件、类、方法)的相对路径信息
        :param hash: [自定义属性] 表示该节点(可能是目录、文件、类、方法)的hash值
        :param icon: 节点自定义图标的 URL 路径。
        :param node_type: [自定义属性] 表示该节点(可能是目录、文件、类、方法)的类型
        :param exist_flag: [自定义属性] 表示该节点(可能是目录、文件、类、方法)是否还已经存在
        :param isRoot: [自定义属性] 表示该节点是否是根节点
    """
    return dict(
        id=kwargs.get('id'),
        pId=kwargs.get('pId'),
        name=kwargs.get('name'),
        open=kwargs.get('open'),
        path=kwargs.get('path'),  # 自定义
        relative_path=kwargs.get('relative_path'),  # 自定义
        hash=kwargs.get('hash'),  # 自定义
        icon=kwargs.get('icon'),
        node_type=kwargs.get('node_type'),  # 自定义
        exist_flag=kwargs.get('exist_flag'),  # 自定义
        isRoot=kwargs.get('isRoot'),  # 自定义
    )


def node_for_plan(*args, **kwargs) -> Dict:
    """
        生成执行计划节点对应的jsonpy数据
        :param id: 节点数据中保存唯一标识的属性名称。[setting.data.simpleData.enable = true 时生效] 默认值："id"
        :param pId: 节点数据中保存其父节点唯一标识的属性名称。[setting.data.simpleData.enable = true 时生效] 默认值："pId"
        :param name: zTree 节点数据保存节点名称的属性名称。默认值："name"
        :param open: 记录 treeNode 节点的 展开 / 折叠 状态。
        :param isParent: 记录 treeNode 节点是否为父节点。
        :param drag: [自定义属性] 表示能否被拖拽
        :param isRoot: [自定义属性] 表示该节点是否是根节点
        :param copyMoveCount: [自定义属性] 表示节点被复制或拖拽的总次数
        :param buildHash: 节点的build的Hash值
    """
    return dict(
        id=kwargs.get('id'),
        pId=kwargs.get('pId'),
        name=kwargs.get('name'),
        open=kwargs.get('open'),
        isParent=kwargs.get('isParent'),
        drag=kwargs.get('drag'),
        isRoot=kwargs.get('isRoot'),
        copyMoveCount=kwargs.get('copyMoveCount'),
        buildHash=kwargs.get('buildHash'),
    )


def node_for_build(*args, **kwargs):
    """
        生成执行计划节点对应的jsonpy数据
        :param id: 节点数据中保存唯一标识的属性名称。[setting.data.simpleData.enable = true 时生效] 默认值："id"
        :param pId: 节点数据中保存其父节点唯一标识的属性名称。[setting.data.simpleData.enable = true 时生效] 默认值："pId"
        :param name: zTree 节点数据保存节点名称的属性名称。默认值："name"
        :param open: 记录 treeNode 节点的 展开 / 折叠 状态。
        :param isParent: 记录 treeNode 节点是否为父节点。
        :param drag: [自定义属性] 表示能否被拖拽
        :param isRoot: [自定义属性] 表示该节点是否是根节点
        :param checked: 节点的 checkBox / radio 的 勾选状态。
        :param buildHash: 节点的build的Hash值。
        :param node_type: [自定义属性] 表示该节点是'build'类型
        :param icon: 节点自定义图标的 URL 路径。
        :param responsiblePerson: [自定义属性] 表示执行计划绑定的责任人信息
    """
    return dict(
        id=kwargs.get('id'),
        pId=kwargs.get('pId'),
        name=kwargs.get('name'),
        open=kwargs.get('open'),
        isParent=kwargs.get('isParent'),
        drag=kwargs.get('drag'),
        isRoot=kwargs.get('isRoot'),
        checked=kwargs.get('checked'),
        buildHash=kwargs.get('buildHash'),  # 自定义
        node_type=kwargs.get('node_type'),  # 自定义
        icon=kwargs.get('icon'),
        responsiblePerson=kwargs.get('responsiblePerson'),  # 自定义
    )

