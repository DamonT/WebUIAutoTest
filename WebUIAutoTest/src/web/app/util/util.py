# coding=utf-8

import traceback
from flask import request, session, current_app, redirect, url_for
from sqlalchemy.exc import SQLAlchemyError
from sqlite3 import Error
from urllib.parse import urlparse, urljoin
import pprint
from random import randint


def save_request_session_info():
    request_session_dict = request.__dict__
    request_session_dict.update(dict(session=session))
    current_app.logger.error(pprint.pformat(request_session_dict))


def is_safe_url(target):
    """判断url是否属于程序内部"""
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and \
           ref_url.netloc == test_url.netloc


def create_hash(length):
    """生成指定长度随机hash数值"""
    l = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', \
         'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    return ''.join([l[randint(0, len(l) - 1)] for _ in range(length)])


def exception_handle(app, exc):
    app.logger.error(traceback.format_exc())
    tbexc = traceback.TracebackException.from_exception(exc)
    if issubclass(tbexc.exc_type, (Error, SQLAlchemyError)):
        from src.web.app.extensions import db
        db.session.rollback()
    save_request_session_info()


def redirect_back(default='main.index', **kwargs):
    for target in request.args.get('next'), request.referrer:
        if not target:
            continue
        if is_safe_url(target):
            return redirect(target)
    return redirect(url_for(default, **kwargs))
