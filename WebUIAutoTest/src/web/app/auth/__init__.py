# coding=utf-8

from flask import Blueprint

bp = Blueprint("auth", __name__)

from src.web.app.auth import routes