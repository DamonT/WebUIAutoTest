# coding=utf-8

from flask import render_template, request, jsonify, current_app
from flask_login import login_required
import json
from datetime import datetime

from src.web.app.main import bp
from src.web.app.util.handlecase import HandleCase
from src.web.app.models import ExecPlan
from src.web.app.util.util import exception_handle

from src.config.config import Config as ProjectConfig


@bp.route('/editor', methods=['GET', 'POST'])
@login_required
def editor():
    if request.method == 'POST':
        form_json = request.form.get('json')
        form_kind = request.form.get('kind')
        form_build_name = request.form.get('buildName')
        form_build_hash = request.form.get('buildHash')
        form_serial_no = request.form.get('serialNo')
        form_version = request.form.get('version')
        form_checked = request.form.get('checked')
        form_responsible_person = request.form.get('responsiblePerson')
        if form_kind is None:
            return jsonify({
                'error_no': -1,
                'error_msg': "缺少参数kind",
            })
        if form_build_name is None:
            return jsonify({
                'error_no': -1,
                'error_msg': "缺少参数buildName",
            })
        if form_build_hash is None:
            return jsonify({
                'error_no': -1,
                'error_msg': "缺少参数buildHash",
            })
        if form_kind == 'select':
            response_json_case = None
            response_json_plan = []
            response_json_build = []
            case_hash_set = set()
            try:
                response_json_case = HandleCase().parse_dir()
            except Exception as e:
                exception_handle(current_app, e)
                return jsonify({
                    'error_no': -1,
                    'error_msg': '错误信息 [' + str(e.args) + ']',
                })
            try:
                if ExecPlan.empty():
                    response_json_plan = ExecPlan.get_plan_data_by_build_hash(name='测试计划')
                    version = str(datetime.now().timestamp() * 1000).split('.')[0]  # 精确到毫秒的时间戳
                    ExecPlan.add(name='测试计划',
                                 _hash=response_json_plan[0].get('buildHash', ''),
                                 data=response_json_plan,
                                 serial_no=1,
                                 version=version,
                                 checked=False,
                                 responsible_person=' ')
                else:
                    if form_build_hash == '':  # 返回所有plan数据
                        response_json_plan = ExecPlan.default_data()
                    else:  # 返回指定的plan数据
                        response_json_plan = ExecPlan.get_plan_data_by_build_hash(_hash=form_build_hash,
                                                                                  name=form_build_name)
                # 判断py文件、类、方法是否还存在，当被删除或修改后原先节点的exist_flag=false，只有存在的为true
                case_hash_set = {_case_dict['hash'] for _case_dict in response_json_case}
                for _plan_datas in response_json_plan:
                    if isinstance(_plan_datas, dict):
                        check_node_existed(plan_dict=_plan_datas, case_hash_set=case_hash_set)
                    else:
                        for _plan_data in _plan_datas:
                            if isinstance(_plan_data, dict):
                                check_node_existed(plan_dict=_plan_data, case_hash_set=case_hash_set)
                response_json_build = ExecPlan.get_build_data()
            except Exception as e:
                exception_handle(current_app, e)
                return jsonify({
                    'error_no': -1,
                    'error_msg': '错误信息 [' + str(e.args) + ']',
                })
            else:
                return jsonify({
                    'error_no': 0,
                    'error_msg': '',
                    'jsonCase': response_json_case,
                    'jsonPlan': response_json_plan,
                    'jsonBuild': response_json_build,
                })
        elif form_kind == 'save':
            if form_json is None:
                return jsonify({
                    'error_no': -1,
                    'error_msg': "缺少参数json",
                })
            if form_serial_no is None:
                return jsonify({
                    'error_no': -1,
                    'error_msg': "缺少参数serialNo",
                })
            if form_version is None:
                return jsonify({
                    'error_no': -1,
                    'error_msg': "缺少参数version",
                })
            if form_checked is None:
                return jsonify({
                    'error_no': -1,
                    'error_msg': "缺少参数checked",
                })
            if form_responsible_person is None:
                return jsonify({
                    'error_no': -1,
                    'error_msg': "缺少参数responsiblePerson",
                })
            try:
                json_py = json.loads(form_json)  # 将json字符串转化为py对象
                ExecPlan.add(
                    name=form_build_name,
                    _hash=form_build_hash,
                    data=json_py,
                    serial_no=form_serial_no,
                    version=form_version,
                    checked=True if form_checked == 'true' else False,
                    responsible_person=form_responsible_person,
                )
            except Exception as e:
                exception_handle(current_app, e)
                return jsonify({
                    'error_no': -1,
                    'error_msg': '错误信息 [' + str(e.args) + ']',
                })
            else:
                return jsonify({
                    'error_no': 0,
                    'error_msg': '',
                })
        else:
            return jsonify({
                'error_no': -1,
                'error_msg': "参数kind传入非法值",
            })
    return render_template('main/editor.html')


@bp.route('/editor/get_all_responsible_person', methods=['POST'])
@login_required
def get_all_responsible_person():
    try:
        responsible_person_email = ProjectConfig.responsible_person_email
    except Exception as e:
        exception_handle(current_app, e)
        return jsonify({
            'error_no': -1,
            'error_msg': '错误信息 [' + str(e.args) + ']',
        })
    else:
        return jsonify({
            'error_no': 0,
            'error_msg': '',
            'responsible_person': ','.join(responsible_person_email.keys()),
        })


def check_node_existed(plan_dict, case_hash_set):
    if plan_dict.get('isRoot'):
        pass
    else:
        if plan_dict.get('hash') not in case_hash_set:
            plan_dict.update(dict(exist_flag=False))
        if plan_dict.get('hash') in case_hash_set:
            plan_dict.update(dict(exist_flag=True))
