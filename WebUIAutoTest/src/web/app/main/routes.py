# coding=utf-8

from flask import render_template, jsonify, current_app, request, abort
from flask_login import current_user
from datetime import datetime
import os

from src.web.app.main import bp
from src.web.app.extensions import db, cache
from src.web.app.models import BuildInfo
from src.web.app.util.util import exception_handle, save_request_session_info

from src.config.config import Config as ProjectConfig
from src.public.public import Public

@bp.before_app_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.now()
        db.session.commit()


@bp.route("/")
@bp.route("/index")
# @cache.cached(timeout=60 * 60)
def index():
    return render_template("main/index.html", title="主页")


@bp.route('/index/get_build_count', methods=['POST'])
def get_build_count():
    form_flag = request.form.get('flag', None)
    if form_flag is None:
        return jsonify({
            'error_no': -1,
            'error_msg': '缺少参数flag',
        })
    elif form_flag == 'current':
        user_id = current_user.id
    elif form_flag == 'all':
        user_id = None
    else:
        return jsonify({
            'error_no': -1,
            'error_msg': '参数flag传入非法值',
        })
    try:
        data = BuildInfo.get_last_30_days_build_counts(user_id=user_id)
    except Exception as e:
        exception_handle(current_app, e)
        return jsonify({
            'error_no': -1,
            'error_msg': '错误信息 [' + str(e.args) + ']',
        })
    else:
        return jsonify({
            'error_no': 0,
            'error_msg': '',
            'data': data,
        })


@bp.route('/cover')
def cover():
    return render_template('main/cover.html')


@bp.route('/upload', methods=['POST'])
def upload():
    if request.method == 'POST':
        file = request.files.get('file', None)
        form_type = request.form.get('type', None)

        if file is None:
            return jsonify({
                'error_no': -1,
                'error_msg': '未找到上传文件',
            })

        if form_type is None:
            return jsonify({
                'error_no': -1,
                'error_msg': '缺少type参数',
            })

        if form_type == 'case':
            try:
                cases_dir_name = 'TestCases'
                cases_zip_name = 'TestCases.zip'
                file_path = os.path.join(ProjectConfig.project_dir, cases_zip_name)
                file.save(file_path)
                # 每次解压前先备份原案例
                if os.path.exists(file_path):
                    os.rename(
                        os.path.join(ProjectConfig.project_dir, cases_dir_name),
                        os.path.join(ProjectConfig.project_dir, cases_dir_name + '_bk_' + datetime.now().strftime("%Y%m%d_%H%M%S"))
                    )
                Public.unpack_archive(file_path, ProjectConfig.project_dir)
            except Exception as e:
                exception_handle(current_app, e)
                return jsonify({
                    'error_no': -1,
                    'error_msg': '错误信息 [' + str(e.args) + ']',
                })
            else:
                return jsonify({
                    'error_no': 0,
                    'error_msg': '',
                })
        else:
            return jsonify({
                'error_no': -1,
                'error_msg': '参数type传入非法值',
            })
    else:
        abort(405)


# 注册其他路由
import src.web.app.main.config_routes
import src.web.app.main.editor_routes
import src.web.app.main.execution_routes
import src.web.app.main.reports_routes
import src.web.app.main.logs_routes
import src.web.app.main.coder_routes
