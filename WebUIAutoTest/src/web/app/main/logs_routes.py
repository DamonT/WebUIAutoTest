# coding=utf-8

from flask import render_template, jsonify, request, current_app
from flask_login import login_required
from datetime import datetime
import os

from src.web.app.main import bp
from src.web.app.util.util import exception_handle

from src.config.config import Config as ProjectConfig
from src.log.log import get_all_logs


@bp.route('/logs', methods=['GET', 'POST'])
@login_required
def logs():
    if request.method == 'POST':
        form_log = request.form.get('log', None)
        form_row_num = request.form.get('row_num', None)
        response_content = ''
        response_row_num_after_read = 0
        response_done = False
        if form_log is None:
            return jsonify({
                'error_no': -1,
                'error_msg': "缺少参数log",
            })
        if form_row_num is None:
            return jsonify({
                'error_no': -1,
                'error_msg': "缺少参数row_num",
            })
        try:
            row_num = int(form_row_num)
            with open(os.path.join(ProjectConfig.project_dir, 'Log', form_log), mode='r', encoding='utf-8') as f:
                # f.seek(position)
                # response_content = f.read(1024 * 1024)  # 1MB
                # response_position_after_read = f.tell()
                # if not response_content:
                #     response_done = True
                all_lines = f.readlines()
                response_content = all_lines[row_num: row_num + 1000]  # 每次读取1000行
                response_row_num_after_read = row_num + len(response_content)
                response_content = ''.join(response_content)
                if response_row_num_after_read >= len(all_lines):
                    response_done = True
        except OSError as e:
            exception_handle(current_app, e)
            return jsonify({
                'error_no': -1,
                'error_msg': '错误信息 [' + str(e.args) + ']',
            })
        else:
            return jsonify({
                'error_no': 0,
                'error_msg': '',
                'row_num': response_row_num_after_read,
                'content': response_content,
                'done': response_done,
            })

    all_logs = get_all_logs()
    logs = []
    page = 1
    num_each_page = 5  # 每页展示五个
    count = 0
    for _log in all_logs:
        mtime = os.path.getmtime(os.path.join(ProjectConfig.project_dir, 'Log', _log))
        # strftime = time.strftime("%Y%m%d %H:%M:%S", time.localtime(mtime))
        last_modified_time = datetime.fromtimestamp(mtime)
        last_modified_utctime = datetime.utcfromtimestamp(mtime)
        size = os.path.getsize(os.path.join(ProjectConfig.project_dir, 'Log', _log))
        log = {
            'name': _log,
            # 'desc': '大小{}kb，最后修改{}'.format(round(size / 1024, 2), strftime),
            'page': page,
            'size': round(size / 1024, 2),
            'last_modified_time': last_modified_time,
            'last_modified_utctime': last_modified_utctime,
        }
        logs.append(log)
        count += 1
        if count == num_each_page:
            page += 1
            count = 0
    pages = page  # 总页数
    if pages >= 5 and len(all_logs) % 5 == 0:  # 去除多统计的1页
        pages -= 1
    return render_template('main/logs.html',  logs=logs, pages=pages)
