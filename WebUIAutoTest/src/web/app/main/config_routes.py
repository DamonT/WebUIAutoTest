# coding=utf-8

from flask import render_template, request, jsonify, current_app, redirect, url_for, flash, make_response
from flask_login import login_required
from flask_mail import Mail
import logging
import json

from src.web.app.main import bp
from src.web.app.main.forms import RunningConfigForm, WebConfigForm
from src.web.app.util.util import exception_handle

from src.config.logconfig import Config as LogConfig
from src.config.config import Config as ProjectConfig


@bp.route('/config', methods=['GET', 'POST'])
@login_required
def config():
    form = RunningConfigForm()
    # 读取config.ini文件设置默认值
    form.browser_type.render_kw['default_value'] = ProjectConfig.browser_type.lower()
    form.browser_path_360.render_kw['value'] = ProjectConfig.browser_path_360 or ''
    form.timeout.render_kw['value'] = ProjectConfig.timeout or ''
    form.headless.render_kw['default_value'] = str(ProjectConfig.headless).lower() or ''
    form.skip_test_method.render_kw['default_value'] = str(ProjectConfig.skip_test_method).lower() or ''
    form.extension_ui_enable.render_kw['default_value'] = str(ProjectConfig.extension_ui_enable).lower() or ''
    form.extension_ui_module_name.render_kw['value'] = ProjectConfig.extension_ui_module_name or ''
    logging_level_default_value = ''
    if LogConfig.logging_level == logging.DEBUG: logging_level_default_value = 'debug'
    if LogConfig.logging_level == logging.INFO: logging_level_default_value = 'info'
    if LogConfig.logging_level == logging.WARNING: logging_level_default_value = 'warning'
    if LogConfig.logging_level == logging.ERROR: logging_level_default_value = 'error'
    form.logging_level.render_kw['default_value'] = logging_level_default_value.lower()
    # 解决当设置控件disabled时提交报"Not a valid choice"的问题
    form.logging_level.data = logging_level_default_value.lower()
    form.manage_case_type.render_kw['default_value'] = ProjectConfig.manage_case_type.lower()
    form.manage_case_type.data = ProjectConfig.manage_case_type.lower()
    form.send_email.render_kw['default_value'] = str(ProjectConfig.send_email).lower()
    form.gen_report.render_kw['default_value'] = str(ProjectConfig.gen_report).lower()
    form.smtp_server_address.render_kw['value'] = ProjectConfig.smtp_server_address
    form.smtp_port.render_kw['value'] = ProjectConfig.smtp_port or ''
    form.smtp_tls.render_kw['default_value'] = str(ProjectConfig.smtp_tls).lower()
    form.smtp_ssl.render_kw['default_value'] = str(ProjectConfig.smtp_ssl).lower()
    form.smtp_login_name.render_kw['value'] = ProjectConfig.smtp_login_name or ''
    form.smtp_login_passwd.render_kw['default_value'] = ProjectConfig.smtp_login_passwd or ''
    form.sender_address.render_kw['value'] = ProjectConfig.sender_address or ''
    form.receiver_address.render_kw['value'] = ','.join(ProjectConfig.receiver_address) or ''
    form.email_title.render_kw['value'] = ProjectConfig.email_title or ''
    form.email_text.render_kw['value'] = ProjectConfig.email_text or ''
    form.title.render_kw['value'] = ProjectConfig.title or ''
    form.description.render_kw['value'] = ProjectConfig.description or ''
    form.notifier_enable.render_kw['default_value'] = str(ProjectConfig.notifier_enable).lower()
    form.access_token.render_kw['value'] = ProjectConfig.access_token or ''
    form.is_at_all.render_kw['default_value'] = str(ProjectConfig.is_at_all).lower()
    form.at_mobiles.render_kw['value'] = ','.join(ProjectConfig.at_mobiles) or ''
    form.dbtype.render_kw['default_value'] = ProjectConfig.dbtype.lower() or ''
    form.dsn_oracle.render_kw['value'] = ProjectConfig.dsn or ''
    form.user_oracle.render_kw['value'] = ProjectConfig.user_oracle or ''
    form.passwd_oracle.render_kw['default_value'] = ProjectConfig.passwd_oracle or ''
    form.host_mysql.render_kw['value'] = ProjectConfig.host or ''
    form.user_mysql.render_kw['value'] = ProjectConfig.user_mysql or ''
    form.passwd_mysql.render_kw['default_value'] = ProjectConfig.passwd_mysql or ''
    form.port_mysql.render_kw['value'] = ProjectConfig.port or ''
    form.charset_mysql.render_kw['value'] = ProjectConfig.charset or ''

    if form.validate_on_submit():
        try:
            ProjectConfig.browser_type = form.browser_type.data
            ProjectConfig.browser_path_360 = form.browser_path_360.data
            ProjectConfig.timeout = form.timeout.data
            ProjectConfig.headless = form.headless.data
            ProjectConfig.skip_test_method = form.skip_test_method.data
            ProjectConfig.extension_ui_enable = form.extension_ui_enable.data
            ProjectConfig.extension_ui_module_name = form.extension_ui_module_name.data
            # ProjectConfig.logging_level = form.logging_level.data  不支持更新日志级别，只能手工修改config.ini文件
            # ProjectConfig.manage_case_type = form.manage_case_type.data  # 不支持更新，只能手工修改config.ini文件
            ProjectConfig.send_email = form.send_email.data
            ProjectConfig.gen_report = form.gen_report.data
            ProjectConfig.smtp_server_address = form.smtp_server_address.data
            ProjectConfig.smtp_port = form.smtp_port.data
            ProjectConfig.smtp_ssl = form.smtp_ssl.data
            ProjectConfig.smtp_tls = form.smtp_tls.data
            ProjectConfig.smtp_login_name = form.smtp_login_name.data
            ProjectConfig.smtp_login_passwd = form.smtp_login_passwd.data
            ProjectConfig.sender_address = form.sender_address.data
            ProjectConfig.receiver_address = form.receiver_address.data
            current_app.config.update(
                MAIL_SERVER=form.smtp_server_address.data,
                MAIL_PORT=int(form.smtp_port.data),
                MAIL_USE_TLS=True if form.smtp_tls.data.lower() == 'true' else False,
                MAIL_USE_SSL=True if form.smtp_ssl.data.lower() == 'true' else False,
                MAIL_USERNAME=form.smtp_login_name.data,
                MAIL_PASSWORD=form.smtp_login_passwd.data,
                MAIL_DEFAULT_SENDER=('Admin', form.smtp_login_name.data),
            )
            mail = Mail()
            mail.init_app(app=current_app)
            ProjectConfig.email_title = form.email_title.data
            ProjectConfig.email_text = form.email_text.data
            ProjectConfig.title = form.title.data
            ProjectConfig.description = form.description.data
            ProjectConfig.notifier_enable = form.notifier_enable.data
            ProjectConfig.access_token = form.access_token.data
            ProjectConfig.is_at_all = form.is_at_all.data
            ProjectConfig.at_mobiles = form.at_mobiles.data
            ProjectConfig.dbtype = form.dbtype.data
            ProjectConfig.dsn = form.dsn_oracle.data
            ProjectConfig.user_oracle = form.user_oracle.data
            ProjectConfig.passwd_oracle = form.passwd_oracle.data
            ProjectConfig.host = form.host_mysql.data
            ProjectConfig.user_mysql = form.user_mysql.data
            ProjectConfig.passwd_mysql = form.passwd_mysql.data
            ProjectConfig.port = form.port_mysql.data
            ProjectConfig.charset = form.charset_mysql.data
        except Exception as e:
            exception_handle(current_app, e)
            flash('配置更新失败，请查看日志', category='error')
        else:
            flash('配置更新成功', category='success')
        return redirect(url_for('main.config'))
    return render_template('main/config.html', form=form)


@bp.route('/config/get_responsible_info', methods=['POST'])
@login_required
def get_responsible_info():
    responsible_person_email = ProjectConfig.responsible_person_email
    return jsonify(
        [{'ID': _i + 1, 'Name': _k, 'EMailAddress': _v} for _i, (_k, _v) in enumerate(responsible_person_email.items())]
    )


@bp.route('/config/save_responsible_info', methods=['POST'])
@login_required
def save_responsible_info():
    form_rows = request.form.get('rows', None)
    if form_rows is None:
        return jsonify({
            'error_no': -1,
            'error_msg': '缺少参数rows',
        })
    try:
        rows = json.loads(form_rows)
        ProjectConfig.responsible_person_email = rows
    except Exception as e:
        exception_handle(current_app, e)
        return jsonify({
            'error_no': -1,
            'error_msg': '错误信息 [' + str(e.args) + ']',
        })
    else:
        return jsonify({
            'error_no': 0,
            'error_msg': '',
        })


@bp.route('/web_config', methods=['POST', 'GET'])
def web_config():
    form = WebConfigForm()
    form.theme_type.render_kw['default_value'] = request.cookies.get('theme', 'default')
    if form.validate_on_submit():
        theme_type = form.theme_type.data
        response = make_response(redirect(url_for('main.web_config')))
        response.set_cookie('theme', theme_type, max_age=30 * 24 * 60 * 60)
        flash('配置更新成功', category='success')
        return response
    return render_template('main/web_config.html', form=form)
