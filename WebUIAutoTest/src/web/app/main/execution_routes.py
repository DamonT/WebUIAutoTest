# coding=utf-8

from flask import render_template, request, jsonify, current_app
from flask_login import login_required
import subprocess
import time
import os

from src.web.app.main import bp
from src.web.app.util.util import exception_handle
from src.web.app.models import BuildInfo

from src.log.log import get_latest_log
from src.config.config import Config as ProjectConfig
from src.public.status import running, progress


@bp.route('/execution', methods=['GET', 'POST'])
@login_required
def execution():
    if request.method == 'POST':
        flag = request.form.get('flag', None)
        if flag is None:
            return jsonify({
                'error_no': -1,
                'error_msg': "缺少参数flag",
            })
        if flag == 'start':
            # 启动运行
            try:
                BuildInfo.add()
                cmd = os.path.join(ProjectConfig.project_dir, 'runtest.bat')
                subprocess.Popen(cmd,
                                 shell=True,
                                 stdin=subprocess.PIPE)
                _count = 0
            except Exception as e:
                exception_handle(current_app, e)
                return jsonify({
                    'error_no': -1,
                    'error_msg': '错误信息 [' + str(e.args) + ']',
                })
            else:
                while _count < 10:
                    time.sleep(1)
                    status = running()
                    if status:  # 已启动
                        return jsonify({
                            'error_no': 0,
                            'error_msg': '',
                        })
                else:
                    return jsonify({
                        'error_no': -1,
                        'error_msg': '启动未成功, 请重试',
                    })
        elif flag == 'stop':
            # 终止运行
            try:
                cmd = os.path.join(ProjectConfig.project_dir, 'stoptest.bat')
                subprocess.Popen(cmd,
                                 shell=True,
                                 stdin=subprocess.PIPE)
                _count = 0
            except Exception as e:
                exception_handle(current_app, e)
                return jsonify({
                    'error_no': -1,
                    'error_msg': '错误信息 [' + str(e.args) + ']',
                })
            while _count < 10:
                time.sleep(1)
                status = running()
                if not status:  # 状态已停止
                    return jsonify({
                        'error_no': 0,
                        'error_msg': '',
                    })
            else:
                return jsonify({
                    'error_no': -1,
                    'error_msg': '停止失败, 请重试',
                })
        else:
            return jsonify({
                'error_no': -1,
                'error_msg': "传递flag参数'{}'是非法值".format(flag),
            })
    try:
        # 读取最新构建日志
        log_file_name = get_latest_log()
        with open(os.path.join(ProjectConfig.project_dir, 'Log', log_file_name), 'r', encoding='utf-8') as f:
            execution_log = f.read()
    except Exception as e:
        exception_handle(current_app, e)
        execution_log = ''
    return render_template('main/execution.html', execution_log=execution_log)


@bp.route('/execution/get_log', methods=['POST'])
@login_required
def get_log():
    if request.method == 'POST':
        # 关于日志文件，每次post应答文件名，检测如果有新的日志文件则加载新的日志文件
        form_row_num = request.form.get('row_num', None)
        form_log = request.form.get('log', None)
        response_row_num_after_read = 0
        response_content = ''
        response_log = ''
        if form_row_num is None:
            return jsonify({
                'error_no': -1,
                'error_msg': "缺少参数row_num",
            })
        if form_log is None:
            return jsonify({
                'error_no': -1,
                'error_msg': "缺少参数log",
            })
        try:
            latest_log = get_latest_log()
            if form_log == '':
                log_file = latest_log
            else:
                log_file = form_log
            response_log = log_file
            row_num = int(form_row_num)
            with open(os.path.join(ProjectConfig.project_dir, 'Log', log_file), mode='r', encoding='utf-8') as f:
                # f.seek(position)
                # response_content = f.read(1024 * 10)  # 10KB
                # response_position_after_read = f.tell()
                # if position > 0 and latest_log != log_file:  # 后台日志文件发生变化
                #     response_content += response_content + f.read(-1)
                #     response_position_after_read = 0
                #     response_log = latest_log
                all_lines = f.readlines()
                response_content = all_lines[row_num: row_num + 100]  # 每次读取100行
                response_row_num_after_read = row_num + len(response_content)
                response_content = ''.join(response_content)
                if row_num > 0 and latest_log != log_file:  # 后台日志文件发生变化
                    response_content = ''.join(all_lines)
                    response_row_num_after_read = 0
                    response_log = latest_log
        except Exception as e:
            exception_handle(current_app, e)
            return jsonify({
                'error_no': -1,
                'error_msg': '错误信息 [' + str(e.args) + ']',
            })
        else:
            return jsonify({
                'error_no': 0,
                'error_msg': '',
                'row_num': response_row_num_after_read,
                'content': response_content,
                'log': response_log,
            })


@bp.route('/execution/exec_status', methods=['POST'])
@login_required
def exec_status():
    """获取构建的执行状态和进度"""
    try:
        response_status = running()
        response_progress = progress()
    except Exception as e:
        exception_handle(current_app, e)
        return jsonify({
            'error_no': -1,
            'error_msg': '错误信息 [' + str(e.args) + ']',
            'status': '',
            'progress': 0,
        })
    else:
        return jsonify({
            'error_no': 0,
            'error_msg': '',
            'status': response_status,
            'progress': response_progress,
        })
