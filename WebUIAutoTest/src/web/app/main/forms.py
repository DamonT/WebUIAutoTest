# coding=utf-8

from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, SelectField
from wtforms.validators import DataRequired, Regexp, ValidationError


def required_when_send_mail(form, field):
    if (form.send_email.data == 'true') and (not field.data):
        raise ValidationError('当发送邮件时，此项不能为空')


def required_when_dingding_enable(form, field):
    if (form.notifier_enable.data == 'true') and (not field.data):
        raise ValidationError('当启动钉钉通知时，此项不能为空')


def required_when_dbtype_selected(dbtype):
    def _required_when_dbtype_selected(form, field):
        if dbtype == 'oracle' and (form.dbtype.data == 'oracle') and (not field.data):
            raise ValidationError('当选择Oracle数据库时，此项不能为空')
        elif dbtype == 'mysql'and (form.dbtype.data == 'mysql') and (not field.data):
            raise ValidationError('当选择MySql数据库时，此项不能为空')
    return _required_when_dbtype_selected


def required_when_extension_ui_enable(form, field):
    if (form.extension_ui_enable.data == 'true') and (not field.data):
        raise ValidationError('当启动启用ui拓展时，此项不能为空')


class RunningConfigForm(FlaskForm):
    # [PROJECT] [TEST] [LOG]
    browser_type = SelectField(label='运行浏览器类型',
                               choices=[('chrome', '谷歌浏览器'), ('firefox', '火狐浏览器'), ('360', '360浏览器')],
                               render_kw={})
    browser_path_360 = StringField(label='360浏览器安装路径',
                                   validators=[DataRequired(message="路径不能为空")],
                                   render_kw={})
    timeout = StringField(label='隐形等待时间',
                          validators=[Regexp(regex=r'^\d+$', message='只能输入正整数'), DataRequired(message='隐形等待时间不能为空')],
                          render_kw={})
    headless = SelectField(label='是否采用无头模式',
                           choices=[('false', '否'), ('true', '是')],
                           render_kw={})
    skip_test_method = SelectField(label='发生错误或失败时跳过之后的案例',
                                   choices=[('false', '否'), ('true', '是')],
                                   render_kw={})
    extension_ui_enable = SelectField(label='是否启用ui拓展',
                                      choices=[('false', '否'), ('true', '是')],
                                      render_kw={})
    extension_ui_module_name = StringField(label='指定拓展的ui类路径',
                                           validators=[required_when_extension_ui_enable],
                                           render_kw={})
    logging_level = SelectField(label='日志输出级别(只读)',
                                choices=[('debug', 'Debug'),
                                         ('info', 'Info'),
                                         ('warning', 'Warning'),
                                         ('error', 'Error')],
                                render_kw={})
    manage_case_type = SelectField(label='组织管理用例方式(只读)',
                                   choices=[('json', 'Json'),
                                            ('excel', 'Excel')],
                                   render_kw={})

    # [EMAIL]
    send_email = SelectField(label='是否发送邮件',
                             choices=[('true', '是'), ('false', '否')],
                             render_kw={})
    gen_report = SelectField(label='是否生成报告',
                             choices=[('true', '是'), ('false', '否')],
                             render_kw={})
    smtp_server_address = StringField(label='SMTP服务器地址',
                                      validators=[required_when_send_mail],
                                      render_kw={})
    smtp_port = StringField(label='SMTP服务器地址端口',
                            validators=[required_when_send_mail, Regexp(regex=r'^\d+$', message='只能输入正整数')],
                            render_kw={})
    smtp_tls = SelectField(label='使用STARTTLS加密',
                           choices=[('true', '是'), ('false', '否')],
                           render_kw={})
    smtp_ssl = SelectField(label='SSL/TLS',
                           choices=[('true', '是'), ('false', '否')],
                           render_kw={})
    smtp_login_name = StringField(label='SMTP服务器登录名',
                                  validators=[required_when_send_mail],
                                  render_kw={})
    smtp_login_passwd = PasswordField(label='SMTP服务器登录密码',
                                      validators=[required_when_send_mail],
                                      render_kw={})
    sender_address = StringField(label='发件地址',
                                 validators=[required_when_send_mail],
                                 render_kw={})
    receiver_address = StringField(label='收件地址',
                                   validators=[required_when_send_mail],
                                   render_kw={})
    email_title = StringField(label='邮件主题',
                              validators=[required_when_send_mail],
                              render_kw={})
    email_text = StringField(label='正文内容',
                             validators=[required_when_send_mail],
                             render_kw={})

    # [REPORT] [NOTIFIER]
    title = StringField(label='报告标题',
                        validators=[DataRequired(message='报告标题不能为空')],
                        render_kw={})
    description = StringField(label='报告描述',
                              validators=[DataRequired(message='报告描述不能为空')],
                              render_kw={})
    notifier_enable = SelectField(label='是否启用钉钉通知',
                                  choices=[('true', '是'), ('false', '否')],
                                  render_kw={})
    access_token = StringField(label='钉钉机器人access_token',
                               validators=[required_when_dingding_enable],
                               render_kw={})
    is_at_all = SelectField(label='是否@所有人',
                            choices=[('true', '是'), ('false', '否')],
                            render_kw={})
    at_mobiles = StringField(label='@一个或多个成员(使用手机号，多个手机号使用逗号分隔)',
                             validators=[],
                             render_kw={})

    # [DATABASE] [ORACLE] [MYSQL]
    dbtype = SelectField(label='数据库类型',
                         choices=[('oracle', 'Oracle'), ('mysql', 'MySql')],
                         render_kw={})
    dsn_oracle = StringField(label='DSN(DataBase Server Name)',
                             validators=[required_when_dbtype_selected(dbtype='oracle')],
                             render_kw={})
    user_oracle = StringField(label='用户名',
                              validators=[required_when_dbtype_selected(dbtype='oracle')],
                              render_kw={})
    passwd_oracle = PasswordField(label='密码',
                                  validators=[required_when_dbtype_selected(dbtype='oracle')],
                                  render_kw={})
    host_mysql = StringField(label='主机名或IP地址',
                             validators=[required_when_dbtype_selected(dbtype='mysql')],
                             render_kw={})
    user_mysql = StringField(label='用户名',
                             validators=[required_when_dbtype_selected(dbtype='mysql')],
                             render_kw={})
    passwd_mysql = PasswordField(label='密码',
                                 validators=[required_when_dbtype_selected(dbtype='mysql')],
                                 render_kw={})
    port_mysql = StringField(label='端口',
                             validators=[required_when_dbtype_selected(dbtype='mysql')],
                             render_kw={})
    charset_mysql = StringField(label='编码',
                                validators=[required_when_dbtype_selected(dbtype='mysql')],
                                render_kw={})

    submit = SubmitField('提交')


class WebConfigForm(FlaskForm):
    theme_type = SelectField(label='主题选择',
                             choices=[
                                 ('default', 'default'),
                                 ('paper', 'paper'),
                                 ('flatly', 'flatly'),
                                 ('journal', 'journal'),
                                 ('lumen', 'lumen'),
                                 ('yeti', 'yeti'),
                             ],
                             render_kw={})
    submit = SubmitField('保存')