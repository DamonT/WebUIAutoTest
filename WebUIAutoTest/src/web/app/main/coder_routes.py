# coding=utf-8

import os
from pathlib import Path
from flask import render_template, jsonify, request, current_app, Markup, flash, abort
from flask_login import login_required

from src.web.app.main import bp
from src.web.app.util.util import exception_handle


@bp.route('/coder', defaults={'path': 'not exist'})
@bp.route('/coder/<path:path>', methods=['GET'])
@login_required
def coder(path):
    path = request.args.get('path', path) if path == 'not exist' else path
    file_name = '未知的py文件名'
    if os.path.exists(path) and path != 'not exist':
        try:
            with open(path, encoding='utf-8', mode='r') as f:
                code_content = f.read()
            path_file = Path(path)
            file_name = path_file.name
        except Exception as e:
            code_content = '# py文件读取失败'
            flash('py文件读取失败，请重试', 'error')
    else:
        code_content = '# 未找到py文件'
        flash('未找到py文件，文件路径: %s，请尝试从编辑页面案例目录中双击打开。' % path, 'warning')
    return render_template('main/coder.html', code_content=Markup(code_content), file_name=file_name, file_path=path.replace('\\', '\\\\'))  # 避免jinja转义


@bp.route('/coder/save_to_file', methods=['POST'])
@login_required
def coder_save_to_file():
    if request.method == 'POST':
        form_file_path = request.form.get('path', None)
        form_content = request.form.get('content', None)
        if form_file_path is None:
            return jsonify({
                'error_no': -1,
                'error_msg': "缺少参数path",
            })
        if form_content is None:
            return jsonify({
                'error_no': -1,
                'error_msg': "缺少参数content",
            })
        try:
            with open(form_file_path, 'w', encoding='utf-8') as f:
                f.write(form_content)
            return jsonify({
                'error_no': 0,
                'error_msg': '',
            })
        except Exception as e:
            exception_handle(current_app, e)
            return jsonify({
                'error_no': -1,
                'error_msg': '错误信息 [' + str(e.args) + ']',
            })
    else:
        abort(405)
