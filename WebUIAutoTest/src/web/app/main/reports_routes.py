# coding=utf-8

from flask import render_template
from flask_login import login_required
from datetime import datetime
import os

from src.web.app.main import bp

from src.config.config import Config as ProjectConfig
from src.report.report import get_all_reports


@bp.route('/reports', methods=['GET'])
@login_required
def reports():
    default_src = ''
    all_reports = get_all_reports()
    reports = []
    page = 1
    num_each_page = 5  # 每页展示五个
    count = 0
    for i, _report in enumerate(all_reports):
        static_path = os.path.join('static', 'report', _report, _report + '.html')
        _static_path = 'static/report/' + _report + '/' + _report + '.html'
        report_path = os.path.join(ProjectConfig.project_dir, 'src', 'web', 'app', static_path)
        if i == 0:
            default_src = _static_path
        mtime = os.path.getmtime(report_path)
        size = os.path.getsize(report_path)
        # strftime = time.strftime("%Y%m%d %H:%M:%S", time.localtime(mtime))
        last_modified_time = datetime.fromtimestamp(mtime)
        last_modified_utctime = datetime.utcfromtimestamp(mtime)
        report = {
            'name': _report,
            # 'desc': '大小{}kb，最后修改{}'.format(round(size / 1024, 2), strftime),
            'page': page,
            'src': _static_path,
            'size': round(size / 1024, 2),
            'last_modified_time': last_modified_time,
            'last_modified_utctime': last_modified_utctime,
        }
        reports.append(report)
        count += 1
        if count == num_each_page:
            page += 1
            count = 0
    pages = page  # 总页数
    if pages >= 5 and len(all_reports) % 5 == 0:  # 去除多统计的1页
        pages -= 1
    return render_template('main/reports.html', reports=reports, pages=pages, default_src=default_src)
