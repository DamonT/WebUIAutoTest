# coding=utf-8

"""
Author: zhangzheng
Description: 供Jinja2使用的全局函数
Version: 0.0.1
LastUpdateDate: 
UpadteURL: 
LOG: 
"""

from flask import url_for

from src.config.config import Config as ProjectConfig


def get_ellipsis_loc(show_page_num: int) -> int:
    """提供翻页按钮添加...号的位置"""
    loc = show_page_num // 2
    if show_page_num % 2 == 0:
        return loc
    else:
        return loc + 1


def url_for_static(endpoint, **values):
    """重新实现url_for为静态资源加上版本号"""
    if endpoint.strip().lower() == 'static':
        values['version'] = ProjectConfig.version
    return url_for(endpoint=endpoint, **values)


if __name__ == '__main__':
    print(get_ellipsis_loc(5))
