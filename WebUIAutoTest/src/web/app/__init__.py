# coding=utf-8

from flask import Flask
import os
import logging
from logging.handlers import RotatingFileHandler
from concurrent_log_handler import ConcurrentRotatingFileHandler

from src.web.app.extensions import db, bootstrap, migrate, login_manager, mail, moment, csrf, cache
from src.web.app.config.config import Config
from src.web.app.template_global import get_ellipsis_loc, url_for_static
from src.web.app import models

from src.config.config import Config as ProjectConfig


# TODO 解决构建页面在谷歌浏览器下自动下拉滚动显示问题
def create_app(config_class=Config):
    # 创建flask并加载配置
    app = Flask(import_name=__name__)
    app.config.from_object(obj=config_class)

    register_logging(app)
    register_extensions(app)
    register_blueprints(app)
    register_commands(app)
    register_shell_context(app)
    register_template_context(app)
    register_template_global(app)

    return app


def register_logging(app: Flask):
    # 日志记录
    webapp_dir = os.path.dirname(__file__)
    if not os.path.exists(os.path.join(webapp_dir, 'logs')):
        os.mkdir(os.path.join(webapp_dir, 'logs'))
    # 改用ConcurrentRotatingFileHandler避免出现多进程时出现文件争用情况
    # file_handler = RotatingFileHandler(filename=os.path.join(webapp_dir, 'logs', 'webserver.log'),
    #                                    maxBytes=1024 * 1024 * 10,
    #                                    backupCount=10)
    file_handler = ConcurrentRotatingFileHandler(filename=os.path.join(webapp_dir, 'logs', 'webserver.log'),
                                       maxBytes=1024 * 1024 * 10,
                                       backupCount=10)
    file_handler.setFormatter(logging.Formatter(
        '%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'
    ))
    # file_sys_handler = RotatingFileHandler(filename=os.path.join(webapp_dir, 'logs', 'webserver_sys.log'),
    #                                        maxBytes=1024 * 1024 * 10,
    #                                        backupCount=10)
    file_sys_handler = ConcurrentRotatingFileHandler(filename=os.path.join(webapp_dir, 'logs', 'webserver_sys.log'),
                                                     maxBytes=1024 * 1024 * 10,
                                                     backupCount=10)
    file_sys_handler.setFormatter(logging.Formatter(
        '%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'
    ))
    stream_handler = logging.StreamHandler()

    app.logger.addHandler(file_handler)
    app.logger.setLevel(logging.INFO)
    app.logger.addHandler(logging.StreamHandler())
    app.logger.info("WebServer 服务已启动")

    # 框架默认logger指定输出handler(包含了http请求日志)
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    logger.addHandler(file_sys_handler)
    stream_handler.setLevel(logging.INFO)
    logger.addHandler(stream_handler)


def register_extensions(app: Flask):
    # 将插件注册到app中
    db.init_app(app=app)
    migrate.init_app(app=app, db=db)
    bootstrap.init_app(app=app)
    login_manager.init_app(app=app)
    mail.init_app(app=app)
    moment.init_app(app=app)
    csrf.init_app(app=app)
    cache.init_app(app=app)


def register_blueprints(app: Flask):
    # 注册蓝图
    from src.web.app.auth import bp as auth_bp
    app.register_blueprint(blueprint=auth_bp, url_prefix="/auth")
    from src.web.app.main import bp as main_bp
    app.register_blueprint(blueprint=main_bp)
    from src.web.app.errors import bp as errors_bp
    app.register_blueprint(blueprint=errors_bp)


def register_shell_context(app: Flask):
    # 添加flask shell上下文对象，注册自定义shell命令
    from src.web.app.models import db, User, ExecPlan, BuildInfo
    from src.web.app.email import send_email

    @app.shell_context_processor
    def make_shell_context():
        return {
            'db': db,
            'User': User,
            'send_email': send_email,
            'ExecPlan': ExecPlan,
            'BuildInfo': BuildInfo,
        }


def register_template_context(app: Flask):
    # 注册jinja模板上下文
    @app.context_processor
    def context_processor():
        return {
            'version': ProjectConfig.version
        }
    # app.context_processor(context_processor)


def register_template_global(app: Flask):
    # 注册jinja全局函数
    app.add_template_global(get_ellipsis_loc, 'get_ellipsis_loc')
    app.add_template_global(url_for_static, 'url_for')


def register_commands(app: Flask):
    pass
