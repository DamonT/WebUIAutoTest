# coding=utf-8

try:
    from celery import Celery
    from celery.schedules import crontab
except ImportError:
    Celery = None


if Celery:
    celery = Celery(
        main=__name__,
        # broker='amqp://',
        broker='redis://localhost:6379/0',
        backend='rpc://',
        include=['src.web.app.tasks']
    )
    # 时区
    celery.conf.timezone = 'Asia/Shanghai'
    # 是否使用UTC
    celery.conf.enable_utc = False

    celery.conf.beat_schedule = {
        'run-once-every-day': {
            # 执行tasks下的run_test函数
            'task': 'src.web.app.tasks.run_test',

            # 每天指定时间执行一次
            'schedule': crontab(minute='30', hour='0'),

            # 传递参数
            # 'args': ('a',)
        },
    }
