# coding=utf-8

from flask import current_app
from flask_login import UserMixin, current_user
from datetime import datetime, timedelta
from werkzeug.security import generate_password_hash, check_password_hash
import jwt
from time import time
from typing import Optional, Dict, List
from sqlalchemy.sql import func

from src.web.app.extensions import db
from src.web.app.util.util import create_hash
from src.web.app.util.node import node_for_build, node_for_plan


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), nullable=False)
    email = db.Column(db.String(120), nullable=False)
    password_hash = db.Column(db.String(128), nullable=False)
    last_seen = db.Column(db.DateTime, default=datetime.now, nullable=False)
    build_info = db.relationship('BuildInfo', back_populates='user')

    __table_args__ = (
        db.UniqueConstraint('username', 'email', name='uix_user_username_email'),
    )

    def __repr__(self):
        return '<User id:{}, username:{}>'.format(self.id, self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password=password)

    def check_password(self, passowrd):
        return check_password_hash(pwhash=self.password_hash, password=passowrd)

    @staticmethod
    def add(username, email, password):
        user = __class__(username=username, email=email)
        user.set_password(password=password)
        db.session.add(user)
        db.session.commit()

    @staticmethod
    def update_password(username: str, email: str, password: str):
        user = __class__.query.filter_by(username=username, email=email).first()
        user.set_password(password=password)
        db.session.commit()

    @staticmethod
    def get_email_token(email: str, expires_in=600) -> str:
        return jwt.encode(
            {'email': email, 'exp': time() + expires_in},
            key=current_app.config['SECRET_KEY'],
            algorithm='HS256'
        ).decode('utf-8')

    @staticmethod
    def get_email_from_token(token: str) -> Optional[str]:
        try:
            email = jwt.decode(
                token,
                key=current_app.config['SECRET_KEY'],
                algorithms=['HS256']
            )['email']
            return email
        except:
            return None

    @staticmethod
    def check_existence_buy_username_email(username, email):
        if __class__.query.filter_by(username=username, email=email).all():
            return True
        else:
            return False


class ExecPlan(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    checked = db.Column(db.Boolean(), nullable=False)
    hash = db.Column(db.String(100), nullable=False)
    data = db.Column(db.JSON, nullable=False)
    serial_no = db.Column(db.Integer, nullable=False)
    responsible_person = db.Column(db.String, nullable=False, default=' ', server_default=' ')  # server_default DDL时默认值
    version = db.Column(db.String, nullable=False)

    __table_args__ = (
        db.UniqueConstraint('name', 'serial_no', 'version', name='uix_execplan_name_serial_no_version'),
    )

    @staticmethod
    def empty() -> bool:
        return not bool(__class__.query.all())

    @staticmethod
    def add(name, _hash, data, serial_no, version, checked, responsible_person):
        row = __class__(
            name=name,
            hash=_hash,
            data=data,
            serial_no=serial_no,
            version=version,
            checked=checked,
            responsible_person=responsible_person
        )
        db.session.add(row)
        db.session.commit()

    @staticmethod
    def default_data() -> List[List[Dict]]:
        """返回最新的构建和执行计划数据"""
        if not __class__.empty():
            latest_version = __class__.version_max()
            all_latest_plan = __class__.query.filter_by(version=latest_version).order_by(__class__.serial_no.asc()).all()
            return [plan.data for plan in all_latest_plan]

    @staticmethod
    def update_row(name, data):
        __class__.query.filter_by(name=name).update(dict(data=data))
        db.session.commit()

    @staticmethod
    def version_max() -> str:
        """获取最大版本号"""
        return db.session.query(func.max(__class__.version)).scalar()

    @staticmethod
    def get_build_data() -> List[Dict]:
        """生成构建计划json数据"""
        # ret = [
        #     {
        #         'id': 1,
        #         'pId': 0,
        #         'name': '构建计划',
        #         'open': True,
        #         'isParent': True,
        #         'drag': False,
        #         'isRoot': True,
        #     },
        # ]
        ret = [
            node_for_build(
                id=1,
                pId=0,
                name='构建计划',
                open=True,
                isParent=True,
                drag=False,
                isRoot=True,
            )
        ]
        if not __class__.empty():
            latest_version = __class__.version_max()
            for row in __class__.query.filter_by(version=latest_version).order_by(__class__.serial_no.asc()).all():
                # ret.append(dict(
                #     id=row.name,
                #     pId=1,
                #     name=row.name,
                #     checked=row.checked,
                #     buildHash=row.hash,  # 自定义
                #     node_type='build',  # 自定义
                #     icon='/static/icon/plan.png',
                #     responsiblePerson=row.responsible_person,  # 自定义
                # ))
                ret.append(node_for_build(
                    id=row.name,
                    pId=1,
                    name=row.name,
                    checked=row.checked,
                    buildHash=row.hash,  # 自定义
                    node_type='build',  # 自定义
                    icon='/static/icon/plan.png',
                    responsiblePerson=row.responsible_person,
                ))
        return ret

    @staticmethod
    def get_plan_data_by_build_hash(_hash=None, name=None) -> List[Dict]:
        """
            根据指定条件(执行计划对应的hash值)获取执行计划json数据plan_data
        :param _hash: 执行计划对应的hash值，也是build节点的hash值
        :param name: 如果hash是none，则根据该name参数生成执行计划的plan_data默认值
        :return: json对应的py对象
        """
        if not __class__.empty():
            res = __class__.query.filter_by(hash=_hash).first()
            if res:
                return res.data
        if not _hash:
            _hash = create_hash(24)
        # ret = [{
        #     'id': 1,
        #     'pId': 0,
        #     'name': name,
        #     'open': True,
        #     'isParent': True,
        #     'drag': False,
        #     'isRoot': True,
        #     'copyMoveCount': 0,  # 自定义
        #     'buildHash': _hash,  # 自定义
        # }, ]
        ret = [
            node_for_plan(
                id=1,
                pId=0,
                name=name,
                open=True,
                isParent=True,
                drag=False,
                isRoot=True,
                copyMoveCount=0,
                buildHash=_hash,
            )
        ]
        return ret


class BuildInfo(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    build_time = db.Column(db.DateTime, default=datetime.now, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    user = db.relationship('User', back_populates='build_info')

    def __repr__(self):
        return '<BuildInfo id:{}, user_id:{}, build_time: {}>'.format(self.id, self.user_id, self.build_time)

    @staticmethod
    def add():
        """每次构建会插入一条流水信息"""
        user_id = current_user.id
        db.session.add(__class__(user_id=user_id))
        db.session.commit()

    @staticmethod
    def get_last_30_days_build_counts(user_id=None) -> List[Dict]:
        """统计30天内指定用户或所有用户的构建次数"""
        now = datetime.now()
        year, month, day = now.year, now.month, now.day
        ret = []
        for i in range(30):
            start_day = datetime(year, month, day, 0, 0, 0) - timedelta(days=i)
            end_day = datetime(year, month, day, 23, 59, 59) - timedelta(days=i)
            if user_id:
                rows = __class__.query\
                                .filter(__class__.user_id == user_id,
                                        __class__.build_time > start_day,
                                        __class__.build_time < end_day)\
                                .all()
            else:
                rows = __class__.query \
                                .filter(__class__.build_time > start_day,
                                        __class__.build_time < end_day) \
                                .all()
            ret.insert(0, dict(
                date=str(start_day.month) + '/' + str(start_day.day),
                count=len(rows),
            ))
        return ret
