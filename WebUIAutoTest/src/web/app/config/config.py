# coding=utf-8

import os

from src.config.config import Config as ProjectConfig


basedir = os.path.join(ProjectConfig.project_dir, 'src', 'web')


class Config:

    # SERVER_NAME = 'WebUiAutoTest:5000'  # enable subdomain support
    # SERVER_NAME = '0.0.0.0:5000'  # enable subdomain support

    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-never-guess'

    # Flask-SQLAlchemy插件从SQLALCHEMY_DATABASE_URI配置变量中获取应用的数据库的位置
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or 'sqlite:///' + os.path.join(basedir, 'app', 'app.db')
    # SQLALCHEMY_TRACK_MODIFICATIONS配置项用于设置数据发生变更之后是否发送信号给应用，我不需要这项功能，因此将其设置为False。
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # flask_mail 配置
    # MAIL_SERVER = 'smtp.163.com'
    # MAIL_PORT = 25
    # MAIL_USE_TLS = False
    # MAIL_USERNAME = '***********@163.com'
    # MAIL_PASSWORD = 'SQ************'
    # ADMINS = ['***********@163.com']
    MAIL_SERVER = ProjectConfig.smtp_server_address
    MAIL_PORT = ProjectConfig.smtp_port
    MAIL_USE_TLS = ProjectConfig.smtp_tls
    MAIL_USE_SSL = ProjectConfig.smtp_ssl
    MAIL_USERNAME = ProjectConfig.smtp_login_name
    MAIL_PASSWORD = ProjectConfig.smtp_login_passwd
    MAIL_DEFAULT_SENDER = ('Admin', MAIL_USERNAME)
    ADMINS = [ProjectConfig.smtp_login_name]

    # flask_cache 配置
    CACHE_TYPE = 'simple'
    CACHE_NO_NULL_WARNING = True

    # csrf 配置
    WTF_CSRF_TIME_LIMIT = 3600 * 24  # 24h
