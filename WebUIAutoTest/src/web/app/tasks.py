# coding=utf-8

from src.web.app.celery import celery
from src.config.config import Config as ProjectConfig
from src import Log

import os
import subprocess
import traceback


@celery.task
def run_test():
    logger = Log()
    logger.info('定时启动构建开始启动')
    try:
        cmd = os.path.join(ProjectConfig.project_dir, 'runtest.bat')
        subprocess.Popen(cmd,
                         shell=True,
                         stdin=subprocess.PIPE)
    except Exception as e:
        logger.error('定时构建启动成功失败，错误信息:\n %s' % traceback.format_exc())
    finally:
        logger.info('定时启动构建启动成功')
