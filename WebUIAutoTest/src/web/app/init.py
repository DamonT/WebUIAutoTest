# coding=utf-8

from src.config.config import Config

import shutil
import os


def web_init():
    """
    web服务启动初始化工作
    """
    src = os.path.join(Config.project_dir, 'Report')
    dst = os.path.join(Config.project_dir, 'src', 'web', 'app', 'static', 'report')
    if os.path.exists(dst):
        shutil.rmtree(dst)
    if os.path.exists(src):
        shutil.copytree(src=src, dst=dst)
