# coding=utf-8

"""
作者：azheng
文件名：config.py
文档描述：
最近修改：2019年12月7日
修改记录：
    1、2018年6月24日  1) 将confing.ini log相关配置获取移动至logconfig.py文件中
                     2) 对配置读取时做异常保护，并输出至日志
    2、2018年11月20日 1) 增加config.ini文件DATABASE.dbType配置项
    3、2019年8月23日 1) 优化日志模块记录逻辑
    4、2019年12月7日 1) 方法与变量名使用小写加下划线的形式，同时也修改config.ini文件中的option值

"""

import configparser
from configparser import NoOptionError, NoSectionError
import os
import codecs
import traceback
from typing import NoReturn, Dict, List

from src.public.public import Public
from src.log.log import Log

PROJECT_DIR = os.path.dirname(
    os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
)

CONFIG_PATH = os.path.join(PROJECT_DIR, 'Config', 'config.ini')

logger = Log()


class GetConfig(object):
    @staticmethod
    def get_config() -> configparser.ConfigParser:
        config = configparser.ConfigParser()
        with open(
            file=CONFIG_PATH, mode="rb"
        ) as f:  # 2018-6-10 BEGIN 判断文件的编码类型 如果是UTF-8 BOM格式，则将其转化为无BOM格式
            s = f.read()
        if s.startswith(codecs.BOM_UTF8):  # 带BOM的文件是以 b'\xef\xbb\xbf' 开头
            s = s[len(codecs.BOM_UTF8):]  # 截取 b'\xef\xbb\xbf' 到文件结尾
            with open(file=CONFIG_PATH, mode="wb") as f:  # 保存为无BOM格式
                f.write(s)
        coding = Public.get_file_coding(filepath=CONFIG_PATH).get("encoding")
        config.read(
            filenames=CONFIG_PATH, encoding=coding
        )  # 2018-6-10 END 判断文件的编码类型 如果是UTF-8 BOM格式，则将其转化为无BOM格式
        return config

    def _write_config(self, config):
        coding = Public.get_file_coding(filepath=CONFIG_PATH).get("encoding")
        with open(CONFIG_PATH, mode='w', encoding=coding) as f:
            config.write(f)

    def _prop_setter(self, section, option, value):
        try:
            config = self.get_config()
            config.set(section=section, option=option, value=value)
            self._write_config(config)
        except Exception as e:
            err_msg = '...\\项目目录\\Config\\config.ini更新配置 %s.%s 选项 值 "%s" 出错。\n错误详情：\n %s' % \
                      (section, option, value, traceback.format_exc())
            logger.warning(err_msg)
            raise RuntimeError(err_msg) from e  # 供web捕获异常，前台提示

    def _delete_all_options(self, section):
        config = self.get_config()
        options = config.options(section=section)
        for option in options:
            config.remove_option(section=section, option=option)
        self._write_config(config=config)

    @property
    def project_dir(self) -> str:
        # return self.get_config().get(section="PROJECT", option="project_dir")
        # 不在需要配置项目路径，项目迁移更加灵活。
        return PROJECT_DIR

    @property
    def version(self) -> str:
        section = "PROJECT"
        option = "version"
        default = None
        try:
            value = self.get_config().get(section=section, option=option)
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                '...\\项目目录\\Config\\config.ini文件未配置 %s.%s 选项，将采用默认值"%s"' %
                (section, option, value)
            )
        return value

    @property
    def manage_case_type(self) -> str:
        section = "PROJECT"
        option = "manage_case_type"
        default = 'excel'
        try:
            value = self.get_config().get(section=section, option=option)
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                '...\\项目目录\\Config\\config.ini文件未配置 %s.%s 选项，将采用默认值"%s"' %
                (section, option, value)
            )
        return value

    @property
    def browser_type(self) -> str:
        section = "PROJECT"
        option = "browser_type"
        default = "Chrome"
        try:
            value = self.get_config().get(section=section, option=option)
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                '...\\项目目录\\Config\\config.ini文件未配置 %s.%s 选项，将采用默认值"%s"' %
                (section, option, value)
            )
        return value

    @browser_type.setter
    def browser_type(self, value) -> NoReturn:
        section = "PROJECT"
        option = "browser_type"
        self._prop_setter(section=section, option=option, value=value)

    @property
    def browser_path_chrome(self) -> str:
        section = "PROJECT"
        option = "browser_path_chrome"
        default = ""
        try:
            value = self.get_config().get(section=section, option=option)
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                '...\\项目目录\\Config\\config.ini文件未配置 %s.%s 选项，将采用默认值"%s"' %
                (section, option, value)
            )
        return value

    @browser_path_chrome.setter
    def browser_path_chrome(self, value) -> NoReturn:
        section = "PROJECT"
        option = "browser_path_chrome"
        self._prop_setter(section=section, option=option, value=value)

    @property
    def browser_path_firefox(self) -> str:
        section = "PROJECT"
        option = "browser_path_firefox"
        default = ""
        try:
            value = self.get_config().get(section=section, option=option)
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                '...\\项目目录\\Config\\config.ini文件未配置 %s.%s 选项，将采用默认值"%s"' %
                (section, option, value)
            )
        return value

    @browser_path_firefox.setter
    def browser_path_firefox(self, value) -> NoReturn:
        section = "PROJECT"
        option = "browser_path_firefox"
        self._prop_setter(section=section, option=option, value=value)

    @property
    def browser_path_360(self) -> str:
        section = "PROJECT"
        option = "browser_path_360"
        default = ""
        try:
            value = self.get_config().get(section=section, option=option)
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                '...\\项目目录\\Config\\config.ini文件未配置 %s.%s 选项，将采用默认值"%s"' %
                (section, option, value)
            )
        return value

    @browser_path_360.setter
    def browser_path_360(self, value) -> NoReturn:
        section = "PROJECT"
        option = "browser_path_360"
        self._prop_setter(section=section, option=option, value=value)

    @property
    def timeout(self) -> int:
        section = "TEST"
        option = "timeout"
        default = 10
        try:
            value = self.get_config().getint(section=section, option=option)
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                '...\\项目目录\\Config\\config.ini文件未配置 %s.%s 选项，将采用默认值"%s"' %
                (section, option, value)
            )
        return value

    @timeout.setter
    def timeout(self, value) -> NoReturn:
        section = "TEST"
        option = "timeout"
        self._prop_setter(section=section, option=option, value=value)

    @property
    def headless(self) -> bool:
        section = "TEST"
        option = "headless"
        default = False
        try:
            value = self.get_config().getboolean(section=section, option=option)
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                '...\\项目目录\\Config\\config.ini文件未配置 %s.%s 选项，将采用默认值"%s"' %
                (section, option, value)
            )
        return value

    @headless.setter
    def headless(self, value) -> NoReturn:
        section = "TEST"
        option = "headless"
        self._prop_setter(section=section, option=option, value=value)

    @property
    def skip_test_method(self) -> bool:
        section = "TEST"
        option = "skip_test_method"
        default = False
        try:
            value = self.get_config().getboolean(section=section, option=option)
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                '...\\项目目录\\Config\\config.ini文件未配置 %s.%s 选项，将采用默认值"%s"' %
                (section, option, value)
            )
        return value

    @skip_test_method.setter
    def skip_test_method(self, value) -> NoReturn:
        section = "TEST"
        option = "skip_test_method"
        self._prop_setter(section=section, option=option, value=value)

    @property
    def send_email(self) -> bool:
        section = "EMAIL"
        option = "send_email"
        default = False
        try:
            value = self.get_config().getboolean(section=section, option=option)
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                '...\\项目目录\\Config\\config.ini文件未配置 %s.%s 选项，将采用默认值"%s"' %
                (section, option, value)
            )
        return value

    @send_email.setter
    def send_email(self, value) -> NoReturn:
        section = "EMAIL"
        option = "send_email"
        self._prop_setter(section=section, option=option, value=value)

    @property
    def gen_report(self) -> bool:
        section = "EMAIL"
        option = "gen_report"
        default = True
        try:
            value = self.get_config().getboolean(section=section, option=option)
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                '...\\项目目录\\Config\\config.ini文件未配置 %s.%s 选项，将采用默认值"%s"' %
                (section, option, value)
            )
        return value

    @gen_report.setter
    def gen_report(self, value) -> NoReturn:
        section = "EMAIL"
        option = "gen_report"
        self._prop_setter(section=section, option=option, value=value)

    @property
    def smtp_login_name(self) -> str:
        section = "EMAIL"
        option = "smtp_login_name"
        default = "********@outlook.com"
        try:
            value = self.get_config().get(section=section, option=option)
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                '...\\项目目录\\Config\\config.ini文件未配置 %s.%s 选项，将采用默认值"%s"' %
                (section, option, value)
            )
        return value

    @smtp_login_name.setter
    def smtp_login_name(self, value) -> NoReturn:
        section = "EMAIL"
        option = "smtp_login_name"
        self._prop_setter(section=section, option=option, value=value)

    @property
    def smtp_login_passwd(self) -> str:
        section = "EMAIL"
        option = "smtp_login_passwd"
        default = "**********"
        try:
            value = self.get_config().get(section=section, option=option)
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                '...\\项目目录\\Config\\config.ini文件未配置 %s.%s 选项，将采用默认值"%s"' %
                (section, option, value)
            )
        return value

    @smtp_login_passwd.setter
    def smtp_login_passwd(self, value) -> NoReturn:
        section = "EMAIL"
        option = "smtp_login_passwd"
        self._prop_setter(section=section, option=option, value=value)

    @property
    def sender_address(self) -> str:
        section = "EMAIL"
        option = "sender_address"
        default = "********@outlook.com"
        try:
            value = self.get_config().get(section=section, option=option)
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                '...\\项目目录\\Config\\config.ini文件未配置 %s.%s 选项，将采用默认值"%s"' %
                (section, option, value)
            )
        return value

    @sender_address.setter
    def sender_address(self, value) -> NoReturn:
        section = "EMAIL"
        option = "sender_address"
        self._prop_setter(section=section, option=option, value=value)

    @property
    def receiver_address(self) -> list:
        section = "EMAIL"
        option = "receiver_address"
        default = []
        try:
            address = self.get_config().get(section=section, option=option)
            value = []
            for _address_splited in address.split(","):
                if _address_splited:
                    value.append(_address_splited.strip())
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                '...\\项目目录\\Config\\config.ini文件未配置 %s.%s 选项，将采用默认值"%s"' %
                (section, option, value)
            )
        return value

    @receiver_address.setter
    def receiver_address(self, value) -> NoReturn:
        section = "EMAIL"
        option = "receiver_address"
        self._prop_setter(section=section, option=option, value=value)

    @property
    def smtp_server_address(self) -> str:
        section = "EMAIL"
        option = "smtp_server_address"
        default = "smtp-mail.outlook.com"
        try:
            value = self.get_config().get(section=section, option=option)
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                '...\\项目目录\\Config\\config.ini文件未配置 %s.%s 选项，将采用默认值"%s"' %
                (section, option, value)
            )
        return value

    @smtp_server_address.setter
    def smtp_server_address(self, value) -> NoReturn:
        section = "EMAIL"
        option = "smtp_server_address"
        self._prop_setter(section=section, option=option, value=value)

    @property
    def smtp_port(self) -> int:
        section = "EMAIL"
        option = "smtp_port"
        default = 587
        try:
            value = self.get_config().getint(section=section, option=option)
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                '...\\项目目录\\Config\\config.ini文件未配置 %s.%s 选项，将采用默认值"%s"' %
                (section, option, value)
            )
        return value

    @smtp_port.setter
    def smtp_port(self, value) -> NoReturn:
        section = "EMAIL"
        option = "smtp_port"
        self._prop_setter(section=section, option=option, value=value)

    @property
    def smtp_tls(self) -> bool:
        section = "EMAIL"
        option = "smtp_tls"
        default = True
        try:
            value = self.get_config().getboolean(section=section, option=option)
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                '...\\项目目录\\Config\\config.ini文件未配置 %s.%s 选项，将采用默认值"%s"' %
                (section, option, value)
            )
        return value

    @smtp_tls.setter
    def smtp_tls(self, value) -> NoReturn:
        section = "EMAIL"
        option = "smtp_tls"
        self._prop_setter(section=section, option=option, value=value)

    @property
    def smtp_ssl(self) -> bool:
        section = "EMAIL"
        option = "smtp_ssl"
        default = True
        try:
            value = self.get_config().getboolean(section=section, option=option)
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                '...\\项目目录\\Config\\config.ini文件未配置 %s.%s 选项，将采用默认值"%s"' %
                (section, option, value)
            )
        return value

    @smtp_ssl.setter
    def smtp_ssl(self, value) -> NoReturn:
        section = "EMAIL"
        option = "smtp_ssl"
        self._prop_setter(section=section, option=option, value=value)

    @property
    def email_title(self) -> str:
        section = "EMAIL"
        option = "email_title"
        default = "Web自动化测试报告邮件..."
        try:
            value = self.get_config().get(section=section, option=option)
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                '...\\项目目录\\Config\\config.ini文件未配置 %s.%s 选项，将采用默认值"%s"' %
                (section, option, value)
            )
        return value

    @email_title.setter
    def email_title(self, value) -> NoReturn:
        section = "EMAIL"
        option = "email_title"
        self._prop_setter(section=section, option=option, value=value)

    @property
    def email_text(self) -> str:
        section = "EMAIL"
        option = "email_text"
        default = "详细测试报告请查看附件..."
        try:
            value = self.get_config().get(section=section, option=option)
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                '...\\项目目录\\Config\\config.ini文件未配置 %s.%s 选项，将采用默认值"%s"' %
                (section, option, value)
            )
        return value

    @email_text.setter
    def email_text(self, value) -> NoReturn:
        section = "EMAIL"
        option = "email_text"
        self._prop_setter(section=section, option=option, value=value)

    @property
    def title(self) -> str:
        section = "REPORT"
        option = "title"
        default = "Web自动化测试报告"
        try:
            value = self.get_config().get(section=section, option=option)
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                '...\\项目目录\\Config\\config.ini文件未配置 %s.%s 选项，将采用默认值"%s"' %
                (section, option, value)
            )
        return value

    @title.setter
    def title(self, value) -> NoReturn:
        section = "REPORT"
        option = "title"
        self._prop_setter(section=section, option=option, value=value)

    @property
    def description(self) -> str:
        section = "REPORT"
        option = "description"
        default = "报告描述"
        try:
            value = self.get_config().get(section=section, option=option)
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                 '...\\项目目录\\Config\\config.ini文件未配置 %s.%s 选项，将采用默认值"%s"' %
                (section, option, value)
            )
        return value

    @description.setter
    def description(self, value) -> NoReturn:
        section = "REPORT"
        option = "description"
        self._prop_setter(section=section, option=option, value=value)

    @property
    def dbtype(self) -> str:
        section = "DATABASE"
        option = "dbtype"
        default = "MySQL"
        try:
            value = self.get_config().get(section=section, option=option)
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                 '...\\项目目录\\Config\\config.ini文件未配置 %s.%s 选项，将采用默认值"%s"' %
                (section, option, value)
            )
        return value

    @dbtype.setter
    def dbtype(self, value) -> NoReturn:
        section = "DATABASE"
        option = "dbtype"
        self._prop_setter(section=section, option=option, value=value)

    @property
    def dsn(self) -> str:
        section = "ORACLE"
        option = "dsn"
        default = "DSN_Oracle"
        try:
            value = self.get_config().get(section=section, option=option)
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                '...\\项目目录\\Config\\config.ini文件未配置 %s.%s 选项，将采用默认值"%s"' %
                (section, option, value)
            )
        return value

    @dsn.setter
    def dsn(self, value) -> NoReturn:
        section = "ORACLE"
        option = "dsn"
        self._prop_setter(section=section, option=option, value=value)

    @property
    def user_oracle(self) -> str:
        section = "ORACLE"
        option = "user"
        default = "sys"
        try:
            value = self.get_config().get(section=section, option=option)
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                '...\\项目目录\\Config\\config.ini文件未配置 %s.%s 选项，将采用默认值"%s"' %
                (section, option, value)
            )
        return value

    @user_oracle.setter
    def user_oracle(self, value) -> NoReturn:
        section = "ORACLE"
        option = "user"
        self._prop_setter(section=section, option=option, value=value)

    @property
    def passwd_oracle(self) -> str:
        section = "ORACLE"
        option = "passwd"
        default = None
        try:
            value = self.get_config().get(section=section, option=option)
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                '...\\项目目录\\Config\\config.ini文件未配置 %s.%s 选项，将采用默认值"%s"' %
                (section, option, value)
            )
        return value

    @passwd_oracle.setter
    def passwd_oracle(self, value) -> NoReturn:
        section = "ORACLE"
        option = "passwd"
        self._prop_setter(section=section, option=option, value=value)

    @property
    def host(self) -> str:
        section = "MYSQL"
        option = "host"
        default = "localhost"
        try:
            value = self.get_config().get(section=section, option=option)
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                '...\\项目目录\\Config\\config.ini文件未配置 %s.%s 选项，将采用默认值"%s"' %
                (section, option, value)
            )
        return value

    @host.setter
    def host(self, value) -> NoReturn:
        section = "MYSQL"
        option = "host"
        self._prop_setter(section=section, option=option, value=value)

    @property
    def user_mysql(self) -> str:
        section = "MYSQL"
        option = "user"
        default = "root"
        try:
            value = self.get_config().get(section=section, option=option)
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                '...\\项目目录\\Config\\config.ini文件未配置 %s.%s 选项，将采用默认值"%s"' %
                (section, option, value)
            )
        return value

    @user_mysql.setter
    def user_mysql(self, value) -> NoReturn:
        section = "MYSQL"
        option = "user"
        self._prop_setter(section=section, option=option, value=value)

    @property
    def passwd_mysql(self) -> str:
        section = "MYSQL"
        option = "passwd"
        default = "root"
        try:
            value = self.get_config().get(section=section, option=option)
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                '...\\项目目录\\Config\\config.ini文件未配置 %s.%s 选项，将采用默认值"%s"' %
                (section, option, value)
            )
        return value

    @passwd_mysql.setter
    def passwd_mysql(self, value) -> NoReturn:
        section = "MYSQL"
        option = "passwd"
        self._prop_setter(section=section, option=option, value=value)

    @property
    def port(self) -> int:
        section = "MYSQL"
        option = "port"
        default = 3306
        try:
            value = self.get_config().getint(section=section, option=option)
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                '...\\项目目录\\Config\\config.ini文件未配置 %s.%s 选项，将采用默认值"%s"' %
                (section, option, value)
            )
        return value

    @port.setter
    def port(self, value) -> NoReturn:
        section = "MYSQL"
        option = "port"
        self._prop_setter(section=section, option=option, value=value)

    @property
    def charset(self) -> str:
        section = "MYSQL"
        option = "charset"
        default = 'utf8'
        try:
            value = self.get_config().get(section=section, option=option)
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                '...\\项目目录\\Config\\config.ini文件未配置 %s.%s 选项，将采用默认值"%s"' %
                (section, option, value)
            )
        return value

    @charset.setter
    def charset(self, value) -> NoReturn:
        section = "MYSQL"
        option = "charset"
        self._prop_setter(section=section, option=option, value=value)

    @property
    def notifier_enable(self) -> bool:
        section = "NOTIFIER"
        option = "enable"
        default = False
        try:
            value = self.get_config().getboolean(section=section, option=option)
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                '...\\项目目录\\Config\\config.ini文件未配置 %s.%s 选项，将采用默认值"%s"' %
                (section, option, value)
            )
        return value

    @notifier_enable.setter
    def notifier_enable(self, value) -> NoReturn:
        section = "NOTIFIER"
        option = "enable"
        self._prop_setter(section=section, option=option, value=value)

    @property
    def access_token(self) -> str:
        section = "NOTIFIER"
        option = "access_token"
        default = "xxxxxxxxxxx"
        try:
            value = self.get_config().get(section=section, option=option)
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                '...\\项目目录\\Config\\config.ini文件未配置 %s.%s 选项，将采用默认值"%s"' %
                (section, option, value)
            )
        return value

    @access_token.setter
    def access_token(self, value) -> NoReturn:
        section = "NOTIFIER"
        option = "access_token"
        self._prop_setter(section=section, option=option, value=value)

    @property
    def is_at_all(self) -> bool:
        section = "NOTIFIER"
        option = "is_at_all"
        default = True
        try:
            value = self.get_config().getboolean(section=section, option=option)
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                '...\\项目目录\\Config\\config.ini文件未配置 %s.%s 选项，将采用默认值"%s"' %
                (section, option, value)
            )
        return value

    @is_at_all.setter
    def is_at_all(self, value) -> NoReturn:
        section = "NOTIFIER"
        option = "is_at_all"
        self._prop_setter(section=section, option=option, value=value)

    @property
    def at_mobiles(self) -> list:
        section = "NOTIFIER"
        option = "at_mobiles"
        default = []
        try:
            value = self.get_config().get(section=section, option=option)
            value = [no.strip() for no in value.split(",")]
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                '...\\项目目录\\Config\\config.ini文件未配置 %s.%s 选项，将采用默认值"%s"' %
                (section, option, value)
            )
        return value

    @at_mobiles.setter
    def at_mobiles(self, value) -> NoReturn:
        section = "NOTIFIER"
        option = "at_mobiles"
        self._prop_setter(section=section, option=option, value=value)

    @property
    def responsible_person_email(self) -> Dict:
        section = "ResponsiblePersonEmail"
        default = {}
        try:
            value = dict(self.get_config().items(section))
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                '...\\项目目录\\Config\\config.ini文件未配置 %s 选项，将采用默认值"%s"' %
                (section, value)
            )
        return value

    @responsible_person_email.setter
    def responsible_person_email(self, rows: List[Dict]) -> NoReturn:
        section = "ResponsiblePersonEmail"
        self._delete_all_options(section=section)
        for row in rows:
            self._prop_setter(section=section, option=row.get('Name'), value=row.get('EMailAddress'))

    @property
    def extension_ui_enable(self) -> bool:
        section = "EXTENSION_UI"
        option = "extension_ui_enable"
        default = False
        try:
            value = self.get_config().getboolean(section=section, option=option)
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                '...\\项目目录\\Config\\config.ini文件未配置 %s.%s 选项，将采用默认值"%s"' %
                (section, option, value)
            )
        return value

    @extension_ui_enable.setter
    def extension_ui_enable(self, value) -> NoReturn:
        section = "EXTENSION_UI"
        option = "extension_ui_enable"
        self._prop_setter(section=section, option=option, value=value)

    @property
    def extension_ui_module_name(self) -> str:
        section = "EXTENSION_UI"
        option = "extension_ui_module_name"
        default = 'none.none'
        try:
            value = self.get_config().get(section=section, option=option)
        except (NoOptionError, NoSectionError):
            value = default
            logger.warning(
                '...\\项目目录\\Config\\config.ini文件未配置 %s.%s 选项，将采用默认值"%s"' %
                (section, option, value)
            )
        return value

    @extension_ui_module_name.setter
    def extension_ui_module_name(self, value) -> NoReturn:
        section = "EXTENSION_UI"
        option = "extension_ui_module_name"
        self._prop_setter(section=section, option=option, value=value)


# 2020-4-2 zz: 为了能够实时获取config.ini文件内容，将Config类改为Config对象(GetConfig的实例)
Config = GetConfig()
# class Config(object):
#     # 项目路径
#     project_dir = GetConfig().project_dir
#     # 版本
#     project_version = GetConfig().project_version
#     # 配置隐形等待时间
#     timeout = GetConfig().timeout
#     # 无头模式运行
#     headless = GetConfig().headless
#
#     # 案例运行浏览器类型
#     browser_type = GetConfig().browser_type  # Firefox\Chrome\360
#     browser_path_360 = GetConfig().browser_path_360
#
#     # Email
#     # 是否发送邮件
#     send_email = GetConfig().send_email
#     # 是否生成报告
#     gen_report = GetConfig().gen_report
#     # SMTP服务器登录名
#     smtp_login_name = GetConfig().smtp_login_name
#     # SMTP服务器登录密码
#     smtp_login_passwd = GetConfig().smtp_login_passwd
#     # 发件地址
#     sender_address = GetConfig().sender_address
#     # 收件地址
#     receiver_address = GetConfig().receiver_address
#     # SMTP服务器地址
#     smtp_server_address = GetConfig().smtp_server_address
#     # SMTP服务器地址端口
#     smtp_port = GetConfig().smtp_port
#     # 邮件主题
#     email_title = GetConfig().email_title
#     # 正文内容
#     email_text = GetConfig().email_text
#
#     # 报告的标题
#     title = GetConfig().title
#     # 报告的描述
#     description = GetConfig().description
#
#     # 数据库类型
#     dbtype = GetConfig().dbtype
#     # Oracle DSN配置
#     dsn = GetConfig().dsn
#     user_oracle = GetConfig().user_oracle
#     passwd_oracle = GetConfig().passwd_oracle
#     # MySQL 配置
#     host = GetConfig().host
#     user_mysql = GetConfig().user_mysql
#     passwd_mysql = GetConfig().passwd_mysql
#     port = GetConfig().port
#     charset = GetConfig().charset
#
#     # 钉钉通知器配置
#     notifier_enable = GetConfig().notifier_enable
#     access_token = GetConfig().access_token
#     is_at_all = GetConfig().is_at_all
#     at_mobiles = GetConfig().at_mobiles
