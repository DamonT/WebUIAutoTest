# coding=utf-8


class SeleniumException(Exception): pass


class SeleniumTypeError(SeleniumException): pass


class SeleniumValueError(SeleniumException): pass


class SeleniumTimeoutError(SeleniumException): pass


class SeleniumNotFoundProperty(SeleniumException): pass


class SeleniumElementNotFound(SeleniumException): pass


class CMDRunError(Exception): pass