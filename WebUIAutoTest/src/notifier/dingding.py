# coding=utf-8

"""
Author: azheng
Description: support for notifying messages using dingding robot.
Version: 0.0.1
LastUpdateDate: 2019-12-8
UpadteURL:
LOG: 1. 2019-12-8 The first version
"""

from urllib import request
import json

from src import Config


HEADERS = {
    'Content-Type': 'application/json',
    'Charset': 'UTF-8'
}

ACCESS_TOKEN = Config.access_token
AT_MOBILES = Config.at_mobiles
IS_AT_ALL = Config.is_at_all


def send_text_msg(content: str = "") -> str:
    parms = {
        'msgtype': 'text',
        'text': {
            "content": content
        },
        "at": {
            "atMobiles": AT_MOBILES,
            "isAtAll": IS_AT_ALL
        }
    }
    url = "https://oapi.dingtalk.com/robot/send?access_token=" + ACCESS_TOKEN
    # 发起post请求
    req = request.Request(url, data=json.dumps(parms).encode("utf-8"), headers=HEADERS)
    u = request.urlopen(req)
    # 读取响应内容
    return json.loads(u.read().decode("utf-8"))


if __name__ == "__main__":
    print(send_text_msg("通知，来自钉钉机器人的消息"))
