# coding=utf-8

# 提供日志记录功能
from .log.log import Log

# 提供扩展unittest.TestCase功能
from .testcasemore.testcasemore import TestCaseMore

# 经过二次封装的WebDirver浏览器驱动
from .newselenium.driver import Driver
from .newselenium.by import By
from .newselenium.keys import Keys

# 数据驱动类，里面提供数据驱动装饰器
from .public.datadriver import DataDriver

# 可以获取配置信息
from .config.config import Config

# 提供数据库数据比对
from .database.db import test_dbcheck

# 利用autoit进行鼠标键盘等windowsGUI操作
from .gui import Operate
# 快速执行AutoIt目录下的.au3脚本
from .gui import run_autoit

# 全局变量
from .public import g
