# coding=utf-8

from typing import Union, Tuple
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.alert import Alert

from src.newselenium.by import By
from src.newselenium.element import Element


class DriverWait(object):
    """
        self._driver 为selenium的原生webdriver类对象 <class WebDirver>
    """
    def __init__(self, driver, timeout: int, poll_frequency: int = 0.5, ignored_exceptions=None):
        self._driver = driver  # 浏览器 <class src.newselenium.driver.Driver>
        self._timeout = timeout
        self._poll_frequency = poll_frequency
        self._ignored_exceptions = ignored_exceptions
        self._WebDriverWait = WebDriverWait(driver=self._driver.web_driver,  # <class selenium.webdriver.chrome.webdriver.WebDriver>
                                            timeout=self._timeout,
                                            poll_frequency=self._poll_frequency,
                                            ignored_exceptions=self._ignored_exceptions)

    def presence_of_element_located(self, by: str = By.ID, value: str = "") -> Element:
        """
        说明：
            等待直到定位到元素
        :param by: 定位方式 <class By>
        :param value: 值 <class str>
        :return: 返回被定位的元素 <class Element>
        """
        web_element = self._WebDriverWait.until(
            EC.presence_of_element_located(locator=(by, value))
        )
        return Element(driver=self._driver, ele=web_element)

    def title_is(self, title: str) -> bool:
        """
        说明：
            等待直到匹配到指定浏览器的title
        :param title: title <class str>
        :return: returns True if the title matches, false otherwise <class bool>
        """
        return self._WebDriverWait.until(
                    EC.title_is(title=title)
                )

    def title_contains(self, title: str) -> bool:
        """
        说明：
            等待直到匹配到指定浏览器的title中包含某些字符串
        :param title: title <class str>
        :return: returns True when the title matches, False otherwise <class bool>
        """
        return self._WebDriverWait.until(
                    EC.title_contains(title=title)
               )

    def visibility_of_element_located(self, by: str = By.ID, value: str = "") -> Union[Element, bool]:
        """
        说明：
            等待直到定位的元素可见
        :param by: 定位方式 <class By>
        :param value: 值 <class str>
        :return: 如果定位到的元素可见则返回该元素 <class Element> 否则引起TimeoutException
        """
        web_element = self._WebDriverWait.until(
                    EC.visibility_of_element_located(locator=(by, value))
                  )
        return Element(driver=self._driver, ele=web_element) if web_element else False

    def visibility_of(self, element: Element) -> Union[Element, bool]:
        """
        说明：
            等待直到指定的元素可见且元素的长和宽大于0
        :param element: 期望检查的指定元素 <class Element>
        :return: 如果该元素存在则返回该元素 <class Element> 否则引起TimeoutException
        """
        web_element = element.change_to_webelement()
        ele = self._WebDriverWait.until(
               EC.visibility_of(element=web_element)
              )
        return Element(driver=self._driver, ele=ele) if ele else False

    def presence_of_all_elements_located(self, by: str = By.ID, value: str = "") -> list:
        """
        说明：
            等待直到定位到元素
        :param by: 定位方式 <class By>
        :param value: 值 <class str>
        :return: 返回 list of Elements
        """
        web_elements = self._WebDriverWait.until(
                    EC.presence_of_all_elements_located(locator=(by, value))
                  )
        if web_elements:
            return list(map(lambda ele: Element(driver=self._driver, ele=ele), web_elements))
        else:
            return web_elements

    def text_to_be_present_in_element(self, by: str = By.ID, value: str = "", text: str = "") -> bool:
        """
        说明：
            等待直到找到指定元素(参数:by value)的text属性包含指定的内容(参数:text)
        :param by: 定位方式 <class By>
        :param value: 值 <class str>
        :param text: text属性包含的内容
        :return: 如果存在返回True 否则引起TimeoutException
        """
        return self._WebDriverWait.until(
                    EC.text_to_be_present_in_element(locator=(by, value), text_=text)
               )

    def text_to_be_present_in_element_value(self, by: str = By.ID, value: str = "", text: str = "") -> bool:
        """
        说明：
            等待直到找到指定元素(参数:by value)的value属性包含指定的内容(参数:text)
        :param by: 定位方式 <class By>
        :param value: 值 <class str>
        :param text: text属性包含的内容
        :return: 如果存在返回True 否则引起TimeoutException
        """
        return self._WebDriverWait.until(
                    EC.text_to_be_present_in_element_value(locator=(by, value), text_=text)
               )

    def frame_to_be_available_and_switch_to_it(self, locator: Tuple[str, str] = (By.ID, "value")) -> bool:
        """
        说明：
            等待直到存在指定的frame可以switch_to,然后会switch_to该frame
        :param locator: 可以根据iframe/frame标签的id、name属性 或者(By.ID, "value")来定位
        :return: 如果存在并switch成功会返回True 否则引起TimeoutException
        """
        return self._WebDriverWait.until(
                    EC.frame_to_be_available_and_switch_to_it(locator=locator)
               )

    def invisibility_of_element_located(self, by: str = By.ID, value: str = "") -> Union[Element, bool]:
        """
        说明：
            等待直到存在指定的元素在当前DOM上不可见或不存在，如果不存在返回True 如果存在但不可见返回该元素element
        :param by: 定位方式 <class By>
        :param value: 值 <class str>
        :return: 如果不存在返回True <class bool> 如果存在但不可见返回该元素element <class Element>，否则引起TimeoutException
        """
        web_element = self._WebDriverWait.until(
                   EC.invisibility_of_element_located(locator=(by, value))
                  )
        return True if web_element else Element(driver=self._driver, ele=web_element)

    def element_to_be_clickable(self, by: str = By.ID, value: str = "") -> Element:
        """
        说明：
            等待直到指定的元素可见并可点击
        :param by: 定位方式 <class By>
        :param value: 值 <class str>
        :return: 如果存在则返回该元素，否则引起TimeoutException
        """
        web_element = self._WebDriverWait.until(
                   EC.element_to_be_clickable(locator=(by, value))
                  )
        return Element(driver=self._driver, ele=web_element)

    def staleness_of(self, element: Element) -> bool:
        """ Wait until an element is no longer attached to the DOM.
        element is the element to wait for.
        returns False if the element is still attached to the DOM, true otherwise.
        """
        web_element = element.change_to_webelement()
        return self._WebDriverWait.until(
                    EC.staleness_of(element=web_element)
               )

    def element_to_be_selected(self, element: Element) -> bool:
        """ An expectation for checking the selection is selected.
        element is WebElement object
        """
        web_element = element.change_to_webelement()
        return self._WebDriverWait.until(
                    EC.element_to_be_selected(element=web_element)
               )

    def element_located_to_be_selected(self, by: str = By.ID, value: str = "") -> bool:
        """An expectation for the element to be located is selected.
        locator is a tuple of (by, path)"""
        return self._WebDriverWait.until(
                    EC.element_located_to_be_selected(locator=(by, value))
               )

    def element_selection_state_to_be(self, element: Element, is_selected: bool) -> bool:
        """ An expectation for checking if the given element is selected.
        element is WebElement object
        is_selected is a Boolean."
        """
        web_element = element.change_to_webelement()
        return self._WebDriverWait.until(
                    EC.element_selection_state_to_be(element=web_element, is_selected=is_selected)
               )

    def element_located_selection_state_to_be(self, by: str, value: str, is_selected: bool) -> bool:
        """ An expectation to locate an element and check if the selection state
        specified is in that state.
        locator is a tuple of (by, path)
        is_selected is a boolean
        """
        return self._WebDriverWait.until(
                    EC.element_located_selection_state_to_be(locator=(by, value), is_selected=is_selected)
               )

    def alert_is_present(self) -> Union[Alert, bool]:
        """ Expect an alert to be present."""
        return self._WebDriverWait.until(
                    EC.alert_is_present()
               )
