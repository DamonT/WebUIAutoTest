# coding=utf-8

from typing import Iterable
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from typing import List, Dict, Tuple
import re
from time import sleep
import math

from src.exception.seleniumexecption import SeleniumNotFoundProperty, SeleniumElementNotFound
from src.newselenium.by import By


class ElementCBS:

    TABLE_LOAD_WAIT_TIME = 3  # table加载时的等待时间

    def cbs_select(self, value):
        """单选或多选下拉框"""
        ref = self.get_attribute('ref')
        if ref is None:
            raise SeleniumNotFoundProperty('当前选择框 [%s] 未找到属性 [ref]' % self.tag_name)
        try:
            tag_div = self._driver.get_element(By.CSS_SELECTOR, 'div[ref_target="%s"]' % ref)
        except (TimeoutException, NoSuchElementException) as e:
            raise SeleniumElementNotFound('未找到下拉框所在的div [%s]' % 'div[ref_target="%s"]') from e
        self.click()
        if isinstance(value, str):
            try:
                tag_div.get_element(By.CSS_SELECTOR, 'ul>li[title="%s"]' % value).click()
            except (TimeoutException, NoSuchElementException) as e:
                raise SeleniumElementNotFound('未找到下拉框选项 [%s]' % value) from e
        elif isinstance(value, Iterable):
            for _v in value:
                try:
                    tag_div.get_element(By.CSS_SELECTOR, 'ul>li[title="%s"]' % _v).click()
                except (TimeoutException, NoSuchElementException) as e:
                    raise SeleniumElementNotFound('未找到下拉框选项 [%s]' % _v) from e
        else:
            raise TypeError('value参数类型错误，只允许传入str 或 Iterable类型')
        # 勾选完毕后隐藏下拉框div
        tag_div.js_set_style_display_none()

    def cbs_table_test(self, wait=3) -> List[Dict]:
        """
        测试table元素
        :param wait: 表格加载时等待时间, 默认3s
        :return: 返回当前页面展示的数据
        """
        self.TABLE_LOAD_WAIT_TIME = wait
        sleep(self.TABLE_LOAD_WAIT_TIME)

        # 获取当前页面整个表头字段和表数据
        current_page_table_data = self._get_table_data()

        # 测试当前页面展示的个数是否与页面下端显示的个数是否一致（注意这里不是总个数而是当前第一页展示的个数）
        _, current_page_data_count, total_data_count = self._get_table_page_info()
        if len(current_page_table_data) != int(current_page_data_count):
            raise AssertionError(
                '当前表格首页展示的个数(%s)与表格下端显示的个数(%s)不一致' % (len(current_page_table_data), current_page_data_count)
            )

        # 获取所有数据（上限为10000个）
        # 测试总个数是否显示一致
        all_table_data = self._get_all_table_data()
        if len(all_table_data) != int(total_data_count):
            raise AssertionError(
                '当前表格总个数(%s)与表格下端显示的个数(%s)不一致' % (len(all_table_data), total_data_count)
            )

        # 测试翻页功能
        self._test_table_page_turn(total_data_count=total_data_count, all_table_data=all_table_data)

        return current_page_table_data

    def _get_table_data(self) -> List[Dict]:
        """获取当前页面整个表头字段和表数据"""
        # 拿到表头字段信息
        ele_table_header = self.get_element(By.CSS_SELECTOR, '.hc-datagrid-header')
        table_header_info = self._get_table_header_info(ele_table_header)

        # 表body元素（可能有多个，过滤掉其中冻结类型的table）
        ele_table_bodys = self.get_elements(By.CSS_SELECTOR, '.hc-datagrid-body')
        ele_table_body = None
        for _ele_table_body in ele_table_bodys:
            _ele_table_body_id = _ele_table_body.get_attribute('id')
            if 'freeze' in _ele_table_body_id:
                continue
            ele_table_body = _ele_table_body

        if not table_header_info:  # 存在表头在.hc-datagrid-body>table>thead中定义的情况
            # 拿到表头字段信息
            table_header_info = self._get_table_header_info_from_table_thead(ele_table_body)

        # 拿到表中的数据
        table_body_info = self._get_table_body_info(ele_table_body)

        # 将表字段与表数据整合在一个数据结构中
        table_data = self._integrate_table_data(table_header_info, table_body_info)
        return table_data

    def _integrate_table_data(self, header: List[Dict], body: List[List]) -> List[Dict]:
        """
        将表字段与表数据整合在一个数据结构中
        :param header: 表头
        :param body: 表数据
        :return: 整合后的数据
        """
        table_data = []
        for row in body:
            table_data_row = []
            for value, column in zip(row, header):
                row_data = dict(value=value)
                row_data.update(column)
                table_data_row.append(row_data)
            table_data.append(table_data_row)
        return table_data

    def _get_table_header_info(self, ele) -> List[Dict]:
        """
        获取表头字段数据
        :param ele: 表头的Element对象
        :return: 表头字段数据
        """
        ele_table_header = ele
        ele_table_header_table = ele_table_header.get_elements(By.CSS_SELECTOR, 'table')
        for _ele in ele_table_header_table:
            if 'freez' in _ele.get_property('id') or 'freeze' in _ele.get_property('id'):
                continue
            ele_table_header_table_tds = _ele.get_elements(By.CSS_SELECTOR, 'tr>td')
            return self._get_table_header_info_from_tds(ele_table_header_table_tds)

    def _get_table_header_info_from_table_thead(self, ele) -> List[Dict]:
        """
        获取表头字段数据，和_get_table_header_info方法不同的是，该表头在.hc-datagrid-body>table>thead中定义
        :param ele: 表的Element对象
        :return: 表头字段数据
        """
        ele_table_thead_tr_tds = ele.get_elements(By.CSS_SELECTOR, 'thead>tr>td')
        return self._get_table_header_info_from_tds(ele_table_thead_tr_tds)

    def _get_table_header_info_from_tds(self, ele):
        """
        根据表头tr下面所有td元素，拿到表头字段数据
        """
        ele_table_thead_tr_tds = ele
        table_header_info = []
        for _ele_td in ele_table_thead_tr_tds:
            _display_value = _ele_td.value_of_css_property('display')  # td是否显示
            _ele_td_name = _ele_td.get_attribute('name')  # td name属性
            _ele_td_div = _ele_td.get_element(By.CSS_SELECTOR, 'td>div')  # td > div
            _div_class_name = _ele_td_div.get_attribute('class')  # td > div 的class值
            _text_value = _ele_td_div.get_attribute('innerText').strip()
            if 'hc-datagrid-cell-check' in _div_class_name or \
                    'u-table-check' in _div_class_name or \
                    'u-table-number' in _div_class_name or \
                    'hc-datagrid-frozen-cell' in _div_class_name:
                continue
            if _display_value.strip() != 'none':
                table_header_info.append(dict(
                    display=True,
                    name=_ele_td_name,
                    column=_text_value
                ))
            else:
                table_header_info.append(dict(
                    display=False,
                    name=_ele_td_name,
                    column=_text_value
                ))
        return table_header_info

    def _get_table_body_info(self, ele) -> List[List]:
        """
        获取表格数据
        :param ele: 表格body元素
        :return: 解析到当前页面中所有数据
        """
        ele_table_body = ele
        ele_table_trs = ele_table_body.get_elements(By.CSS_SELECTOR, 'tbody>tr')  # 获取所有行
        rows = []
        for _ele_table_tr in ele_table_trs:  # 数据量大时该循环非常耗时(不建议数据超过100条)
            _ele_table_tr_td_divs = _ele_table_tr.get_elements(By.CSS_SELECTOR, 'tr>td>div')  # 获取一行中所有数据
            row = []
            for _ele_table_tr_td_div in _ele_table_tr_td_divs:
                _ele_table_tr_td_div_class_name = _ele_table_tr_td_div.get_attribute('class')
                if 'hc-datagrid-cell-check' in _ele_table_tr_td_div_class_name or \
                    'h_numbercolumn' in _ele_table_tr_td_div_class_name:
                    continue
                _ele_table_tr_td_div_text = _ele_table_tr_td_div.get_attribute('innerText').strip()
                row.append(_ele_table_tr_td_div_text)
            rows.append(row)
        return rows

    def _get_table_page_info(self) -> Tuple[int]:
        """
        返回当前页面展示页面信息
        :return: 总页数, 当前页面数据个数, 总数据个数
        """
        ele_page_div = self.get_element(By.CSS_SELECTOR, '.g-pagebar-wrap')
        ele_page_div_div = ele_page_div.get_element(By.CSS_SELECTOR, '.m-pagebar-all')
        page_info = ele_page_div_div.get_attribute('innerText')
        # 页面显示总条数
        [(total_page_count, _, current_page_data_count, total_data_count)] = \
            re.findall(r'共(\d+)页，显示(\d+)到(\d+)条，共(\d+)条记录', page_info)  # 共1页，显示1到20条，共20条记录
        return int(total_page_count), int(current_page_data_count), int(total_data_count)

    def _get_all_table_data(self) -> List[Dict]:
        """获取所有数据(上限10000条)"""
        self.show_table_data_count_per_page(count=10000)
        self._refresh_table()
        return self._get_table_data()

    def _test_table_page_turn(self, total_data_count, all_table_data):
        if total_data_count > 1:  # 只有当总数大于1时才测试翻页
            per_page_count = math.ceil(total_data_count / 2)
            self.show_table_data_count_per_page(count=per_page_count)
            self._refresh_table()
            # 第一页
            current_page_table_data = self._get_table_data()
            if current_page_table_data != all_table_data[0:per_page_count]:
                for _i, (_current_page_table_data_row, _all_table_data_row) in enumerate(zip(current_page_table_data, all_table_data[0:per_page_count])):
                    for _current_page_table_data_row_ceil, _all_table_data_row_ceil in zip(_current_page_table_data_row, _all_table_data_row):
                        if _current_page_table_data_row_ceil != _all_table_data_row_ceil:
                            raise AssertionError(
                                '翻页测试发现分页后第一页第%s条数据(%s)与表格所有数据中%s条数据(%s)不匹配，请检查' % (
                                    _i + 1, _current_page_table_data_row_ceil, _i + 1, _all_table_data_row_ceil
                                )
                            )
            # 点击下一页
            self.table_page_turn_next()
            current_page_table_data = self._get_table_data()
            if current_page_table_data != all_table_data[per_page_count:]:
                for _i, (_current_page_table_data_row, _all_table_data_row) in enumerate(zip(current_page_table_data, all_table_data[per_page_count:0])):
                    for _current_page_table_data_row_ceil, _all_table_data_row_ceil in zip(_current_page_table_data_row, _all_table_data_row):
                        if _current_page_table_data_row_ceil != _all_table_data_row_ceil:
                            raise AssertionError(
                                '翻页测试发现分页后第二页第%s条数据(%s)与表格所有数据中%s条数据(%s)不匹配，请检查' % (
                                    _i + 1, _current_page_table_data_row_ceil, _i + 1 + per_page_count, _all_table_data_row_ceil
                                )
                            )

    def _refresh_table(self):
        """刷新表格"""
        self.get_element(By.CSS_SELECTOR, '.refresh_btn').click()
        sleep(self.TABLE_LOAD_WAIT_TIME)

    def show_table_data_count_per_page(self, count=100):
        """控制每页展示的条数"""
        ele_li_inputs = self.get_elements(By.CSS_SELECTOR, '.hc-pagination-num')
        ele_li_page_size_input = None
        for _ele_li_input in ele_li_inputs:
            if 'pageSize' in _ele_li_input.get_attribute('id'):
                ele_li_page_size_input = _ele_li_input
        ele_li_page_size_input.js_send_keys(str(count))

    def show_table_current_page_num(self, num):
        """设置当前是第几页"""
        ele_li_inputs = self.get_elements(By.CSS_SELECTOR, '.hc-pagination-num')
        ele_li_page_size_input = None
        for _ele_li_input in ele_li_inputs:
            if 'toPage' in _ele_li_input.get_attribute('id'):
                ele_li_page_size_input = _ele_li_input
        ele_li_page_size_input.js_send_keys(str(num))

    def table_page_turn_next(self):
        """点击表格下一页"""
        self.get_element(By.CSS_SELECTOR, '.next_page_btn').click()

    def get_all_table_data(self, wait=3) -> List[Dict]:
        """返回表格所有数据"""
        self.TABLE_LOAD_WAIT_TIME = wait
        self.default_table_page()
        return self._get_all_table_data()

    def default_table_page(self, wait=3):
        """将页面页数和每页展示条数重置为默认值，即第1页，每页100条"""
        self.TABLE_LOAD_WAIT_TIME = wait
        self.show_table_data_count_per_page(count=100)
        self.click_table_first_page_button()
        # self.show_table_current_page_num(num=1)
        self._refresh_table()

    def click_table_first_page_button(self):
        """点击跳转第一页按钮"""
        ele_first_page_btn = self.get_element(By.CSS_SELECTOR, '.first_page_btn')
        if 'disabled' in ele_first_page_btn.get_attribute('class'):
            return
        else:
            ele_first_page_btn.click()