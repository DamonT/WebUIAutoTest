# coding=utf-8

from selenium import webdriver
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.chrome.options import Options
import traceback
import time
import os
from typing import Union, NoReturn, Dict, List, Optional
import requests

from src.config.config import Config
from src.log.log import Log
from src.newselenium.by import By
from src.newselenium.keys import Keys
from src.newselenium.element import Element
from src.newselenium.driverwait import DriverWait
from src.exception.seleniumexecption import SeleniumTypeError, SeleniumTimeoutError, SeleniumValueError


# 记录日志
logger = Log()


class Driver(object):
    """
    说明：
        重写selenium的webdriver
        self._web_driver 为selenium的原生webdriver类对象
    """

    _cookies = []

    def __init__(self, url: str = None, browser_type: str = "", browser_driver: WebDriver = None):
        """
        说明：
            实例化出driver对象
        :param url: 访问地址
        :param browser_type: 浏览器类型 firefox \ chrome
        """
        self._web_driver = browser_driver  # 浏览器 <class 'WebDriver'>
        self.url = url
        self.browser_type = browser_type
        if not self._web_driver:  # 如果未指定浏览器对象，则自动打开浏览器
            self._init_open_browser()
        elif isinstance(self._web_driver, (Driver,)):
            raise SeleniumTypeError("参数WebDriver类型不对，不能为newselenium.Driver类")
        elif isinstance(self._web_driver, (WebDriver,)):
            logger.info("将selenium.WebDriver类转化为newselenium.Driver类")

    @classmethod
    def webdriver_to_driver(cls, web_driver: WebDriver):
        """
        说明：
            类方法
            将webDriver实例转化为Driver实例
        示例：

            from selenium import webdriver
            from src.newselenium.driver import Driver

            driver = webdriver.Chrome()
            driver.get(url=r"https://www.baidu.com/")
            newdriver = Driver.webdriver_to_driver(webdriver=driver)
            newdriver.close()
            ...

        :param web_driver: webDriver实例  <class 'selenium.webdriver.remote.webdriver.WebDriver'>
        :return: Driver实例  <class 'Driver'>
        """
        return Driver(url=web_driver.current_url, browser_type="fromWebDriver", browser_driver=web_driver)

    @classmethod
    def save_cookies(cls, driver=None) -> NoReturn:
        """
        说明：
            保存当前driver浏览器中的cookie信息
        :param driver: 浏览器实例 <class Driver>
        """
        if driver is None:
            raise SeleniumValueError('请传入浏览器对象driver')
        if not isinstance(driver, (Driver, )):
            raise SeleniumTypeError('driver对象应该是Driver类的实例，当前是类 %s 的实例' % type(driver))
        cls._cookies = driver.get_cookies()

    def _get_option(self, browser: str) -> Union[Options, None]:
        if str.lower(browser) == "chrome":
            option = webdriver.ChromeOptions()
            option.binary_location = Config.browser_path_chrome
            if Config.headless:
                option.set_headless(headless=True)
            return option
        if str.lower(browser) == "firefox":
            option = webdriver.FirefoxOptions()
            option.binary_location = Config.browser_path_firefox
            if Config.headless:
                option.set_headless(headless=True)
            return option

    def _init_open_browser(self) -> NoReturn:
        drivers_path = os.path.join(Config.project_dir, "Drivers")
        try:
            browser = str.strip(self.browser_type)
            if not browser:  # 如果没有指定浏览器类型，读取配置中设置的浏览器类型
                browser = str.strip(Config.browser_type)
            if str.lower(browser) == "chrome":  # 谷歌浏览器
                driver_remote = os.path.join(drivers_path, "chromedriver.exe")
                self._web_driver = webdriver.Chrome(
                    executable_path=driver_remote,
                    options=self._get_option(browser=browser),
                )
                logger.info("当前谷歌浏览器版本为%s" % (self._web_driver.capabilities.get("version") or self._web_driver.capabilities.get(
                    "browserVersion")))                # 静态超时时间
                # 2018-6-10 改为根据浏览器类型区分，火狐浏览器对这一方法兼容性较差，可能会导致报错
                self._web_driver.implicitly_wait(Config.timeout)
            elif str.lower(browser) == "firefox":  # 火狐浏览器
                driver_remote = os.path.join(drivers_path, "geckodriver.exe")
                self._web_driver = webdriver.Firefox(
                    executable_path=driver_remote,
                    options=self._get_option(browser=browser),
                )
                logger.info(
                    "当前使用的是火狐浏览器，初始化时未指定静态超时时间，可以通过driver.implicitly_wait() 方法设置"
                )
                logger.info("当前火狐浏览器版本为%s" % (self._web_driver.capabilities.get("version") or self._web_driver.capabilities.get(
                    "browserVersion")))
            elif str.lower(browser) == "phantomjs":  # 无头浏览器
                driver_remote = os.path.join(drivers_path, "phantomjs.exe")
                self._web_driver = webdriver.PhantomJS(
                    executable_path=driver_remote,
                )
                logger.info(
                    "当前使用的是phantomjs浏览器"
                )
                logger.info("当前phantomjs浏览器版本为%s" % (self._web_driver.capabilities.get("version") or self._web_driver.capabilities.get(
                    "browserVersion")))
            elif str.lower(browser) == "360":  # 360浏览器
                driver_remote = os.path.join(drivers_path, "chromedriver.exe")
                chrome_options = Options()
                browser_path_360 = Config.browser_path_360
                if browser_path_360 is None:  # 如果未配置则从注册表中获取
                    from src.public.regedit import Regedit
                    browser_path_360 = Regedit.get_value(path=r"HKEY_CLASSES_ROOT\360SeSES\DefaultIcon", name="(默认)").split(",")[0]
                chrome_options.binary_location = browser_path_360
                self._web_driver = webdriver.Chrome(chrome_options=chrome_options,
                                                    executable_path=driver_remote)
                logger.info("当前360浏览器Chrome内核版本为%s" % (self._web_driver.capabilities.get("version") or self._web_driver.capabilities.get(
                    "browserVersion")))
                # 静态超时时间
                self._web_driver.implicitly_wait(Config.timeout)
            else:
                raise SeleniumTypeError("未找到指定的浏览器及其对应的驱动...")
            # 打开网页
            # 2020-6-23 给浏览器添加cookie需要先get再add_cookie
            self._web_driver.get(url=self.url)
            self._init_add_cookies()
            self._web_driver.get(url=self.url)
        except Exception as e:
            logger.error("使用驱动打开浏览器错误，可能是驱动版本与浏览器版本不匹配。报错信息:\n" + traceback.format_exc())

    def _init_add_cookies(self):
        """
        说明：
            设置浏览器对象cookies，当类保存有cookie信息时，下次启动时会自动将cookie加载到浏览器中
        """
        for cookie in __class__._cookies:
            self.add_cookie(cookie)

    # --------------以下为重写的方法,方法名和源码一致，某些调用更方便,功能更强大-----------------

    def move_to_element(self, to_element: Element) -> NoReturn:
        """
        说明：
            鼠标移动到指定元素上
        :param to_element: 需要移动到的元素 type = <class Element>
        """
        ActionChains(self._web_driver).move_to_element(
            to_element=to_element.change_to_webelement()
        ).perform()

    def move_to_element_with_offset(self, to_element: Element, xoffset: int, yoffset: int) -> NoReturn:
        """
        Move the mouse by an offset of the specified element.
           Offsets are relative to the top-left corner of the element.

        :Args:
         - to_element: The WebElement to move to.
         - xoffset: X offset to move to.
         - yoffset: Y offset to move to.
        """
        ActionChains(self._web_driver).move_to_element_with_offset(
            to_element=to_element.change_to_webelement(),
            xoffset=xoffset,
            yoffset=yoffset
        ).perform()

    def pause(self, seconds: Union[int, float]) -> NoReturn:
        """ Pause all inputs for the specified duration in seconds """
        ActionChains(self._web_driver).pause(seconds=seconds).perform()

    def release(self, on_element: Element) -> NoReturn:
        """
        Releasing a held mouse button on an element.

        :Args:
         - on_element: The element to mouse up.
           If None, releases on current mouse position.
        """
        ActionChains(self._web_driver).release(
            on_element=on_element.change_to_webelement()
        ).perform()

    def drag_and_drop(self, source: Element, target: Element) -> NoReturn:
        """
        说明：
            将元素source拖动并释放到元素target处
        :param source: 需要移动的元素
        :param target: 目标元素
        示例：
            driver.drag_and_drop(source, target)
        注意：
            在拖拽元素时，即使匹配source元素的规则的在拖拽过程中发生变化，也可以完成拖拽
        """
        ActionChains(self._web_driver).drag_and_drop(
            source=source.change_to_webelement(), target=target.change_to_webelement()
        ).perform()
        # ActionChains(self._web_driver).click_and_hold(source).move_to_element(target).release().perform()

    def drag_and_drop_by_offset(self, source: Element, xoffset: int, yoffset: int) -> NoReturn:
        """
        说明：
            将元素source拖动到相对自身x, y坐标位置并释放
        :param source: 需要移动的元素
        :param xoffset: x方向像素值
        :param yoffset: y方向像素值
        示例：
            driver.drag_and_drop_by_offset(source, 500, 0)
        注意：
            在拖拽元素时，即使匹配source元素的规则的在拖拽过程中发生变化，也可以完成拖拽
        """
        ActionChains(self._web_driver).drag_and_drop_by_offset(
            source=source.change_to_webelement(), xoffset=xoffset, yoffset=yoffset
        ).perform()

    def click_and_hold(self, on_element: Element) -> NoReturn:
        """
        Holds down the left mouse button on an element.

        :Args:
         - on_element: The element to mouse down.
           If None, clicks on current mouse position.
        """
        ActionChains(self._web_driver).click_and_hold(
            on_element=on_element.change_to_webelement()
        ).perform()

    def key_down(self, value: Keys, element: Element) -> NoReturn:
        """
        Sends a key press only, without releasing it.
           Should only be used with modifier keys (Control, Alt and Shift).

        :Args:
         - value: The modifier key to send. Values are defined in `Keys` class.
         - element: The element to send keys.
           If None, sends a key to current focused element.

        Example, pressing ctrl+c::

            ActionChains(driver).key_down(Keys.CONTROL).send_keys('c').key_up(Keys.CONTROL).perform()

        """
        ActionChains(self._web_driver).key_down(
            value=value, element=element.change_to_webelement()
        ).perform()

    def key_up(self, value: Keys, element: Element) -> NoReturn:
        """
        Releases a modifier key.

        :Args:
         - value: The modifier key to send. Values are defined in Keys class.
         - element: The element to send keys.
           If None, sends a key to current focused element.

        Example, pressing ctrl+c::

            ActionChains(driver).key_down(Keys.CONTROL).send_keys('c').key_up(Keys.CONTROL).perform()

        """
        ActionChains(self._web_driver).key_up(
            value=value, element=element.change_to_webelement()
        ).perform()

    def send_keys(self, *keys_to_send) -> NoReturn:
        """
        Sends keys to current focused element.

        :Args:
         - keys_to_send: The keys to send.  Modifier keys constants can be found in the
           'Keys' class.
        """
        ActionChains(self._web_driver).send_keys(*keys_to_send).perform()

    def send_keys_to_element(self, element: Element, *keys_to_send) -> NoReturn:
        """
        Sends keys to an element.

        :Args:
         - element: The element to send keys.
         - keys_to_send: The keys to send.  Modifier keys constants can be found in the
           'Keys' class.
        """
        ActionChains(self._web_driver).send_keys_to_element(
            element=element.change_to_webelement(),
            *keys_to_send
        ).perform()

    def move_by_offset(self, xoffset: int, yoffset: int) -> NoReturn:
        """
        Moving the mouse to an offset from current mouse position.

        :Args:
         - xoffset: X offset to move to, as a positive or negative integer.
         - yoffset: Y offset to move to, as a positive or negative integer.
        """
        ActionChains(self._web_driver).move_by_offset(
            xoffset=xoffset, yoffset=yoffset
        ).perform()

    def switch_to_frame(self, frame_reference: Union[int, str, Element]) -> NoReturn:
        """
        说明：
            切换iframe/frame窗口
            可以根据iframe/frame标签的id、name属性，也可以通过getelement()返回的Element对象切换进去
            如果没有找到则会切换到最上层Top Window
        :param frame_reference: 
        """
        if isinstance(frame_reference, (int, str)):
            self._web_driver.switch_to.frame(frame_reference=frame_reference)
        elif isinstance(frame_reference, Element):
            self._web_driver.switch_to.frame(
                frame_reference=frame_reference.change_to_webelement()
            )
        else:
            self._web_driver.switch_to.default_content()  # 切换到最上层页面

    def switch_to_default_content(self) -> NoReturn:
        """
        说明：
            切换到最上层页面 Top Window
        """
        self._web_driver.switch_to.default_content()

    def switch_to_window(self, window_name: str) -> NoReturn:
        """
        说明：
            切换窗口句柄(标签页)
        :param window_name: 
            1.可以使用Driver.window_handles 返回的句柄进行切换
        示例:
            1. driver.switch_to_window(driver.window_handles[0])
        """
        self._web_driver.switch_to.window(window_name=window_name)

    def switch_to_alert(self) -> Alert:
        """
        说明：
            Switches focus to an alert on the page.
            将焦点切换到页面的提示框上(alert)，可以使用返回的alert对象操作提示框
        :return: 提示框alert对象 type = <class Alert >
        """
        return self._web_driver.switch_to.alert

    def execute_script(self, script: str, *args):
        """
        说明：
            执行 js 命令
        :param script: js 脚本 type = <class str>
        :param args: 可以是Element对象
        :return: 根据命令返回
        示例：
            driver.execute_script("arguments[0].scrollIntoView();", Element)
            driver.execute_script("return arguments[0].scrollHeight;", Element)
            driver.execute_script("return arguments[0][arguments[1]]", su, "value")
        """
        # 如果参数里面有Element的实例化对象，需要将其转化为selenium.webdriver.remote.webelement.WebElement对象
        if args:
            args = (arg.element if isinstance(arg, Element) else arg for arg in args)
        return self._web_driver.execute_script(script, *args)

    def execute_async_script(self, script, *args):
        """
        Asynchronously Executes JavaScript in the current window/frame.
        在当前window/frame执行异步js命令

        :Args:
         - script: The JavaScript to execute. js命令
         - \*args: Any applicable arguments for your JavaScript. js命令中的适用参数

        :Usage:
            script = "var callback = arguments[arguments.length - 1]; " \
                     "window.setTimeout(function(){ callback('timeout') }, 3000);"
            driver.execute_async_script(script)
        """
        return self._web_driver.execute_async_script(script, *args)

    def get(self, url: str) -> NoReturn:
        """
        说明：
            在当前窗口请求地址url
        """
        self._web_driver.get(url=url)

    def back(self) -> NoReturn:
        """
        说明：
            在浏览器历史中向后退一步
        """
        self._web_driver.back()

    def forward(self) -> NoReturn:
        """
        说明：
            在浏览器历史上前进一步
        """
        self._web_driver.forward()

    def close(self) -> NoReturn:
        """
        说明：
            关闭当前窗口
        """
        self._web_driver.close()

    def quit(self) -> NoReturn:
        """
        说明：
            关闭所有窗口并退出浏览器
        """
        self._web_driver.quit()

    def maximize_window(self) -> NoReturn:
        """
        说明：
            最大化浏览器窗口
        """
        self._web_driver.maximize_window()

    def minimize_window(self) -> NoReturn:
        """
        说明：
            最小化浏览器窗口
        """
        self._web_driver.minimize_window()

    def refresh(self) -> NoReturn:
        """
        说明：
            刷新当前页面
        """
        self._web_driver.refresh()

    def get_window_position(self) -> Dict[str, int]:
        """
        说明：
            返回当前浏览器的 x,y 坐标
        :return: {'y': 10, 'x': 10} type = <class dict>
        """
        return self._web_driver.get_window_position()

    def get_window_rect(self):
        """
        说明：
            返回当前浏览器的位置
        :return:
        """
        return self._web_driver.get_window_rect()

    def get_window_size(self) -> Dict[str, int]:
        """
        说明：
            返回当前浏览器的位置
        :return: {'width': 945, 'height': 1030} type = <class dict>
        """
        return self._web_driver.get_window_size()

    def set_window_position(self, x: int, y: int, window_handle: str = "current") -> NoReturn:
        """
        说明：
            设置浏览器的位置
        :param x: 
        :param y: 
        :param window_handle:
        """
        self._web_driver.set_window_position(x=x, y=y, windowHandle=window_handle)

    def set_window_rect(self, x=None, y=None, width=None, height=None) -> NoReturn:
        """
        说明：
            设置浏览器的位置与大小
        :param x: 
        :param y: 
        :param width: 
        :param height: 
        """
        self._web_driver.set_window_rect(x=x, y=y, width=width, height=height)

    def set_window_size(self, width: int, height: int, window_handle: str = "current") -> NoReturn:
        """
        说明：
            设置浏览器的大小
        :param width: 
        :param height: 
        :param window_handle:
        """
        self._web_driver.set_window_size(
            width=width, height=height, windowHandle=window_handle
        )

    def implicitly_wait(self, time_to_wait: int) -> NoReturn:
        self._web_driver.implicitly_wait(time_to_wait=time_to_wait)

    def set_script_timeout(self, time_to_wait: int):
        """
        Set the amount of time that the script should wait during an
           execute_async_script call before throwing an error.
        设置等待execute_async_script执行的超时时间

        :Args:
         - time_to_wait: The amount of time to wait (in seconds)

        :Usage:
            driver.set_script_timeout(30)
        """
        return self._web_driver.set_script_timeout(time_to_wait=time_to_wait)

    def set_page_load_timeout(self, time_to_wait: int):
        """
        Set the amount of time to wait for a page load to complete
           before throwing an error.
        设置等待页面加载的最大时间

        :Args:
         - time_to_wait: The amount of time to wait

        :Usage:
            driver.set_page_load_timeout(30)
        """
        return self._web_driver.set_page_load_timeout(time_to_wait=time_to_wait)

    def get_cookies(self):
        """
        说明：
            获得所有 cookie 信息
        :return: 所有cookie信息
        """
        return self._web_driver.get_cookies()

    def get_cookie(self, name: str):
        """
        说明：
            返回有特定 name 值有 cookie 信息
        :param name:
        :return: 指定cookie值
        """
        return self._web_driver.get_cookie(name=name)

    def add_cookie(self, cookie_dict: dict) -> NoReturn:
        """
        说明：
            添加 cookie
        :param cookie_dict: 字典对象, 必填参数 - "name" and "value";
                                     可选参数 - "path", "domain", "secure", "expiry"
        :return: None
        示例：
            driver.add_cookie({'name' : 'foo', 'value' : 'bar'})
            driver.add_cookie({'name' : 'foo', 'value' : 'bar', 'path' : '/'})
            driver.add_cookie({'name' : 'foo', 'value' : 'bar', 'path' : '/', 'secure':True})
        """
        self._web_driver.add_cookie(cookie_dict=cookie_dict)

    def delete_cookie(self, name: str) -> NoReturn:
        """
        说明：
            删除指定 cookie
        :param name:
        :return: None
        """
        self._web_driver.delete_cookie(name=name)

    def delete_all_cookies(self) -> NoReturn:
        """
        说明：
            删除所有 cookie信息
        :return: None
        """
        self._web_driver.delete_all_cookies()

    @property
    def window_handles(self) -> List[str]:
        """
        说明：
            返回当前浏览器的窗口句柄
        :return: <type list>
        """
        return self._web_driver.window_handles

    @property
    def current_window_handle(self) -> str:
        """
        说明：
            返回当前窗口句柄
        :return: type = <class str>
        """
        return self._web_driver.current_window_handle

    @property
    def current_url(self) -> str:
        """
        说明：
            返回当前窗口请求的url地址
        :return: type = <class str>
        """
        return self._web_driver.current_url

    @property
    def title(self) -> str:
        """
        说明：
            返回当前窗口的标题
        :return: type = <class str>
        """
        return self._web_driver.title

    @property
    def name(self) -> str:
        return self._web_driver.name

    @property
    def page_source(self) -> str:
        return self._web_driver.page_source

    # --------------以下为新命名的方法-----------------

    def get_element(self, by: str = By.ID, value: str = None) -> Element:
        """
        说明：
            定位并返回元素
        :param by: By.ID By.XPATH By.LINK_TEXT By.PARTIAL_LINK By.NAME By.TAG_NAME By.CLASS_NAME By.CSS_SELECTOR type = <class By>
        :param value: 匹配规则 type = <class str>
        :return: type = <class Element>
        示例：
            element = driver.get_element(By.ID, "id_name")
        """
        # return Element(self._web_driver, self._web_driver.find_element(by=by, value=value))
        # return DriverWait(driver=self._web_driver, timeout=Config.timeout).\
        #     presence_of_element_located(by=by, value=value)
        return DriverWait(driver=self, timeout=Config.timeout).presence_of_element_located(by=by, value=value)

    def get_elements(self, by: str = By.ID, value: str = None) -> List[Element]:
        """
        说明：
            定位并返回元素
        :param by: By.ID By.XPATH By.LINK_TEXT By.PARTIAL_LINK By.NAME By.TAG_NAME By.CLASS_NAME By.CSS_SELECTOR type = <class By>
        :param value: 匹配规则 type = <class str>
        :return: type = <class list>
        示例：
            elements = driver.get_elements(By.ID, "id_name")
        """
        # elements = self._web_driver.find_elements(by=by, value=value)  # 如果没找到返回空列表
        elements = DriverWait(driver=self, timeout=Config.timeout).\
            presence_of_all_elements_located(by=by, value=value)
        return list(map(lambda ele: Element(self._web_driver, ele), elements))

    def double_click(self, element: Element) -> NoReturn:
        """
        说明：
            鼠标双击指定的element
        :param element: element type = <class Element>
        """
        ActionChains(self._web_driver).double_click(element.change_to_webelement()).perform()

    def left_click(self, element: Element) -> NoReturn:
        """
        说明：
            鼠标左击指定的element
        :param element: element type = <class Element>
        """
        ActionChains(self._web_driver).click(element.change_to_webelement()).perform()

    def right_click(self, element: Element) -> NoReturn:
        """
        说明：
            鼠标右击指定的element
        :param element: element type = <class Element>
        """
        ActionChains(self._web_driver).context_click(element.change_to_webelement()).perform()

    def switch_to_window_by_tag_name(self, tag_name: str) -> NoReturn:
        """
        说明：
            切换窗口句柄(标签页)
        :param tag_name: 
            1.可以使用窗口的标签页名称进行切换
        示例:
            1. driver.switch_to_window_by_tag_name("百度一下，你就知道")
        """
        timeout = 10
        clock = 0
        while clock < timeout:
            for handle in self._web_driver.window_handles:
                self._web_driver.switch_to.window(handle)
                if self._web_driver.title == tag_name:
                    print(self._web_driver.title)
                    return
            time.sleep(1)
            clock += 1
        raise SeleniumTimeoutError("未找到您提供“{}”的标签名".format(tag_name))

    def operate_alert(self, mode="ACCEPT") -> Optional[str]:
        """
        说明：
            操作当前窗口的alert提示框，也支持confirm、prompt对话框
        :param mode: 
            1. "ACCEPT" 点击提示框确认按钮
            2. "DISMISS" 点击提示框右上角的“x”
            3. "TEXT" 返回提示框的文字信息
        :return: 如果mode="TEXT"将返回提示框的内容
        """
        alert = self._web_driver.switch_to.alert
        if mode.upper() == "ACCEPT":
            alert.accept()
        elif mode.upper() == "DISMISS":
            alert.dismiss()
        elif mode.upper() == "TEXT":
            return alert.text

    def open_new_window(self):
        """
        说明：
            打开新窗口(标签页)
        :return: 根据命令返回
        """
        return self.execute_script("window.open()")

    def get_screen_shot(self, path: str = None) -> bool:
        """
        说明：
            截图并保存到指定log目录
        :param path: 如果指定目录则存放在指定目录中
        """
        if path is None:
            time_now = time.strftime("%Y%m%d-%H%M%S", time.localtime())
            path = os.path.join(Config.project_dir, 'Log', time_now + '.png')
        return self._web_driver.get_screenshot_as_file(filename=path)

    def scroll_to_bottom(self):
        """
        说明：
            滚动滚动条到底部
        :return: 根据命令返回
        """
        return self._web_driver.execute_script("window.scrollTo(0, 999999)")

    def scroll_to_up(self):
        """
        说明：
            滚动滚动条到顶部
        :return: 根据命令返回
        """
        return self._web_driver.execute_script("window.scrollTo(0, 0)")

    def scroll_to_xy(self, x=0, y=0):
        """
        说明：
            滚动到相对于左上角的(x, y)位置
        :return: 根据命令返回
        """
        return self._web_driver.execute_script("window.scrollTo({}, {})".format(x, y))

    def focus_element(self, element: Element):
        """
        说明：
            视图定位到指定的element上
        :param element: Element对象 <class 'Element'>
        :return: 根据命令返回
        """
        return self.execute_script("arguments[0].scrollIntoView();", element)

    def change_to_webdriver(self) -> WebDriver:
        """
        说明：
            返回原生webdriver对象
        :return: 
        """
        return self._web_driver

    def wait_until(self, timeout=10) -> DriverWait:
        """
        说明：
            明确等待
        :param timeout: 超时时间
        :return: type=WebDriverWait
        """
        return DriverWait(driver=self._web_driver, timeout=timeout)

    @property
    def web_driver(self) -> WebDriver:
        """
        说明：
            返回原生WebDriver对象
        :return: <class 'WebDriver'>
        """
        return self._web_driver

    def send_request(self, method: str, url: str, data=None, json=None, **kwargs) -> requests.Response:
        """
        说明：
            利用当前浏览器的cookie信息发送get或post请求，返回requests.Response实例
        :param method: 'get' or 'put' 请求类型
        :param url: 地址
        :param data: (optional) Dictionary, list of tuples, bytes, or file-like
                     object to send in the body of the :class:`Request`.
        :param json: (optional) json to send in the body of the :class:`Request`.
        :param kwargs: Optional arguments that ``request`` takes.
        :return: 应答信息 <class requests.Response>
        """
        cookies = self.get_cookies()
        s = requests.Session()
        for cookie in cookies:
            s.cookies.set(cookie['name'], cookie['value'])
        if method.lower() == 'post':
            resp = s.post(url=url, data=data, json=json, **kwargs)
        elif method.lower() == 'get':
            resp = s.get(url=url, **kwargs)
        else:
            raise SeleniumTypeError('不支持method类型为%s的请求方式，目前只支持get和post' % method)
        return resp
