# coding=utf-8

from subprocess import Popen
import subprocess
import os
import traceback
import psutil

from src import Log
from src.public.public import ParseXml
from src.config.config import Config
from runtest import update_aborted_data


logger = Log()


def stop(pid=None):
    try:
        logger.info('正在终止自动化构建')
        xml = ParseXml(filepath=os.path.join(Config.project_dir, "automation.data"))
        if pid is None:
            running_pid = xml.get_value_from_xpath(xpath="./BuildData/RunningPID", attribute="pid")
        else:
            running_pid = pid
        logger.info('获取到构建进程pid {}'.format(running_pid))
        # zz: 使用psutil process.kill()发现存在子进程无法及时删除的问题，还是采用windows下的taskkill工具
        cmd = 'taskkill /PID {} /F /T'.format(running_pid)
        p = Popen(cmd,
                  shell=True,
                  stdin=subprocess.PIPE,
                  stdout=subprocess.PIPE,
                  stderr=subprocess.STDOUT,
                  universal_newlines=True)
        returncode = p.wait(30)
        stdout, stderr = p.communicate()
        if returncode != 0 or stderr:
            logger.error('终止构建失败，失败原因：\n' + stdout or stderr)
        else:
            update_aborted_data()
            logger.info('自动化构建已停止，详细信息：\n' + stdout)
        # p = psutil.Process(int(running_pid))
        # if p.is_running():
        #     p.kill()
    except Exception:
        logger.error("终止构建失败。报错信息:\n" + traceback.format_exc())


if __name__ == "__main__":
    stop()